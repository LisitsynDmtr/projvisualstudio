﻿using System;
using System.Collections.Generic;
using System.Resources;

namespace labDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> x = new Dictionary<int, string>();
            x.Add(10, "Брянск");
            x.Add(20, "Псков");
            x.Add(30, "Тула");
            x.Add(40, "Калуга");
            x.Add(50, "Самара");

            x[40] = "Москва";
            x.Remove(20);
            foreach (KeyValuePair<int, string> v in x)
            {
                Console.WriteLine($"Key = {v.Key}, Value = {v.Value}");
            }

            Console.WriteLine("-------");
            Console.WriteLine(x[30]);
        }
    }
}
