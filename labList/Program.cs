﻿using System;
using System.Collections.Generic;

namespace labList
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> x = new List<int>() { 1, 2, 3, 4 };
            x.Add(5);
            x.AddRange(new int[] { 6, 7, 8 });
            x.Insert(0, 100);
            x.RemoveAt(2);

            foreach (int i in x)
                Console.WriteLine(i);
        }
    }
}
