﻿
namespace labImageCircuit
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.buRectangle = new System.Windows.Forms.Button();
            this.buOriginal = new System.Windows.Forms.Button();
            this.buCircuit = new System.Windows.Forms.Button();
            this.pxResult = new System.Windows.Forms.PictureBox();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.trDelta = new System.Windows.Forms.TrackBar();
            this.textTr = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pxResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trDelta)).BeginInit();
            this.SuspendLayout();
            // 
            // buRectangle
            // 
            this.buRectangle.Location = new System.Drawing.Point(113, 431);
            this.buRectangle.Name = "buRectangle";
            this.buRectangle.Size = new System.Drawing.Size(114, 45);
            this.buRectangle.TabIndex = 2;
            this.buRectangle.Text = "Прямоугольник";
            this.buRectangle.UseVisualStyleBackColor = true;
            // 
            // buOriginal
            // 
            this.buOriginal.Location = new System.Drawing.Point(381, 431);
            this.buOriginal.Name = "buOriginal";
            this.buOriginal.Size = new System.Drawing.Size(114, 45);
            this.buOriginal.TabIndex = 3;
            this.buOriginal.Text = "Оригинал";
            this.buOriginal.UseVisualStyleBackColor = true;
            // 
            // buCircuit
            // 
            this.buCircuit.Location = new System.Drawing.Point(631, 431);
            this.buCircuit.Name = "buCircuit";
            this.buCircuit.Size = new System.Drawing.Size(114, 45);
            this.buCircuit.TabIndex = 4;
            this.buCircuit.Text = "Контур";
            this.buCircuit.UseVisualStyleBackColor = true;
            // 
            // pxResult
            // 
            this.pxResult.Location = new System.Drawing.Point(508, 29);
            this.pxResult.Name = "pxResult";
            this.pxResult.Size = new System.Drawing.Size(343, 377);
            this.pxResult.TabIndex = 1;
            this.pxResult.TabStop = false;
            // 
            // pxImage
            // 
            this.pxImage.Image = global::labImageCircuit.Properties.Resources.sprite;
            this.pxImage.Location = new System.Drawing.Point(12, 29);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(357, 377);
            this.pxImage.TabIndex = 0;
            this.pxImage.TabStop = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(863, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(127, 22);
            this.toolStripButton1.Text = "Загрузить картинку...";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // trDelta
            // 
            this.trDelta.Location = new System.Drawing.Point(571, 482);
            this.trDelta.Name = "trDelta";
            this.trDelta.Size = new System.Drawing.Size(226, 45);
            this.trDelta.TabIndex = 6;
            // 
            // textTr
            // 
            this.textTr.Location = new System.Drawing.Point(792, 482);
            this.textTr.Name = "textTr";
            this.textTr.Size = new System.Drawing.Size(35, 20);
            this.textTr.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 510);
            this.Controls.Add(this.textTr);
            this.Controls.Add(this.trDelta);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.buCircuit);
            this.Controls.Add(this.buOriginal);
            this.Controls.Add(this.buRectangle);
            this.Controls.Add(this.pxResult);
            this.Controls.Add(this.pxImage);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pxResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trDelta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.PictureBox pxResult;
        private System.Windows.Forms.Button buRectangle;
        private System.Windows.Forms.Button buOriginal;
        private System.Windows.Forms.Button buCircuit;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.TrackBar trDelta;
        private System.Windows.Forms.TextBox textTr;
    }
}

