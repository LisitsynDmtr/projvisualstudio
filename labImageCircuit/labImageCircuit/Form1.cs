﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageCircuit
{
    public partial class Form1 : Form
    {
      
        private Point MD;
        Pen redPen = new Pen(Color.Red, 2);
       
        private Color curColor;
        private bool Rect = false;
        private bool Cont = false;
        private Rectangle rect;
        private Rectangle mRect;

        public Point FirstPoint { get; private set; }

        public Form1()
        {
            InitializeComponent();
            //Hw 
            //три кнопки  - оригинал, прямоугольник - контур
            //при нажатии кнопкой мыши внутри выдел обл она коп в друг пикбокс
            var bitmap = new Bitmap(pxImage.Image, pxImage.Width, pxImage.Height);
            pxImage.Image = new Bitmap(bitmap);
            buRectangle.Click += BuRectangle_Click;
            buCircuit.Click += BuCircuit_Click;
            buOriginal.Click += BuOriginal_Click;
            trDelta.Maximum = 255 * 3;
            trDelta.Value = 100;
            textTr.Text = $"{trDelta.Value}";
            trDelta.ValueChanged += (s,e) => textTr.Text = $"{trDelta.Value}";
        }

        private void BuOriginal_Click(object sender, EventArgs e)
        {
            
            Rect = false;
            pxResult.Image = pxImage.Image;
            pxResult.Invalidate();
            pxImage.Invalidate();

            pxImage.MouseMove -= PxImage_MouseMove;
            pxImage.Click -= PxImage_Click;
            pxImage.MouseDown += PxImage_MouseDown;
           
            pxResult.Paint -= PxResult_Paint;
            pxImage.MouseMove -= PxImage_MouseMove; ;
           // pxImage.MouseClick -= PxImage_MouseClick;


        }

   


        private void BuCircuit_Click(object sender, EventArgs e)
        {
            Cont = true;
            Rect = false;
            pxImage.Invalidate();
            pxImage.MouseMove -= PxImage_MouseMove;
            pxImage.Click -= PxImage_Click;
            pxResult.Paint -= PxResult_Paint;

            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.MouseClick += PxImage_MouseClick;

        }

        private void BuRectangle_Click(object sender, EventArgs e)
        {
            Rect = true;
            Cont = false;
            pxImage.MouseMove -= PxImage_MouseMove; ;
           

            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.Click += PxImage_Click;
            pxImage.MouseDown += PxImage_MouseDown;

        }


        private void PxImage_MouseDown(object sender, MouseEventArgs e)
        {
            mRect = new Rectangle(e.X, e.Y, 0, 0);
            this.Invalidate();
        }

        private void PxImage_MouseClick(object sender, MouseEventArgs e)
        {
            var bitmap = new Bitmap(pxImage.Image);
            curColor = bitmap.GetPixel(e.X, e.Y);
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    var color = bitmap.GetPixel(i, j);

                    
                    var v =
                        (Math.Max(curColor.R, color.R) - Math.Min(curColor.R, color.R) +
                        (Math.Max(curColor.G, color.G) - Math.Min(curColor.G, color.G) +
                        (Math.Max(curColor.B, color.B) - Math.Min(curColor.B, color.B))));

                    bitmap.SetPixel(i, j, (v < trDelta.Value) ? color : Color.Transparent);

                }
            }
            pxResult.Image = new Bitmap(bitmap);
            pxResult.Invalidate();
            this.Text = $"{Application.ProductName} : {curColor}";
        }


        private void PxImage_Click(object sender, EventArgs e)
        {
           
             pxResult.Paint += PxResult_Paint;
             pxResult.Invalidate();


        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            pxImage.Paint += PxImage_Paint;


            if (e.Button == MouseButtons.Left)
            {
                mRect = new Rectangle(mRect.Left, mRect.Top, e.X - mRect.Left, e.Y - mRect.Top);
                this.Invalidate();
            }

            pxImage.Invalidate();


          
        }

        private void PxImage_Paint(object sender, PaintEventArgs e)
        {
            if (Rect == true)
            {
                using (Pen pen = new Pen(Color.Red, 1))
                {
                    e.Graphics.DrawRectangle(pen, mRect);
                }
            }
        }

        private void PxResult_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawImage(pxImage.Image,
            new Rectangle(0, 0, pxResult.Width, pxResult.Height),
            mRect,
            GraphicsUnit.Pixel
            
            );

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                pxImage.Image = new Bitmap(dialog.FileName);
                var bitmap = new Bitmap(pxImage.Image, pxImage.Width, pxImage.Height);
                pxImage.Image = new Bitmap(bitmap);
                pxImage.Invalidate();
            }
        }
    }
}
