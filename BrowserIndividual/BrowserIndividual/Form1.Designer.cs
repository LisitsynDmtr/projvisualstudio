﻿namespace BrowserIndividual
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buBack = new System.Windows.Forms.ToolStripButton();
            this.buForward = new System.Windows.Forms.ToolStripButton();
            this.buRefresh = new System.Windows.Forms.ToolStripButton();
            this.buHome = new System.Windows.Forms.ToolStripButton();
            this.url = new System.Windows.Forms.ToolStripTextBox();
            this.menuTool = new System.Windows.Forms.ToolStripDropDownButton();
            this.addToFavorite = new System.Windows.Forms.ToolStripMenuItem();
            this.makeHome = new System.Windows.Forms.ToolStripMenuItem();
            this.printPage = new System.Windows.Forms.ToolStripMenuItem();
            this.offImages = new System.Windows.Forms.ToolStripMenuItem();
            this.history = new System.Windows.Forms.ToolStripMenuItem();
            this.historyClear = new System.Windows.Forms.ToolStripMenuItem();
            this.favoriteClear = new System.Windows.Forms.ToolStripMenuItem();
            this.listFavorite = new System.Windows.Forms.ToolStripDropDownButton();
            this.buClose = new System.Windows.Forms.ToolStripButton();
            this.buOpenNew = new System.Windows.Forms.ToolStripButton();
            this.buFind = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.labelProgress = new System.Windows.Forms.Label();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buBack,
            this.buForward,
            this.buRefresh,
            this.buHome,
            this.url,
            this.menuTool,
            this.listFavorite,
            this.buClose,
            this.buOpenNew,
            this.buFind});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1032, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buBack
            // 
            this.buBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buBack.Image = ((System.Drawing.Image)(resources.GetObject("buBack.Image")));
            this.buBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buBack.Name = "buBack";
            this.buBack.Size = new System.Drawing.Size(23, 22);
            this.buBack.Text = "toolStripButton1";
            // 
            // buForward
            // 
            this.buForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buForward.Image = ((System.Drawing.Image)(resources.GetObject("buForward.Image")));
            this.buForward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buForward.Name = "buForward";
            this.buForward.Size = new System.Drawing.Size(23, 22);
            this.buForward.Text = "toolStripButton2";
            // 
            // buRefresh
            // 
            this.buRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buRefresh.Image = ((System.Drawing.Image)(resources.GetObject("buRefresh.Image")));
            this.buRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buRefresh.Name = "buRefresh";
            this.buRefresh.Size = new System.Drawing.Size(23, 22);
            this.buRefresh.Text = "toolStripButton3";
            // 
            // buHome
            // 
            this.buHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buHome.Image = ((System.Drawing.Image)(resources.GetObject("buHome.Image")));
            this.buHome.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buHome.Name = "buHome";
            this.buHome.Size = new System.Drawing.Size(23, 22);
            this.buHome.Text = "toolStripButton1";
            // 
            // url
            // 
            this.url.AutoSize = false;
            this.url.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.url.Name = "url";
            this.url.Size = new System.Drawing.Size(800, 23);
            // 
            // menuTool
            // 
            this.menuTool.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.menuTool.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuTool.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToFavorite,
            this.makeHome,
            this.printPage,
            this.offImages,
            this.history,
            this.historyClear,
            this.favoriteClear});
            this.menuTool.Image = ((System.Drawing.Image)(resources.GetObject("menuTool.Image")));
            this.menuTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuTool.Name = "menuTool";
            this.menuTool.Size = new System.Drawing.Size(29, 22);
            this.menuTool.Text = "Tools";
            // 
            // addToFavorite
            // 
            this.addToFavorite.Name = "addToFavorite";
            this.addToFavorite.Size = new System.Drawing.Size(166, 22);
            this.addToFavorite.Text = "Add to favotite";
            // 
            // makeHome
            // 
            this.makeHome.Name = "makeHome";
            this.makeHome.Size = new System.Drawing.Size(166, 22);
            this.makeHome.Text = "Make home page";
            // 
            // printPage
            // 
            this.printPage.Name = "printPage";
            this.printPage.Size = new System.Drawing.Size(166, 22);
            this.printPage.Text = "Print ";
            // 
            // offImages
            // 
            this.offImages.Name = "offImages";
            this.offImages.Size = new System.Drawing.Size(166, 22);
            this.offImages.Text = "Off Images";
            // 
            // history
            // 
            this.history.Name = "history";
            this.history.Size = new System.Drawing.Size(166, 22);
            this.history.Text = "History";
            // 
            // historyClear
            // 
            this.historyClear.Name = "historyClear";
            this.historyClear.Size = new System.Drawing.Size(166, 22);
            this.historyClear.Text = "Clear History";
            // 
            // favoriteClear
            // 
            this.favoriteClear.Name = "favoriteClear";
            this.favoriteClear.Size = new System.Drawing.Size(166, 22);
            this.favoriteClear.Text = "Clear Favorite";
            // 
            // listFavorite
            // 
            this.listFavorite.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.listFavorite.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.listFavorite.Image = ((System.Drawing.Image)(resources.GetObject("listFavorite.Image")));
            this.listFavorite.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.listFavorite.Name = "listFavorite";
            this.listFavorite.Size = new System.Drawing.Size(29, 22);
            this.listFavorite.Text = "Favorite sites";
            // 
            // buClose
            // 
            this.buClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buClose.Image = ((System.Drawing.Image)(resources.GetObject("buClose.Image")));
            this.buClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buClose.Name = "buClose";
            this.buClose.Size = new System.Drawing.Size(23, 22);
            this.buClose.Text = "toolStripButton1";
            // 
            // buOpenNew
            // 
            this.buOpenNew.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buOpenNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buOpenNew.Image = ((System.Drawing.Image)(resources.GetObject("buOpenNew.Image")));
            this.buOpenNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buOpenNew.Name = "buOpenNew";
            this.buOpenNew.Size = new System.Drawing.Size(23, 22);
            this.buOpenNew.Text = "toolStripButton5";
            // 
            // buFind
            // 
            this.buFind.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buFind.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buFind.Image = ((System.Drawing.Image)(resources.GetObject("buFind.Image")));
            this.buFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buFind.Name = "buFind";
            this.buFind.Size = new System.Drawing.Size(23, 22);
            this.buFind.Text = "toolStripButton4";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Location = new System.Drawing.Point(0, 28);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1032, 632);
            this.tabControl1.TabIndex = 1;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBar1.Location = new System.Drawing.Point(12, 668);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(147, 19);
            this.progressBar1.TabIndex = 2;
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(165, 670);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(0, 17);
            this.labelProgress.TabIndex = 3;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 697);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "BrowserIndividual";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buBack;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolStripButton buForward;
        private System.Windows.Forms.ToolStripButton buRefresh;
        private System.Windows.Forms.ToolStripTextBox url;
        private System.Windows.Forms.ToolStripButton buFind;
        private System.Windows.Forms.ToolStripButton buOpenNew;
        private System.Windows.Forms.ToolStripButton buClose;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.ToolStripButton buHome;
        private System.Windows.Forms.ToolStripDropDownButton menuTool;
        private System.Windows.Forms.ToolStripMenuItem makeHome;
        private System.Windows.Forms.ToolStripMenuItem printPage;
        private System.Windows.Forms.ToolStripMenuItem offImages;
        private System.Windows.Forms.ToolStripMenuItem addToFavorite;
        private System.Windows.Forms.ToolStripDropDownButton listFavorite;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem history;
        private System.Windows.Forms.ToolStripMenuItem historyClear;
        private System.Windows.Forms.ToolStripMenuItem favoriteClear;
    }
}

