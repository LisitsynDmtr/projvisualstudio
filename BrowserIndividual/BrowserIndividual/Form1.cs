﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrowserIndividual
{
    public partial class Form1 : Form
    {
       
        int i = 1;
        private string homeUrl;
        private WebBrowser activeWindow;
      
        private string[] linkFavorite;
        private string currentPath;
        

        public Form1()
        {

            InitializeComponent();
            homeUrl = "google.com";

            WebBrowser brow = new WebBrowser();



            tabControl1.TabPages.Add("Start Page");
            tabControl1.SelectTab(0);
            brow.Dock = DockStyle.Fill;
            tabControl1.SelectedTab.Controls.Add(brow);

            activeWindow = (WebBrowser)tabControl1.SelectedTab.Controls[0];

            (activeWindow).Navigate(homeUrl);
            url.Text = homeUrl;

            brow.ProgressChanged += Brow_ProgressChanged;
            brow.DocumentCompleted += Browser_DocumentCompleted;

            tabControl1.Selected += TabControl1_Selected;

            favoriteClear.Click += FavoriteClear_Click;
            listFavorite.DropDownItemClicked += ListFavorite_DropDownItemClicked;
            toolStrip1.Resize += ToolStrip1_Resize;
            historyClear.Click += (s, e) => { history.DropDownItems.Clear(); MessageBox.Show("History was clear"); };
            history.DropDownItemClicked += History_DropDownItemClicked;
            offImages.Click += OffImages_Click;
            addToFavorite.Click += AddToFavorite_Click;
            makeHome.Click += (s, e) => homeUrl = activeWindow.Url.ToString();
            printPage.Click += (s, e) => activeWindow.Print();
            url.KeyDown += Url_KeyDown;
            buOpenNew.Click += BuOpenNew_Click;
            buFind.Click += BuFind_Click;
            buBack.Click += BuBack_Click;
            buForward.Click += BuForward_Click;
            buRefresh.Click += BuRefresh_Click;
            buClose.Click += BuClose_Click;
            buHome.Click += BuHome_Click;

            currentPath = Directory.GetCurrentDirectory().ToString();
            DirectoryInfo dirInfo = new DirectoryInfo(currentPath);

            if (!File.Exists(currentPath + @"\favorite.txt"))
            {
                File.Create(currentPath + @"\favorite.txt");

            }

            using (StreamReader readFile = new StreamReader($@"{currentPath}\favorite.txt"))
            { 
                while (!readFile.EndOfStream)
                {
                    string s = readFile.ReadLine();
                    listFavorite.DropDownItems.Add(s);
                }
                readFile.Close();
            }

        }

        private void FavoriteClear_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter(currentPath + @"\favorite.txt", false, System.Text.Encoding.Default);
            sw.Close();
            listFavorite.DropDownItems.Clear();
            MessageBox.Show("Favorite list was clear");
        }

        private void ToolStrip1_Resize(object sender, EventArgs e)
        {
            url.Width = toolStrip1.Width - 232;
 
      }
      

        private void History_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            activeWindow.Navigate(e.ClickedItem.Text);
            history.DropDownItems.Add(activeWindow.Url.ToString());
        }

        private void ListFavorite_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
            //indexClicked = e.ClickedItem;
            linkFavorite = e.ClickedItem.Text.Split(new char[] { '(', ')' });
            activeWindow.Navigate(linkFavorite[1]);

        }

    

        private void AddToFavorite_Click(object sender, EventArgs e)
        {
            using (StreamWriter sw = new StreamWriter(currentPath + @"\favorite.txt", true, System.Text.Encoding.Default))
            {
                sw.WriteLine(activeWindow.Document.Title.ToString() + "(" + activeWindow.Url.ToString() + ")");
                sw.Close();
            }

            listFavorite.DropDownItems.Add(activeWindow.Document.Title.ToString() + "(" + activeWindow.Url.ToString() + ")");   
        }

        private void OffImages_Click(object sender, EventArgs e)
        {
            foreach (HtmlElement image in (activeWindow).Document.Images)
            {

                image.SetAttribute("src", " ");

            }
        }

        private void BuHome_Click(object sender, EventArgs e)
        {
            activeWindow.Navigate(homeUrl);
        }

        private void TabControl1_Selected(object sender, TabControlEventArgs e)
        {

            url.Text = activeWindow.Url.ToString();
        }

        private void Brow_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {

            try
            {
            if(e.CurrentProgress > 0)
                progressBar1.Value = (int)(e.CurrentProgress * 100 / e.MaximumProgress);
            }
            catch (Exception)
            {

                return;
            }
            
        }

        private void Url_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                labelProgress.Text = "Loading...";
                activeWindow.Navigate(url.Text);
                history.DropDownItems.Add(activeWindow.Url.ToString());
            
            }
        }


        private void BuClose_Click(object sender, EventArgs e)
        {
            if (tabControl1.TabPages.Count > 1)
            {
                tabControl1.TabPages.RemoveAt(tabControl1.SelectedIndex);
                tabControl1.SelectTab(tabControl1.TabPages.Count - 1);
            }
            else
                Application.Exit();

        }

        private void BuRefresh_Click(object sender, EventArgs e)
        {
            activeWindow.Refresh();
        }

        private void BuForward_Click(object sender, EventArgs e)
        {
            activeWindow.GoForward();
            
        }

        private void BuBack_Click(object sender, EventArgs e)
        {
            activeWindow.GoBack();
           
        }

        private void BuFind_Click(object sender, EventArgs e)
        {
            if (url.Text != null)
            {
                labelProgress.Text = "Loading...";
                ((WebBrowser)tabControl1.SelectedTab.Controls[0]).Navigate(url.Text);
                history.DropDownItems.Add(activeWindow.Url.ToString());
            }
        }

        private void BuOpenNew_Click(object sender, EventArgs e)
        {
            WebBrowser browser = new WebBrowser();
            browser.Visible = true;
            //browser.ScriptErrorsSuppressed = true;
            browser.Dock = DockStyle.Fill;
            browser.ProgressChanged += Brow_ProgressChanged;
            browser.DocumentCompleted += Browser_DocumentCompleted;
            tabControl1.TabPages.Add("New Page");
            
            tabControl1.SelectTab(i);
            tabControl1.SelectedTab.Controls.Add(browser);
            
            i++;

        }

       

        private void Browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            
            labelProgress.Text = "Loading Complited";

            tabControl1.SelectedTab.Text = (activeWindow).DocumentTitle;
            
            url.Text = activeWindow.Document.Url.ToString();
            foreach (HtmlElement link in (activeWindow).Document.Links )
            {

                link.Click += Link_Click;

            }

        }

        private void Link_Click(object sender, HtmlElementEventArgs e)
        {
            HtmlElement linkToNavigate = sender as HtmlElement;
            url.Text = linkToNavigate.GetAttribute("href");
            activeWindow.Navigate(url.Text);
            history.DropDownItems.Add(activeWindow.Url.ToString());
        }

    }
}
