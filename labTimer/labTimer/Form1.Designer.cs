﻿namespace labTimer
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.buUp = new System.Windows.Forms.Button();
            this.buDown = new System.Windows.Forms.Button();
            this.pbUp = new System.Windows.Forms.ProgressBar();
            this.pbDown = new System.Windows.Forms.ProgressBar();
            this.buPauseUp = new System.Windows.Forms.Button();
            this.buPauseDown = new System.Windows.Forms.Button();
            this.pbMilSec = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // buUp
            // 
            this.buUp.Location = new System.Drawing.Point(18, 56);
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(285, 100);
            this.buUp.TabIndex = 0;
            this.buUp.Text = "button1";
            this.buUp.UseVisualStyleBackColor = true;
            // 
            // buDown
            // 
            this.buDown.Location = new System.Drawing.Point(18, 249);
            this.buDown.Name = "buDown";
            this.buDown.Size = new System.Drawing.Size(285, 100);
            this.buDown.TabIndex = 1;
            this.buDown.Text = "button2";
            this.buDown.UseVisualStyleBackColor = true;
            // 
            // pbUp
            // 
            this.pbUp.Location = new System.Drawing.Point(18, 162);
            this.pbUp.Name = "pbUp";
            this.pbUp.Size = new System.Drawing.Size(285, 23);
            this.pbUp.TabIndex = 2;
            // 
            // pbDown
            // 
            this.pbDown.Location = new System.Drawing.Point(18, 355);
            this.pbDown.Name = "pbDown";
            this.pbDown.Size = new System.Drawing.Size(285, 23);
            this.pbDown.TabIndex = 2;
            // 
            // buPauseUp
            // 
            this.buPauseUp.Location = new System.Drawing.Point(442, 76);
            this.buPauseUp.Name = "buPauseUp";
            this.buPauseUp.Size = new System.Drawing.Size(126, 61);
            this.buPauseUp.TabIndex = 3;
            this.buPauseUp.Text = "Pause";
            this.buPauseUp.UseVisualStyleBackColor = true;
            // 
            // buPauseDown
            // 
            this.buPauseDown.Location = new System.Drawing.Point(442, 269);
            this.buPauseDown.Name = "buPauseDown";
            this.buPauseDown.Size = new System.Drawing.Size(126, 61);
            this.buPauseDown.TabIndex = 3;
            this.buPauseDown.Text = "Pause";
            this.buPauseDown.UseVisualStyleBackColor = true;
            // 
            // pbMilSec
            // 
            this.pbMilSec.Location = new System.Drawing.Point(18, 191);
            this.pbMilSec.Name = "pbMilSec";
            this.pbMilSec.Size = new System.Drawing.Size(285, 23);
            this.pbMilSec.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pbMilSec);
            this.Controls.Add(this.buPauseDown);
            this.Controls.Add(this.buPauseUp);
            this.Controls.Add(this.pbDown);
            this.Controls.Add(this.pbUp);
            this.Controls.Add(this.buDown);
            this.Controls.Add(this.buUp);
            this.Name = "Form1";
            this.Text = "labTimer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button buUp;
        private System.Windows.Forms.Button buDown;
        private System.Windows.Forms.ProgressBar pbUp;
        private System.Windows.Forms.ProgressBar pbDown;
        private System.Windows.Forms.Button buPauseUp;
        private System.Windows.Forms.Button buPauseDown;
        private System.Windows.Forms.ProgressBar pbMilSec;
    }
}

