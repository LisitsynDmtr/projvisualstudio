﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTimer
{
    public partial class Form1 : Form
    {

        private readonly Timer tmDown = new Timer();
        private readonly Timer tmUp = new Timer();
        private DateTime startup;
        private int SEС_MAX = 30;
        private DateTime startDown;
        private TimeSpan x_Up;
        private TimeSpan x_Down;
        private bool paused = false;
        private DateTime pauseTime;
        private DateTime contTime;
        private TimeSpan timeoutDown;
        private TimeSpan timeoutUp;

        public Form1()
        {
            InitializeComponent();

            Timer timer = new Timer();
            timer.Interval = 1000;//1s
            timer.Tick += Timer_Tick;
            timer.Start();

            System.Timers.Timer timer1 = new System.Timers.Timer();
            timer1.Interval = 2000;
            timer1.Elapsed += Timer1_Elapsed;
            //timer1.SynchronizingObject = this;
            timer1.Start();

            var dt = DateTime.Now.AddSeconds(30);

            buPauseUp.Click += BuPauseUp_Click;
            tmUp.Interval = 100;
            tmUp.Tick += TmUp_Tick;
            buUp.Click += BuUp_Click;
            pbUp.Maximum = SEС_MAX;
            pbMilSec.Maximum = SEС_MAX * 1000;
           
            buPauseDown.Click += BuPauseDown_Click;
            tmDown.Interval = 100;
            tmDown.Tick += TmDown_Tick;
            buDown.Click += BuDown_Click;
            pbDown.Maximum = SEС_MAX;

            //HW
          

        
        }

        private void BuPauseDown_Click(object sender, EventArgs e)
        {
            if (buPauseDown.Text == "Pause")
            {
               
                tmDown.Enabled = false;
                tmDown.Stop();
                pauseTime = DateTime.Now;
                buPauseDown.Text = "Continue";
            }
            else if (buPauseDown.Text == "Continue")
            {
                contTime = DateTime.Now;
                buPauseDown.Text = "Pause";
                timeoutDown += contTime - pauseTime;
                tmDown.Enabled = true;
                tmDown.Start();
            }
        }

        private void BuPauseUp_Click(object sender, EventArgs e)
        {
           
            if (buPauseUp.Text == "Pause")
            {
                
                tmUp.Enabled = false;
                tmUp.Stop();
                pauseTime = DateTime.Now;
                buPauseUp.Text = "Continue";
            }
            else if (buPauseUp.Text == "Continue") {
                
                contTime = DateTime.Now;
                buPauseUp.Text = "Pause";
                timeoutUp += contTime - pauseTime;
                tmUp.Enabled = true;
                tmUp.Start();
                
                
            }

        }

        private void BuDown_Click(object sender, EventArgs e)
        {
            tmDown.Enabled = !tmDown.Enabled;
            startDown = DateTime.Now;
        }

        private void TmDown_Tick(object sender, EventArgs e)
        {
            x_Down = startDown.AddSeconds(SEС_MAX) - DateTime.Now +timeoutDown ;
            if (x_Down.TotalSeconds < 0)
            {
                tmDown.Stop();
                x_Down = TimeSpan.Zero;
            }
            buDown.Text = x_Down.ToString(@"mm\:ss\.ffff");
            pbDown.Value = SEС_MAX - (int)x_Down.TotalSeconds;

        }

        private void BuUp_Click(object sender, EventArgs e)
        {
            tmUp.Enabled = !tmUp.Enabled;
            startup = DateTime.Now;
        }

        private void TmUp_Tick(object sender, EventArgs e)
        {
                
                x_Up = DateTime.Now - startup - timeoutUp;
               
                if (x_Up.TotalSeconds > SEС_MAX)
                {
                    tmUp.Stop();
                    pbUp.Value = SEС_MAX;
                }
                else
                {
                    pbUp.Value = (int)x_Up.TotalSeconds;
                pbMilSec.Value = (int)x_Up.TotalMilliseconds;
                }
                    buUp.Text = x_Up.ToString(@"mm\:ss\.f");
            
            
        }

        private void Timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //this.Text += "+";
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //this.Text += "_";
        }
    }
}
