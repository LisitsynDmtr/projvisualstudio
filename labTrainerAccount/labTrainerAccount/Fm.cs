﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTrainerAccount
{
    public partial class Fm : Form
    {
        private Game g;
        public Fm()
        {
            InitializeComponent();
            g = new Game();
            g.Change += G_Change;
            g.DoReset();
            buYes.Click += (s, e) => g.DoAnswer(true);
            buNo.Click += (s, e) => g.DoAnswer(false);
            buPass.Click += (s, e) => g.DoContinue();
            buReset.Click += (s, e) => g.resetAll();
        }

        private void G_Change(object sender, EventArgs e)
        {
            laCorrect.Text = $"Верно = {g.CountCorrect}";
            laWrong.Text = $"Неверно = {g.CountWrong}";
            laLevel.Text = $"Уровень: {g.CountLevel}";
            laCode.Text = g.CodeText;
        }
    }
}
