﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

namespace labTrainerAccount
{
    internal class Game
    {
        

        private bool AnswerCorrect;

        public int CountCorrect { get; private set; }
        public int CountWrong { get; private set; }
        public int CountLevel { get; private set; }
        public string[] mathSigns { get; private set; }
        public string CodeText { get; private set; }

        public event EventHandler Change;
        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            CountLevel = 1;
            mathSigns = new string[] { "+","-","/", "*"};
           
            DoContinue();
        }

        public void DoContinue()
        {
            Random rnd = new Random();
            int xValue1 = rnd.Next(20);
            int xValue2 = rnd.Next(20);

            string xSign = mathSigns[rnd.Next(mathSigns.Length)];

            if (CountLevel == 2)
            {
                xValue1 = rnd.Next(20, 50);
                xValue2 = rnd.Next(20, 50);
            }
            if (CountLevel == 3)
            {
                xValue1 = rnd.Next(50, 100);
                xValue2 = rnd.Next(50, 100);
            }
            if (CountLevel == 4)
            {
                xValue1 = rnd.Next(100,200);
                xValue2 = rnd.Next(100,200);
            }

            double xResult = 0;
            switch (xSign)
            {
                case "+":
                    xResult = xValue1 + xValue2;
                    break;
                case "-":
                    xResult = xValue1 - xValue2;
                    break;
                case "*":
                    xResult = xValue1 * xValue2; ;
                    break;
                case "/":
                    xResult = Math.Round((double)xValue1 / (double)xValue2, 2);
                    break;
            }
            double xResultNew = xResult;
            if (rnd.Next(2)== 1)
                xResultNew += rnd.Next(1,7) * (rnd.Next(2) == 1 ? 1: -1);
            AnswerCorrect = xResult == xResultNew;
            CodeText = $"{xValue1} {xSign} {xValue2} = {xResultNew}";
            

            Change?.Invoke(this, EventArgs.Empty);
        }
        public void DoAnswer(bool v) {

            if (v == AnswerCorrect)
            {
                CountCorrect++;

                if ((CountCorrect % 3 == 0) && (CountLevel < 4))
                {
                        CountLevel++;
                   
                }
            }
            else
            {
                CountWrong++;
                CountLevel = 1;
            }
           
            DoContinue();
        }
        public void resetAll() {
            CountCorrect = 0;
            CountWrong = 0;
            CountLevel = 1;
            DoContinue();
        }
    }
}