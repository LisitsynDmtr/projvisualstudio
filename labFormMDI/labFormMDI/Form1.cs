﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFormMDI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            miCreateNEwForm.Click += MiCreateNEwForm_Click;
            //toolStripMenuItem.Click += (s, e) => this.LayoutMdi(MdiLayout.Cascade);
            // toolStripMenuItem2.Click += (s, e) => this.LayoutMdi(MdiLayout.TileHorizontal);
            // toolStripMenuItem2.Click += (s, e) => this.LayoutMdi(MdiLayout.TileVertical);
            //toolStripMenuItem2.Click += (s, e) => this.LayoutMdi(MdiLayout.ArrangeIcons);
            toolStripMenuItem3.Click += (s, e) => this.ActiveMdiChild?.Close();
            toolStripMenuItem4.Click += ToolStripMenuItem4_Click;
            toolStripMenuItem5.Click += (s, e) => new FormAbout().ShowDialog();
        }

        private void ToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            while (this.MdiChildren.Count() > 0)
            {
                this.MdiChildren[0].Close();
            }
        }

        private void MiCreateNEwForm_Click(object sender, EventArgs e)
        {
            var x = new FormNote();
            x.MdiParent = this;
            x.Show();
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
