﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPyatnashki
{
    public partial class Fm : Form
    {
        private Game g;
        int clicks = 0;
        public Fm()
        {
           InitializeComponent();

            g = new Game(4);
            tablePanel.Visible = false;
            toolStripMenuItem3.Click += ToolStripMenuItem3_Click;
            toolStripMenuItem4.Click += ToolStripMenuItem4_Click;
            laCount.Visible = false;

            //уровни сложности 3x3, 5x5,4x4
            //перемешивание с указанием кол-ва ходов
            //управление мышкой и клавиатурой + перемещение рядов
            //размер формы

          
           
        }

        private void ToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            clicks = 0;
            g.start();
            for (int i = 0; i < 50; i++)
            {
                g.random();
            }
            refresh_All();
        }

        private void ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            laCount.Visible = true;
            tablePanel.Visible = true;
            toolStripMenuItem4.Enabled = true;
            toolStripMenuItem3.Enabled = false;
            g.start();
            for (int i = 0; i < 50; i++)
            {
                g.random();
            }
            refresh_All();
        }

        private void refresh_All() {

            for (int i = 0; i < 16; i++)
            {
                button(i).Text = g.get_num(i).ToString();
                button(i).Font = new Font("Segoe UI", 20);
                button(i).Visible = g.get_num(i) > 0;
            }
        }

       
        private Button button(int position) {

            switch (position) {
                case 0: return button0;
                case 1: return button1;
                case 2: return button2;
                case 3: return button3;
                case 4: return button4;
                case 5: return button5;
                case 6: return button6;
                case 7: return button7;
                case 8: return button8;
                case 9: return button9;
                case 10: return button10;
                case 11: return button11;
                case 12: return button12;
                case 13: return button13;
                case 14: return button14;
                case 15: return button15;
                default: return null;
            }
        }

        private void Fm_Click(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                clicks++;
                laCount.Text = "Ходов: " + clicks.ToString();
                int tag = Convert.ToInt32(x.Tag);
                //MessageBox.Show(tag.ToString());
                g.shift(tag);
                refresh_All();
                if (g.check())
                    MessageBox.Show("Вы победили!");
            }
        }
    }
}
