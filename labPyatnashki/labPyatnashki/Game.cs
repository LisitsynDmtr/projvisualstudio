﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace labPyatnashki
{
    internal class Game
    {
        static Random rnd = new Random();
        int size;
        int[,] map;
        int x_space, y_space;
        public Game( int size)
        {
            if (size < 2)
            {
                size = 2;
            }
            if (size > 5) size = 5;
            this.size = size;
            map = new int [size, size];
        }

        public void start() {

            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    map[x, y] = coords(x, y) + 1;
                }
            }
            x_space = size -1   ;
            y_space = size -1  ;
            map[x_space, y_space] = 0;
        }
        public bool check() {
            if (y_space != size - 1 && x_space != size - 1)
                return false;
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                {
                    if (x != size - 1 && y != size - 1) {
                        if (map[x, y] != coords(x, y) + 1)
                            return false;
                           
                    }
                }
        
            return true;
        }
        public void shift(int position) {

            int x, y;
            position_to(position, out x, out y);
            if (Math.Abs (x_space - x) + Math.Abs (y_space -y) !=1)
            {
                return;
            }
            map[x_space, y_space] = map[x, y];
            map[x, y] = 0;
            x_space = x;
            y_space = y;
        }
        public void random() {

            int t = rnd.Next(0, 4);
            int x = x_space;
            int y = y_space;
            switch (t) {
                case 0: x--;break;
                case 1: x++; break;
                case 2: y--; break;
                case 3: y++; break;
            }
            shift(coords(x,y));
        }
        public int get_num(int position) {

            int x, y;
            position_to(position, out x, out y);
            if (x < 0 || x > size) return 0;
            if (y < 0 || y > size) return 0;
            return map[x, y];
        }

        private int  coords(int x, int y) {
            if (x< 0)
            {
                x = 0;
            }
            if (x > size-1)
            {
                x = size -1;
            }
            if (y< 0)
            {
                y = 0;
            }
            if (y > size-1)
            {
                y = size - 1;
            }
            return y * size + x;
        
        }
        private void position_to(int position, out int x, out int y) {
            if (position < 0)
            {
                position = 0;
            }
            if (position > size* size - 1)
            {
                position = size * size - 1;
            }
           
            x = position % size;
            y = position / size;
        
        }
        
    }
}