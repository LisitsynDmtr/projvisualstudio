﻿namespace labPyatnashki
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.laCount = new System.Windows.Forms.Label();
            this.tablePanel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tablePanel
            // 
            this.tablePanel.ColumnCount = 4;
            this.tablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.68041F));
            this.tablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.31959F));
            this.tablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 213F));
            this.tablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 227F));
            this.tablePanel.Controls.Add(this.button15, 3, 3);
            this.tablePanel.Controls.Add(this.button14, 2, 3);
            this.tablePanel.Controls.Add(this.button13, 1, 3);
            this.tablePanel.Controls.Add(this.button12, 0, 3);
            this.tablePanel.Controls.Add(this.button11, 3, 2);
            this.tablePanel.Controls.Add(this.button10, 2, 2);
            this.tablePanel.Controls.Add(this.button9, 1, 2);
            this.tablePanel.Controls.Add(this.button8, 0, 2);
            this.tablePanel.Controls.Add(this.button7, 3, 1);
            this.tablePanel.Controls.Add(this.button6, 2, 1);
            this.tablePanel.Controls.Add(this.button5, 1, 1);
            this.tablePanel.Controls.Add(this.button4, 0, 1);
            this.tablePanel.Controls.Add(this.button3, 3, 0);
            this.tablePanel.Controls.Add(this.button2, 2, 0);
            this.tablePanel.Controls.Add(this.button0, 0, 0);
            this.tablePanel.Controls.Add(this.button1, 1, 0);
            this.tablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel.Location = new System.Drawing.Point(0, 28);
            this.tablePanel.Name = "tablePanel";
            this.tablePanel.RowCount = 4;
            this.tablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.1194F));
            this.tablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.8806F));
            this.tablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tablePanel.Size = new System.Drawing.Size(828, 509);
            this.tablePanel.TabIndex = 3;
            this.tablePanel.Visible = false;
            // 
            // button15
            // 
            this.button15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button15.Location = new System.Drawing.Point(603, 382);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(222, 124);
            this.button15.TabIndex = 1;
            this.button15.Tag = "15";
            this.button15.Text = "button16";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button14
            // 
            this.button14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button14.Location = new System.Drawing.Point(390, 382);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(207, 124);
            this.button14.TabIndex = 1;
            this.button14.Tag = "1";
            this.button14.Text = "button15";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button13
            // 
            this.button13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button13.Location = new System.Drawing.Point(187, 382);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(197, 124);
            this.button13.TabIndex = 1;
            this.button13.Tag = "13";
            this.button13.Text = "button14";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button12
            // 
            this.button12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button12.Location = new System.Drawing.Point(3, 382);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(178, 124);
            this.button12.TabIndex = 1;
            this.button12.Tag = "12";
            this.button12.Text = "button13";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button11
            // 
            this.button11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button11.Location = new System.Drawing.Point(603, 242);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(222, 134);
            this.button11.TabIndex = 1;
            this.button11.Tag = "11";
            this.button11.Text = "button12";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button10
            // 
            this.button10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button10.Location = new System.Drawing.Point(390, 242);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(207, 134);
            this.button10.TabIndex = 1;
            this.button10.Tag = "10";
            this.button10.Text = "button11";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button9
            // 
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.Location = new System.Drawing.Point(187, 242);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(197, 134);
            this.button9.TabIndex = 1;
            this.button9.Tag = "9";
            this.button9.Text = "button10";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Location = new System.Drawing.Point(3, 242);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(178, 134);
            this.button8.TabIndex = 1;
            this.button8.Tag = "8";
            this.button8.Text = "button9";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Location = new System.Drawing.Point(603, 125);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(222, 111);
            this.button7.TabIndex = 1;
            this.button7.Tag = "7";
            this.button7.Text = "button8";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button6
            // 
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Location = new System.Drawing.Point(390, 125);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(207, 111);
            this.button6.TabIndex = 1;
            this.button6.Tag = "6";
            this.button6.Text = "button7";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.Location = new System.Drawing.Point(187, 125);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(197, 111);
            this.button5.TabIndex = 1;
            this.button5.Tag = "5";
            this.button5.Text = "button6";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(3, 125);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(178, 111);
            this.button4.TabIndex = 1;
            this.button4.Tag = "4";
            this.button4.Text = "button5";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Location = new System.Drawing.Point(603, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(222, 116);
            this.button3.TabIndex = 1;
            this.button3.Tag = "3";
            this.button3.Text = "button4";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(390, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(207, 116);
            this.button2.TabIndex = 1;
            this.button2.Tag = "2";
            this.button2.Text = "button3";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button0
            // 
            this.button0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button0.Location = new System.Drawing.Point(3, 3);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(178, 116);
            this.button0.TabIndex = 0;
            this.button0.Tag = "0";
            this.button0.Text = "button1";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.Fm_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(187, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(197, 116);
            this.button1.TabIndex = 1;
            this.button1.Tag = "1";
            this.button1.Text = "button2";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Fm_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(828, 28);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(107, 24);
            this.toolStripMenuItem3.Text = "Начать игру";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Enabled = false;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(126, 24);
            this.toolStripMenuItem4.Text = "Начать заново";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(109, 24);
            this.toolStripMenuItem1.Text = "Начать Игру";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(126, 24);
            this.toolStripMenuItem2.Text = "Начать заново";
            // 
            // laCount
            // 
            this.laCount.AutoSize = true;
            this.laCount.Location = new System.Drawing.Point(709, 5);
            this.laCount.Name = "laCount";
            this.laCount.Size = new System.Drawing.Size(55, 20);
            this.laCount.TabIndex = 5;
            this.laCount.Text = "Ходов:";
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 537);
            this.Controls.Add(this.laCount);
            this.Controls.Add(this.tablePanel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Fm";
            this.Text = "labPyatnashki";
            this.Click += new System.EventHandler(this.Fm_Click);
            this.tablePanel.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tablePanel;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.Label laCount;
    }
}

