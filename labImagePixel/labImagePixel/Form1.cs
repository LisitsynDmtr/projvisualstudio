﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImagePixel
{
    public partial class Form1 : Form
    {
        private Color curColor;

        public Form1()
        {
            InitializeComponent();
            pxImage.MouseMove += PxImage_MouseMove;
            buImageGray.Click += BuImageGray_Click;
            buImageInvert.Click += BuImageInvert_Click;
            trDelta.Maximum = 255*3;
            trDelta.Value = 100;
        }   

        private void BuImageInvert_Click(object sender, EventArgs e)
        {
            var bitmap = new Bitmap(pxImage.Image);
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    var color = bitmap.GetPixel(i, j);
                   
                    bitmap.SetPixel(i, j, Color.FromArgb(255-color.R, 255-color.G, 255-color.B));
                }
            }
            pxResult.Image = new Bitmap(bitmap);
        }

        private void BuImageGray_Click(object sender, EventArgs e)
        {
            var bitmap = new Bitmap(pxImage.Image);
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    var color = bitmap.GetPixel(i, j);
                    var gray = (color.R + color.G + color.B) / 3;
                    bitmap.SetPixel(i, j, Color.FromArgb(gray, gray, gray));
                }
            }
            pxResult.Image = new Bitmap(bitmap);
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            var bitmap = new Bitmap(pxImage.Image);
            curColor = bitmap.GetPixel(e.X, e.Y);
            pxCurColor.BackColor = curColor;
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    var color = bitmap.GetPixel(i, j);

                    // bitmap.SetPixel(i, j,(color == curColor)?color : Color.Transparent);
                    var v =
                        (Math.Max(curColor.R, color.R) - Math.Min(curColor.R, color.R) +
                        (Math.Max(curColor.G, color.G) - Math.Min(curColor.G, color.G) +
                        (Math.Max(curColor.B, color.B) - Math.Min(curColor.B, color.B))));

                     bitmap.SetPixel(i, j, (v < trDelta.Value) ? color : Color.Transparent);
                
                }
            }
            pxResult.Image = new Bitmap(bitmap);
            this.Text = $"{Application.ProductName} : {curColor}";
        }
    }
}
