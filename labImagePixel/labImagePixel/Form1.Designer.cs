﻿
namespace labImagePixel
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buImageGray = new System.Windows.Forms.Button();
            this.buImageInvert = new System.Windows.Forms.Button();
            this.pxResult = new System.Windows.Forms.PictureBox();
            this.pxCurColor = new System.Windows.Forms.PictureBox();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.trDelta = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.pxResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxCurColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trDelta)).BeginInit();
            this.SuspendLayout();
            // 
            // buImageGray
            // 
            this.buImageGray.Location = new System.Drawing.Point(490, 431);
            this.buImageGray.Name = "buImageGray";
            this.buImageGray.Size = new System.Drawing.Size(85, 26);
            this.buImageGray.TabIndex = 3;
            this.buImageGray.Text = "button1";
            this.buImageGray.UseVisualStyleBackColor = true;
            // 
            // buImageInvert
            // 
            this.buImageInvert.Location = new System.Drawing.Point(595, 431);
            this.buImageInvert.Name = "buImageInvert";
            this.buImageInvert.Size = new System.Drawing.Size(85, 26);
            this.buImageInvert.TabIndex = 4;
            this.buImageInvert.Text = "button2";
            this.buImageInvert.UseVisualStyleBackColor = true;
            // 
            // pxResult
            // 
            this.pxResult.Location = new System.Drawing.Point(403, 12);
            this.pxResult.Name = "pxResult";
            this.pxResult.Size = new System.Drawing.Size(385, 392);
            this.pxResult.TabIndex = 2;
            this.pxResult.TabStop = false;
            // 
            // pxCurColor
            // 
            this.pxCurColor.Location = new System.Drawing.Point(355, 410);
            this.pxCurColor.Name = "pxCurColor";
            this.pxCurColor.Size = new System.Drawing.Size(81, 77);
            this.pxCurColor.TabIndex = 1;
            this.pxCurColor.TabStop = false;
            // 
            // pxImage
            // 
            this.pxImage.Image = global::labImagePixel.Properties.Resources.img1;
            this.pxImage.Location = new System.Drawing.Point(12, 12);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(376, 392);
            this.pxImage.TabIndex = 0;
            this.pxImage.TabStop = false;
            // 
            // trDelta
            // 
            this.trDelta.Location = new System.Drawing.Point(36, 442);
            this.trDelta.Name = "trDelta";
            this.trDelta.Size = new System.Drawing.Size(234, 45);
            this.trDelta.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 547);
            this.Controls.Add(this.trDelta);
            this.Controls.Add(this.buImageInvert);
            this.Controls.Add(this.buImageGray);
            this.Controls.Add(this.pxResult);
            this.Controls.Add(this.pxCurColor);
            this.Controls.Add(this.pxImage);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pxResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxCurColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trDelta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.PictureBox pxCurColor;
        private System.Windows.Forms.PictureBox pxResult;
        private System.Windows.Forms.Button buImageGray;
        private System.Windows.Forms.Button buImageInvert;
        private System.Windows.Forms.TrackBar trDelta;
    }
}

