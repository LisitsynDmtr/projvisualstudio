﻿namespace labClock
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelClock = new System.Windows.Forms.Label();
            this.buChange = new System.Windows.Forms.Button();
            this.labelDayOfWeek = new System.Windows.Forms.Label();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelDayOfMonth = new System.Windows.Forms.Label();
            this.labelMonth = new System.Windows.Forms.Label();
            this.labelDayOfYear = new System.Windows.Forms.Label();
            this.textChangeMinute = new System.Windows.Forms.TextBox();
            this.textChangeHour = new System.Windows.Forms.TextBox();
            this.textChangeSecond = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelClock
            // 
            this.labelClock.AutoSize = true;
            this.labelClock.Font = new System.Drawing.Font("Segoe UI", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelClock.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelClock.Location = new System.Drawing.Point(234, 42);
            this.labelClock.Name = "labelClock";
            this.labelClock.Size = new System.Drawing.Size(337, 106);
            this.labelClock.TabIndex = 0;
            this.labelClock.Text = "00:00:00";
            // 
            // buChange
            // 
            this.buChange.Location = new System.Drawing.Point(335, 390);
            this.buChange.Name = "buChange";
            this.buChange.Size = new System.Drawing.Size(111, 37);
            this.buChange.TabIndex = 1;
            this.buChange.Text = "Change time";
            this.buChange.UseVisualStyleBackColor = true;
            this.buChange.UseWaitCursor = true;
            // 
            // labelDayOfWeek
            // 
            this.labelDayOfWeek.AutoSize = true;
            this.labelDayOfWeek.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelDayOfWeek.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelDayOfWeek.Location = new System.Drawing.Point(96, 193);
            this.labelDayOfWeek.Name = "labelDayOfWeek";
            this.labelDayOfWeek.Size = new System.Drawing.Size(241, 59);
            this.labelDayOfWeek.TabIndex = 2;
            this.labelDayOfWeek.Text = "dayOfWeek";
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Font = new System.Drawing.Font("Segoe UI", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelYear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelYear.Location = new System.Drawing.Point(526, 193);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(105, 59);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "year";
            // 
            // labelDayOfMonth
            // 
            this.labelDayOfMonth.AutoSize = true;
            this.labelDayOfMonth.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelDayOfMonth.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelDayOfMonth.Location = new System.Drawing.Point(214, 261);
            this.labelDayOfMonth.Name = "labelDayOfMonth";
            this.labelDayOfMonth.Size = new System.Drawing.Size(167, 37);
            this.labelDayOfMonth.TabIndex = 4;
            this.labelDayOfMonth.Text = "dayOfMonth";
            // 
            // labelMonth
            // 
            this.labelMonth.AutoSize = true;
            this.labelMonth.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelMonth.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelMonth.Location = new System.Drawing.Point(84, 261);
            this.labelMonth.Name = "labelMonth";
            this.labelMonth.Size = new System.Drawing.Size(96, 37);
            this.labelMonth.TabIndex = 5;
            this.labelMonth.Text = "Month";
            // 
            // labelDayOfYear
            // 
            this.labelDayOfYear.AutoSize = true;
            this.labelDayOfYear.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelDayOfYear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelDayOfYear.Location = new System.Drawing.Point(462, 261);
            this.labelDayOfYear.Name = "labelDayOfYear";
            this.labelDayOfYear.Size = new System.Drawing.Size(138, 37);
            this.labelDayOfYear.TabIndex = 6;
            this.labelDayOfYear.Text = "dayOfYear";
            // 
            // textChangeMinute
            // 
            this.textChangeMinute.Font = new System.Drawing.Font("Segoe UI", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textChangeMinute.Location = new System.Drawing.Point(354, 42);
            this.textChangeMinute.Multiline = true;
            this.textChangeMinute.Name = "textChangeMinute";
            this.textChangeMinute.Size = new System.Drawing.Size(92, 106);
            this.textChangeMinute.TabIndex = 8;
            this.textChangeMinute.Visible = false;
            // 
            // textChangeHour
            // 
            this.textChangeHour.Font = new System.Drawing.Font("Segoe UI", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textChangeHour.Location = new System.Drawing.Point(245, 42);
            this.textChangeHour.Multiline = true;
            this.textChangeHour.Name = "textChangeHour";
            this.textChangeHour.Size = new System.Drawing.Size(92, 106);
            this.textChangeHour.TabIndex = 9;
            this.textChangeHour.Visible = false;
            // 
            // textChangeSecond
            // 
            this.textChangeSecond.Font = new System.Drawing.Font("Segoe UI", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textChangeSecond.Location = new System.Drawing.Point(462, 42);
            this.textChangeSecond.Multiline = true;
            this.textChangeSecond.Name = "textChangeSecond";
            this.textChangeSecond.Size = new System.Drawing.Size(92, 106);
            this.textChangeSecond.TabIndex = 10;
            this.textChangeSecond.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textChangeSecond);
            this.Controls.Add(this.textChangeHour);
            this.Controls.Add(this.textChangeMinute);
            this.Controls.Add(this.labelDayOfYear);
            this.Controls.Add(this.labelMonth);
            this.Controls.Add(this.labelDayOfMonth);
            this.Controls.Add(this.labelYear);
            this.Controls.Add(this.labelDayOfWeek);
            this.Controls.Add(this.buChange);
            this.Controls.Add(this.labelClock);
            this.Name = "Form1";
            this.Text = "labClock";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelClock;
        private System.Windows.Forms.Button buChange;
        private System.Windows.Forms.Label labelDayOfWeek;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Label labelDayOfMonth;
        private System.Windows.Forms.Label labelMonth;
        private System.Windows.Forms.Label labelDayOfYear;
        private System.Windows.Forms.TextBox textChangeMinute;
        private System.Windows.Forms.TextBox textChangeHour;
        private System.Windows.Forms.TextBox textChangeSecond;
    }
}

