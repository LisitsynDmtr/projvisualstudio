﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labClock
{
    public partial class Form1 : Form
    {
            Timer timer = new Timer();
            Clock clock = new Clock();
            Timer change = new Timer();
        private DateTime oDate;
        private DateTime newDate;

        public Form1()
        {
            InitializeComponent();

            timer.Interval = 1000;
            timer.Tick += Timer_Tick;
            timer.Start();
            buChange.Click += BuChange_Click;
            change.Interval = 1000;
            change.Tick += Change_Tick;

        }

        private void Change_Tick(object sender, EventArgs e)
        {
            oDate = oDate.AddSeconds(1);
            newDate = oDate;
            var hours = newDate.Hour < 10 ? "0" + newDate.Hour.ToString() : newDate.Hour.ToString();
            var minute = newDate.Minute < 10 ? "0" + newDate.Minute.ToString() : newDate.Minute.ToString();
            var second = newDate.Second < 10 ? "0" + newDate.Second.ToString() : newDate.Second.ToString();

            labelClock.Text = hours + ":" + minute + ":" + second;
        }

        private void BuChange_Click(object sender, EventArgs e)
        {
            if (timer.Enabled) { timer.Stop(); timer.Enabled = false; }

            if (buChange.Text == "Change time")
            {
                textChangeHour.Visible = true;
                textChangeMinute.Visible = true;
                textChangeSecond.Visible = true;
                buChange.Text = "Save time";

            }
            else if (buChange.Text == "Save time")
            {
                buChange.Text = "Change time";
                textChangeHour.Visible = false;
                textChangeMinute.Visible = false;
                textChangeSecond.Visible = false;
                try
                {
                    string itime = textChangeHour.Text+":"+ textChangeMinute.Text + ":"+ textChangeSecond.Text;
                    oDate = DateTime.ParseExact(itime, "h:m:s", null);

                }
                catch
                {
                    MessageBox.Show("invalid data");
                    
                    timer.Enabled = true;
                    timer.Start();
                    return;
                }
                change.Start();
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            labelClock.Text = clock.getHours()+":"+clock.getMinutes() + ":"+ clock.getSeconds();
            labelDayOfWeek.Text = clock.getDayOfWeek();
            labelDayOfMonth.Text = clock.getDayOfMonth()+ "-ое";
            labelMonth.Text = clock.getMonth();
            labelYear.Text = clock.getYear() + "год";
            labelDayOfYear.Text = "До конца года: " + clock.getDayOfYear() + "дней";
        }
    }
}
