﻿using System;

namespace labClock
{
    internal class Clock
    {

        public string getHours()
        {
            var x = DateTime.Now.Hour < 10 ? "0" + DateTime.Now.Hour.ToString() : DateTime.Now.Hour.ToString();
            return x;
        }
        public string getMinutes()
        {
            var x = DateTime.Now.Minute < 10 ? "0" + DateTime.Now.Minute.ToString() : DateTime.Now.Minute.ToString();
            return x;

        }
        public string getSeconds()
        {
            var x = DateTime.Now.Second < 10 ? "0" + DateTime.Now.Second.ToString() : DateTime.Now.Second.ToString();
            return x;
        }
        public string getDayOfWeek()
        {
            return DateTime.Now.ToString("dddd");
        }
        public string getYear()
        {
            return DateTime.Now.ToString("yyyy");
        }
        public string getDayOfMonth()
        {
            return DateTime.Now.ToString("dd");
        }
        public string getMonth()
        {
            return DateTime.Now.ToString("MMMM");
        }
        public string getDayOfYear()
        {
            var x = DateTime.IsLeapYear(DateTime.Now.Year) ? 366 - DateTime.Now.DayOfYear : 365 - DateTime.Now.DayOfYear;
            return x.ToString();
        }



    }



}