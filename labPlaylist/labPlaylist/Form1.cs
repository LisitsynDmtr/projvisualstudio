﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPlaylist
{
    public partial class Form1 : Form
    {
        private const string PATH_FILE = @"C:\моргенштек.m3u8";
        private string[] textFromFile;

        public Form1()
        {
            InitializeComponent();

            if (File.Exists(PATH_FILE))
            {
                // edTemp.Text = File.ReadAllText(PATH_FILE, Encoding.Unicode);
                textFromFile = File.ReadAllText(PATH_FILE, Encoding.Unicode).Split(Environment.NewLine);
                //edTemp.Text = string.Join(Environment.NewLine, textFromFile);

                textFromFile = textFromFile.Where(v => !v.StartsWith("#"))
                    .Select(v => Path.Combine(Directory.GetParent(PATH_FILE).ToString(), v))
                    .ToArray();

                lv.View = View.List;
                foreach (var item in textFromFile)
                {
                    lv.Items.Add(item);
                }

                edTemp.Text = string.Join(Environment.NewLine, textFromFile);
                var xPathNew = Path.Combine(Directory.GetParent(PATH_FILE).ToString(), "OUT");
                Directory.CreateDirectory(xPathNew);
                var n = 1;
                foreach (var item in textFromFile)
                {
                    var xItemNew = Path.Combine(xPathNew, $"{n++} - {Path.GetFileName(item)}");
                    File.Copy(item, xItemNew, true);
                }
            }
            //hw
            //механизм загрузки файла
            //загурзка файлов других форматов
            //загрузка плейлиста из текстого файла(там только пути к файлам)
            //по кнопкам на форме изменять порядок элементов и удаление их из плейлиста 
            //переещать элементы в ListView с помощью мышки
            //по клавише alt + up/down перемещать элементы
            //добавить механизм выбора папки для выгрузки резльтата (проверка наличия папки, файлов, ...)
            //добавить прогрессбар в механизм выгрузки файлов(распределить)
            //отобразить столбцы для отобр признака существования файла, размера, длины трека и тд



        }
    }
}
