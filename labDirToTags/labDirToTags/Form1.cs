﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labDirToTags
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();


            //edDir.Text = Directory.GetCurrentDirectory();
            edDir.Text = @"D:\фото\МосПолитех\2020.11.25 Семинар ""ООП"" (Иванов, Петров)\20201125-1310-001.jpg";

            var s = edDir.Text.Split(new char[] {' ', '\\' }).ToArray();
            s = s.Select(v => v.TrimStart(new char[] { '(', '{', '"' })
            .TrimEnd(new char[] { ')', '}', '"' })).ToArray();


            edTags.Text = string.Join(Environment.NewLine, s);

            //HW
            //string[] tags = irToTags("D:\МосПолитех");
            
        }

    }
}
