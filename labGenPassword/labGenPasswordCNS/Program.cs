﻿using labGenPassword_Core;
using System;
using System.Runtime.CompilerServices;

namespace labGenPasswordCNS
{
    class Program
    {
       static string x;
        private static int pasLength;
        private static bool x1;
        private static bool x2;
        private static bool x3;
        private static bool x4;
        private static string t;

        static void Main(string[] args)
        {
            bool a = true;
            
            param();
           
            while (a)
            {
                Console.WriteLine(Utils.RandomStr(pasLength, x1, x2, x3, x4));

                Console.WriteLine("Продолжить? Y/N " +
                "(Поменять параметры: change) ");

                x = Console.ReadLine();

                if (x == "change")
                    param();

                if (((x != "Y") && (x != "y")) && ((x != "N") && (x != "n")))
                {
                    Console.WriteLine("Введите корректные данные");
                    x = Console.ReadLine();
                }
                 

                if (x == "N" || x == "n") { a = false; Environment.Exit(0); }

                Console.Clear();
            }
            
        }
        static void param()
        {
            Console.WriteLine("Выберите параметры пароля");
           
            Console.WriteLine("Длина пароля: ");
            t = Console.ReadLine();
            while (true)
            {
                if (Int32.TryParse(t, out pasLength))
                {
                    pasLength = Convert.ToInt32(t);
                    break;
                }
                else
                {
                    Console.WriteLine("Введите корректные данные");
                    t = Console.ReadLine();
                }
            }  

            Console.WriteLine("Символы в нижнем регистре? Y/N");
            t = Console.ReadLine();
            while (((t != "Y") && (t != "y")) && ((t != "N") && (t != "n")))
            {
                Console.WriteLine("Введите корректные данные");
                t = Console.ReadLine();
            }
                x1 = (t == "Y" || t =="y") ? true : false; 
           
            Console.WriteLine("Символы в верхнем регистре? Y/N");
            t = Console.ReadLine();
            while (((t != "Y") && (t != "y")) && ((t != "N") && (t != "n")))
            {
                Console.WriteLine("Введите корректные данные");
                t = Console.ReadLine();
            }
            x2 = (t == "Y" || t == "y") ? true : false;

            Console.WriteLine("Цифры? Y/N");
            t = Console.ReadLine();
            while (((t != "Y") && (t != "y")) && ((t != "N") && (t != "n")))
            {
                Console.WriteLine("Введите корректные данные");
                t = Console.ReadLine();
            }
            x3 = (t == "Y" || t == "y") ? true : false;


            Console.WriteLine("Специальные символы? Y/N");
            t = Console.ReadLine();
            while (((t != "Y") && (t != "y")) && ((t != "N") && (t != "n")))
            {
                Console.WriteLine("Введите корректные данные");
                t = Console.ReadLine();
            }
            x4 = (t == "Y" || t == "y") ? true : false;


            Console.Clear();
        }
    }
}
