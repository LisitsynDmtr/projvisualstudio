﻿namespace labGenPassword
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.edPassword = new System.Windows.Forms.TextBox();
            this.buPassword = new System.Windows.Forms.Button();
            this.ckLower = new System.Windows.Forms.CheckBox();
            this.ckUpper = new System.Windows.Forms.CheckBox();
            this.ckNumr = new System.Windows.Forms.CheckBox();
            this.ckSpec = new System.Windows.Forms.CheckBox();
            this.edLength = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.edLength)).BeginInit();
            this.SuspendLayout();
            // 
            // edPassword
            // 
            this.edPassword.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.edPassword.Location = new System.Drawing.Point(26, 24);
            this.edPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.edPassword.Name = "edPassword";
            this.edPassword.Size = new System.Drawing.Size(664, 36);
            this.edPassword.TabIndex = 0;
            this.edPassword.Text = "<Password>";
            this.edPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buPassword
            // 
            this.buPassword.Location = new System.Drawing.Point(26, 75);
            this.buPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buPassword.Name = "buPassword";
            this.buPassword.Size = new System.Drawing.Size(663, 52);
            this.buPassword.TabIndex = 1;
            this.buPassword.Text = "Генерировать";
            this.buPassword.UseVisualStyleBackColor = true;
            // 
            // ckLower
            // 
            this.ckLower.AutoSize = true;
            this.ckLower.Location = new System.Drawing.Point(26, 149);
            this.ckLower.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ckLower.Name = "ckLower";
            this.ckLower.Size = new System.Drawing.Size(188, 19);
            this.ckLower.TabIndex = 2;
            this.ckLower.Text = "Символы в нижнем регистре";
            this.ckLower.UseVisualStyleBackColor = true;
            // 
            // ckUpper
            // 
            this.ckUpper.AutoSize = true;
            this.ckUpper.Location = new System.Drawing.Point(26, 184);
            this.ckUpper.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ckUpper.Name = "ckUpper";
            this.ckUpper.Size = new System.Drawing.Size(190, 19);
            this.ckUpper.TabIndex = 3;
            this.ckUpper.Text = "Символы в верхнем регистре";
            this.ckUpper.UseVisualStyleBackColor = true;
            // 
            // ckNumr
            // 
            this.ckNumr.AutoSize = true;
            this.ckNumr.Location = new System.Drawing.Point(26, 217);
            this.ckNumr.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ckNumr.Name = "ckNumr";
            this.ckNumr.Size = new System.Drawing.Size(67, 19);
            this.ckNumr.TabIndex = 4;
            this.ckNumr.Text = "Цифры";
            this.ckNumr.UseVisualStyleBackColor = true;
            // 
            // ckSpec
            // 
            this.ckSpec.AutoSize = true;
            this.ckSpec.Location = new System.Drawing.Point(26, 250);
            this.ckSpec.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ckSpec.Name = "ckSpec";
            this.ckSpec.Size = new System.Drawing.Size(156, 19);
            this.ckSpec.TabIndex = 5;
            this.ckSpec.Text = "Специальные символы";
            this.ckSpec.UseVisualStyleBackColor = true;
            // 
            // edLength
            // 
            this.edLength.Location = new System.Drawing.Point(140, 281);
            this.edLength.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.edLength.Name = "edLength";
            this.edLength.Size = new System.Drawing.Size(131, 23);
            this.edLength.TabIndex = 6;
            this.edLength.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 283);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Длина пароля";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 338);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.edLength);
            this.Controls.Add(this.ckSpec);
            this.Controls.Add(this.ckNumr);
            this.Controls.Add(this.ckUpper);
            this.Controls.Add(this.ckLower);
            this.Controls.Add(this.buPassword);
            this.Controls.Add(this.edPassword);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.edLength)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edPassword;
        private System.Windows.Forms.Button buPassword;
        private System.Windows.Forms.CheckBox ckLower;
        private System.Windows.Forms.CheckBox ckUpper;
        private System.Windows.Forms.CheckBox ckNumr;
        private System.Windows.Forms.CheckBox ckSpec;
        private System.Windows.Forms.NumericUpDown edLength;
        private System.Windows.Forms.Label label1;
    }
}

