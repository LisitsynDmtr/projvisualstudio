﻿using System;
using System.Text;

namespace labGenPassword_Core
{
    public class Utils
    {

        public static string RandomStr(int length, bool lower, bool upper, bool numbr, bool specl)
        {

            string c1 = "abcdefghijklmnopqrstuvwxyz";
            string c2 = "0123456789";
            string c3 = "{}[]<>.%$#@";

            var x = new StringBuilder(); // набор символов 
            var xResult = new StringBuilder();
            var rnd = new Random();

            //Создать набор символов в соотсветствии с параметрами функции

            if (lower) x.Append(c1);
            if (upper) x.Append(c1.ToUpper());
            if (numbr) x.Append(c2);
            if (specl) x.Append(c3);
            // Если набор пустой - включаем символы нижнего регистра
            if (x.ToString() == "") x.Append(c1);


            while (xResult.Length < length)
                xResult.Append(x[rnd.Next(x.Length)]);

            return xResult.ToString();      
        }

    }
}
