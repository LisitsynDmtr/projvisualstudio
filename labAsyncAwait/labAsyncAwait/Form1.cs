﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labAsyncAwait
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button1.Click += Button1_Click;
            button2.Click += Button2_Click;
            button3.Click += Button3_Click;
        }

       async  private void Button3_Click(object sender, EventArgs e)
        {
            button3.Enabled = false;
            for (int i = 0; i < 10; i++)
            {
                await Task.Delay(600);
                button3.Text = i.ToString();
            }
            button3.Text = "END";
            button3.Enabled = true;
        }

       async private void Button2_Click(object sender, EventArgs e)
        {
            button2.Text = "жди...";
            await Task.Delay(2000);
            button2.Text = DateTime.Now.ToString();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            button1.Text = "жди...";
            Thread.Sleep(2000);
            button1.Text = DateTime.Now.ToString();
        }
    }
}
