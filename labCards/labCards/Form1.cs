﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Http.Headers;
using System.Windows.Forms;

namespace labCards
{
    public partial class Form1 : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private readonly CardPack cardPack;
        private readonly ImageBox imageBox;
        private int cardCount = 6;
        private bool randomCards;
        private bool veerCards;
        private double grad = -10;
        private int smeh;

        public Form1()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);



            imageBox = new ImageBox(Properties.Resources.carti, 4, 13);

            cardPack = new CardPack(imageBox.Count);
            //imageBox = new ImageBox(Properties.Resources.cards2, 5, 13,4*13 + 6);
            //hw


            //отдельный класс для хранения набора карт у игрока(index)


            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);
            trackBar.ValueChanged += TrackBar_ValueChanged;
            this.Click += Form1_Click;
            but1.Click += But1_Click;
            but2.Click += But2_Click;

        }

        private void TrackBar_ValueChanged(object sender, EventArgs e)
        {
            Form1_Click(sender, e);
        }

        private void But2_Click(object sender, EventArgs e)
        {
            g.Clear(SystemColors.Control);
            randomCards = false;
            veerCards = true;
        }

        private void But1_Click(object sender, EventArgs e)
        {
            g.Clear(SystemColors.Control);
            randomCards = true;
            veerCards = false;
        }

        private void Form1_Click(object sender, EventArgs e)
        {
            g.Clear(SystemColors.Control);
            Random rnd = new Random();
            // g.DrawImage(imageBox[rnd.Next(imageBox.Count)], 0, 0);
            if (randomCards == true && veerCards == false)
            {
                for (int i = 0; i < cardCount; i++)
                {

                    g.DrawImage(imageBox[cardPack[i]],
                    rnd.Next(this.ClientSize.Width), rnd.Next(this.ClientSize.Height));
                }
                cardPack.CardRandom();
                this.Invalidate();

            }
            else if (veerCards == true && randomCards == false)
            {
                grad = trackBar.Value;
               
                g.Clear(SystemColors.Control);
                for (int i = 0; i < cardCount; i++)
                {
               
                    g.TranslateTransform(this.ClientSize.Width / 2, this.ClientSize.Height - imageBox[cardPack[i]].Height);
                    g.RotateTransform((float)(grad));
                    g.TranslateTransform(-this.ClientSize.Width / 2, -this.ClientSize.Height + imageBox[cardPack[i]].Height);

                  
                    g.DrawImage(imageBox[cardPack[i]], this.ClientSize.Width / 2, this.ClientSize.Height - imageBox[cardPack[i]].Height);

                    grad = 20;



                }
                g.TranslateTransform(this.ClientSize.Width / 2, this.ClientSize.Height / 2);
                g.ResetTransform();
                cardPack.CardRandom();
                this.Invalidate();
            }
        }
    }
}