﻿using System;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Windows.Forms.Layout;

namespace labCards
{
    internal class CardPack
    {
        private Random rnd = new Random();
        private int[] arr;

        private int Count { get; }

        public CardPack(int count)
        {
            Count = count;
            CardSort();
        }
        public int this[int index] { get { return arr[index]; } }

        public void CardSort()
        {
            arr = Enumerable.Range(0, Count).ToArray();

        }
        public void CardRandom() => arr = Enumerable.Range(0, Count).OrderBy(_ => rnd.Next()).ToArray();
    }
}