﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labParallel
{
    public partial class Form1 : Form
    {
        public int FILES_COUNT = 100;

        public Form1()
        {
            InitializeComponent();
            edDirTemp.Text = Path.Combine(Path.GetTempPath(), Application.ProductName);
            Directory.CreateDirectory(edDirTemp.Text);
            buCreateFiles.Click += BuCreateFiles_Click;
            buDeleteFiles.Click += BuDeleteFiles_Click;
            button3.Click += Button3_Click;    
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Parallel.Invoke(
                () => { /*Задача 1*/},
                () => { /*Задача 2*/},
                () => { /*Задача 3*/},
                () => { /*Задача 4*/},
                () => { /*Задача 5*/}
                );
        }

        private void BuDeleteFiles_Click(object sender, EventArgs e)
        {
            Parallel.For(0, FILES_COUNT, (v) => File.Delete(Path.Combine(edDirTemp.Text, $"file_{v:000}.txt")));
        }

        private void BuCreateFiles_Click(object sender, EventArgs e)
        {
            Parallel.For(0, FILES_COUNT, CreateFile);
        }

        private void CreateFile(int v, ParallelLoopState arg2) {

            File.Create(Path.Combine(edDirTemp.Text, $"file_{v:000}.txt")).Close();
                       
               edLogs.Text += $"file_{v:000}.txt" + Environment.NewLine;
            
        }
    }
}
