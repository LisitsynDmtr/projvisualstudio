﻿namespace labParallel
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buCreateFiles = new System.Windows.Forms.Button();
            this.buDeleteFiles = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.edDirTemp = new System.Windows.Forms.TextBox();
            this.edLogs = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.buCreateFiles.Location = new System.Drawing.Point(49, 51);
            this.buCreateFiles.Name = "button1";
            this.buCreateFiles.Size = new System.Drawing.Size(75, 23);
            this.buCreateFiles.TabIndex = 0;
            this.buCreateFiles.Text = "button1";
            this.buCreateFiles.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buDeleteFiles.Location = new System.Drawing.Point(302, 51);
            this.buDeleteFiles.Name = "button2";
            this.buDeleteFiles.Size = new System.Drawing.Size(76, 23);
            this.buDeleteFiles.TabIndex = 1;
            this.buDeleteFiles.Text = "button2";
            this.buDeleteFiles.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(496, 51);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.edDirTemp.Location = new System.Drawing.Point(12, 12);
            this.edDirTemp.Name = "textBox1";
            this.edDirTemp.Size = new System.Drawing.Size(591, 23);
            this.edDirTemp.TabIndex = 3;
            // 
            // textBox2
            // 
            this.edLogs.Location = new System.Drawing.Point(87, 135);
            this.edLogs.Multiline = true;
            this.edLogs.Name = "textBox2";
            this.edLogs.Size = new System.Drawing.Size(543, 288);
            this.edLogs.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.edLogs);
            this.Controls.Add(this.edDirTemp);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.buDeleteFiles);
            this.Controls.Add(this.buCreateFiles);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buCreateFiles;
        private System.Windows.Forms.Button buDeleteFiles;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox edDirTemp;
        private System.Windows.Forms.TextBox edLogs;
    }
}

