﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageSelectInGrid
{
    public partial class Form1 : Form
    {
        enum SelMode
        {
            Line,
            Square,
            Normal
        }
        private const int rows_min = 2;
        private const int rows_max = 15;
        private const int cols_min = 2;
        private const int cols_max = 15;
        private Bitmap b;
        private Graphics g;
        private int cellWidth;

        private int cellHeight;
        private int curRow;
        private int curCol;
        private (int row, int col) selBegin;
        private (int row, int col) selEnd;
        private (int row, int col) selBeginR;
        private (int row, int col) selEndR;
        private (int row, int col) x;
        private Point curPoint;
        private Point startPoint;
        private SelMode selMode;

        public int Cols { get; private set; } = 5;
        public int Rows { get; private set; } = 5;

        public Form1()
        {
            InitializeComponent();


            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);


            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, curPoint);
            this.Resize += (s, e) => ResizeCells();

            this.MouseDown += (s, e) => startPoint = e.Location;

            this.MouseMove += Form1_MouseMove;
            this.MouseDown += Form1_MouseDown;
            this.KeyUp += Form1_KeyUp;
            this.KeyDown += Form1_KeyDown;
            this.Text = $"{Application.ProductName}: (f5/f6-rows. f7/f8 - cols)";
            ResizeCells();
            //Hw
           
            
            //изменение масштаба с помощью мыши
            //выделять квадрат с alt и ряд с shift
            //двигать выделенную область ctrl
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            selMode = SelMode.Normal;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            selMode = e.Shift ? SelMode.Line : e.Alt ? SelMode.Square : SelMode.Normal;
            switch (e.KeyCode)
            {
                case Keys.F5:
                        if (Rows > rows_min)
                    {
                        Rows--;
                        ResizeCells();
                    }
                    break;

                case Keys.F6:
                    if (Rows < rows_max)
                    {
                        Rows++;
                        ResizeCells();
                    }
                    break;

                case Keys.F8:
                    if (Rows > cols_min)
                    {
                        Cols--;
                        ResizeCells();
                    }
                    break;

                case Keys.F9:
                    if (Rows < cols_max)
                    {
                        Cols++;
                        ResizeCells();
                    }
                    break;
                
            }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Left)
            {
                selBegin = selEnd  = (curRow, curCol);

                DrawCells();
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Right)
            {
                curPoint.X += e.X - startPoint.X;
                curPoint.Y += e.Y - startPoint.Y;
                startPoint = e.Location;
                this.Invalidate();
            }

            for (int i = 0; i < Rows; curRow = i, i++)
            {
                if (i * cellHeight > e.Y - curPoint.Y) break;
            }
            for (int i = 0; i < Cols; curCol = i, i++)
            {
                if (i * cellWidth > e.X - curPoint.X) break;
            }

            if (e.Button == MouseButtons.Left)
            {
                selEnd = (curRow, curCol);
                selBeginR = (Math.Min(selBegin.row, selEnd.row), Math.Min(selBegin.col, selEnd.col));
                selEndR = (Math.Min(selBegin.row, selEnd.row), Math.Min(selBegin.col, selEnd.col));
            }
            switch (selMode) {

                case SelMode.Line:
                    if (Math.Abs(selBegin.row - selEnd.row) > Math.Abs(selBegin.col - selEnd.col))
                        selBeginR.col = selEndR.col = selBegin.row;
                    else
                        selBeginR.row = selEndR.row - selBegin.row;
                    break;
                case SelMode.Square:
                    break;
                case SelMode.Normal:
                    break;
                default:
                    break;       
            }

            DrawCells();
            this.Text = $"{selBegin.row}:{selBegin.col} - {selEnd.row}:{selEnd.col}";
        }

        private void ResizeCells() {

            cellWidth = this.ClientSize.Width / Cols;
            cellHeight = this.ClientSize.Height / Rows;
            DrawCells();
           
        }

        private void DrawCells() {

            g.Clear(DefaultBackColor);
            if (selEnd.row - selBegin.row > 0 || selEnd.col - selBegin.col > 0)
                g.FillRectangle(new SolidBrush(Color.LightBlue),
                selBegin.col*cellWidth,
                selBegin.row*cellHeight,
                (selEnd.col - selBegin.col +1)*cellWidth,
                (selEnd.row - selBegin.row + 1)*cellHeight
                );
            else
                g.FillRectangle(new SolidBrush(Color.LightBlue),
               selEnd.col * cellWidth,
               selEnd.row * cellHeight,
               (Math.Abs(selEnd.col - selBegin.col) + 1) * cellWidth,
               (Math.Abs(selEnd.row - selBegin.row) + 1) * cellHeight
               );


            for (int i = 0; i < Rows; i++)
            {
                g.DrawLine(new Pen(Color.Green, 1), 0, i * cellHeight, Cols * cellWidth, i * cellHeight);
            }

            for (int i = 0; i < Rows; i++)
            {
                g.DrawLine(new Pen(Color.Green, 1), i * cellWidth,0,  i * cellWidth, Rows * cellHeight);
            }

            g.DrawRectangle(new Pen(Color.Coral, 5), curCol* cellWidth, curRow* cellHeight, cellWidth, cellHeight);

            g.DrawString($"{curRow}:{curCol}", new Font("", 30), new SolidBrush(Color.Black),
                new Rectangle (curCol * cellWidth, curRow * cellHeight,cellWidth, cellHeight),
                new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center});
            this.Invalidate();
        }

    }

    
}
