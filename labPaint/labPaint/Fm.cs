﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPaint
{
    public partial class Fm : Form
    {
        private Bitmap b;
        private Graphics g;
        private Point startPoint;
        private Bitmap bb;
        private Pen myPenLeft;
        private Pen myPenRight;
        private int Rectan = 0;
        private int Ellipse = 0;
        private int Line = 1;
        private int Clearing = 0;

        public Fm()
        {
            InitializeComponent();
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            myPenLeft = new Pen(pxColor1.BackColor, 10);
            myPenRight = new Pen(pxColor1.BackColor, 10);
            myPenLeft.StartCap = myPenLeft.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            myPenRight.StartCap = myPenRight.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            pxImage.MouseDown += (s, e) => startPoint = e.Location;
            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

            trPenWidth.Value = Convert.ToInt32(myPenLeft.Width);
            trPenWidth.Value = Convert.ToInt32(myPenRight.Width);
            trPenWidth.ValueChanged += (s, e) => myPenLeft.Width = trPenWidth.Value;
            trPenWidth.ValueChanged += (s, e) => myPenRight.Width = trPenWidth.Value;

            pxColor1.MouseDown += PxColorAll_Click;
            pxColor2.MouseDown += PxColorAll_Click;
            pxColor3.MouseDown += PxColorAll_Click;
            pxColor4.MouseDown += PxColorAll_Click;


            picLeft.BackColor = myPenLeft.Color;
            picRight.BackColor = myPenRight.Color;

            picChoose.MouseDown += PxColorAll_MouseDown;

            buRect.MouseDown += BuRect_MouseDown;
            buLine.MouseDown += BuLine_MouseDown;
            buEllipse.MouseDown += BuEllipse_MouseDown;
            buClearing.MouseDown += BuClearing_MouseDown;

            pxImage.MouseDown += PxImage_MouseDown;

            buImageClear.Click += delegate
            {

                g.Clear(SystemColors.Control);
                pxImage.Invalidate();

            };


        
            buSaveToFile.Click += BuSaveToFile_Click;
            buLoadFromFile.Click += BuLoadFromFile_Click;
            buAddRandomStars.Click += BuAddRandomStars_Click;

            //hw левая-правая кнопка,
            //функционал похожий на пэинт
            //hw 2 - больше фигур, заливка,копировать, рисование снизу вверх


        }

        private void BuLine_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void PxImage_MouseDown(object sender, MouseEventArgs e)
        {
            startPoint = e.Location;
            bb = (Bitmap)b.Clone();
            if (e.Button == MouseButtons.Left && Rectan == 1)
            {
                Rectangle rect = new Rectangle(startPoint.X - 100 / 2, startPoint.Y - 100 / 2, 100, 100);
                g.DrawRectangle(myPenLeft, rect);
                startPoint = e.Location;
                pxImage.Invalidate();
            }
            if (e.Button == MouseButtons.Right && Rectan == 1)
            {
                Rectangle rect = new Rectangle(startPoint.X - 100 / 2, startPoint.Y - 100 / 2, 100, 100);
                g.DrawRectangle(myPenRight, rect);
                startPoint = e.Location;
                pxImage.Invalidate();
            }

            if (e.Button == MouseButtons.Left && Ellipse == 1)
            {
                Rectangle rect = new Rectangle(startPoint.X - 100 / 2, startPoint.Y - 100 / 2, 100, 100);
                g.DrawEllipse(myPenLeft, rect);
                startPoint = e.Location;
                pxImage.Invalidate();
            }
            if (e.Button == MouseButtons.Right && Ellipse == 1)
            {
                Rectangle rect = new Rectangle(startPoint.X - 100 / 2, startPoint.Y - 100 / 2, 100, 100);
                g.DrawEllipse(myPenRight, rect);
                startPoint = e.Location;
                pxImage.Invalidate();
            }
        }
        private void BuClearing_MouseDown(object sender, MouseEventArgs e)
        {
            Rectan = 0;
            Line = 0;
            Ellipse = 0;
            Clearing = 1;
        }

        private void BuEllipse_MouseDown(object sender, MouseEventArgs e)
        {
            Rectan = 0;
            Line = 0;
            Ellipse = 1;
            Clearing = 0;
        }

        private void BuLine_MouseDown(object sender, MouseEventArgs e)
        {
            Rectan = 0;
            Line = 1;
            Ellipse = 0;
            Clearing = 0;

        }

        private void BuRect_MouseDown(object sender, MouseEventArgs e)
        {

            Rectan = 1;
            Line = 0;
            Ellipse = 0;
            Clearing = 0;
        }
      

        private void PxColorAll_Click(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (sender is PictureBox x)
                {
                    myPenRight.Color = x.BackColor;
                    picRight.BackColor = myPenRight.Color;

                }
                
            }
            if (e.Button == MouseButtons.Left)
            {
                if (sender is PictureBox x)
                {
                    myPenLeft.Color = x.BackColor;
                    picLeft.BackColor = myPenLeft.Color;
                }

            }
        }

        private void BuAddRandomStars_Click(object sender, EventArgs e)
        {
            var rnd = new Random();
            for (int i = 0; i < 200; i++)
            {
                g.FillEllipse(new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256))),
                    rnd.Next(b.Width),
                    rnd.Next(b.Height),
                    rnd.Next(1, 10),
                    rnd.Next(1, 10)


                    ) ;
                pxImage.Invalidate();
            }
        }

        private void BuLoadFromFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                g.Clear(SystemColors.Control);
                g.DrawImage(Bitmap.FromFile(dialog.FileName), 0, 0);
                pxImage.Invalidate();
            }
        }

        private void BuSaveToFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "(*.JPG)|*.JPG";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                b.Save(dialog.FileName);
            }


        }

        private void PxColorAll_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (sender is PictureBox x)
                {
                    ColorDialog colorDialog = new ColorDialog();
                    colorDialog.Color = x.BackColor;
                    if (colorDialog.ShowDialog() == DialogResult.OK)
                    {
                        picRight.BackColor = colorDialog.Color;
                        myPenRight.Color = picRight.BackColor;
                    }
                }

            }
            if (e.Button == MouseButtons.Left)
            {
                if (sender is PictureBox x)
                {
                    ColorDialog colorDialog = new ColorDialog();
                    colorDialog.Color = x.BackColor;
                    if (colorDialog.ShowDialog() == DialogResult.OK)
                    {
                        picLeft.BackColor = colorDialog.Color;
                        myPenLeft.Color = picLeft.BackColor;

                    }
                }

            }

        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            

            if (e.Button == MouseButtons.Left && Clearing == 1)
            {
                g.FillEllipse(new SolidBrush(this.BackColor), e.X - 20 / 2, e.Y - 20 / 2, 20, 20); ;
                startPoint = e.Location;
                pxImage.Invalidate();
            }
            if (e.Button == MouseButtons.Right && Clearing == 1)
            {
                g.FillEllipse(new SolidBrush(this.BackColor), e.X - 20 / 2, e.Y - 20 / 2, 20, 20); ;
                startPoint = e.Location;
                pxImage.Invalidate();
            }

            if (e.Button == MouseButtons.Left && Line == 1)
            {
                g.DrawLine(myPenLeft, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();
            }
            if (e.Button == MouseButtons.Right && Line == 1)
            {
                g.DrawLine(myPenRight, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();
            }

        }

       
    }

}
