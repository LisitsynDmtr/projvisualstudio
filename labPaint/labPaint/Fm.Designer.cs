﻿namespace labPaint
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.buClearing = new System.Windows.Forms.Button();
            this.buRect = new System.Windows.Forms.Button();
            this.buEllipse = new System.Windows.Forms.Button();
            this.buLine = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.picChoose = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.picRight = new System.Windows.Forms.PictureBox();
            this.picLeft = new System.Windows.Forms.PictureBox();
            this.buAddRandomStars = new System.Windows.Forms.Button();
            this.buLoadFromFile = new System.Windows.Forms.Button();
            this.buSaveToFile = new System.Windows.Forms.Button();
            this.buImageClear = new System.Windows.Forms.Button();
            this.trPenWidth = new System.Windows.Forms.TrackBar();
            this.pxColor4 = new System.Windows.Forms.PictureBox();
            this.pxColor3 = new System.Windows.Forms.PictureBox();
            this.pxColor2 = new System.Windows.Forms.PictureBox();
            this.pxColor1 = new System.Windows.Forms.PictureBox();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.buDrawModeLine = new System.Windows.Forms.Button();
            this.buDrawModeTriangle = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picChoose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Controls.Add(this.buDrawModeTriangle);
            this.panel1.Controls.Add(this.buDrawModeLine);
            this.panel1.Controls.Add(this.buClearing);
            this.panel1.Controls.Add(this.buRect);
            this.panel1.Controls.Add(this.buEllipse);
            this.panel1.Controls.Add(this.buLine);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.picChoose);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.picRight);
            this.panel1.Controls.Add(this.picLeft);
            this.panel1.Controls.Add(this.buAddRandomStars);
            this.panel1.Controls.Add(this.buLoadFromFile);
            this.panel1.Controls.Add(this.buSaveToFile);
            this.panel1.Controls.Add(this.buImageClear);
            this.panel1.Controls.Add(this.trPenWidth);
            this.panel1.Controls.Add(this.pxColor4);
            this.panel1.Controls.Add(this.pxColor3);
            this.panel1.Controls.Add(this.pxColor2);
            this.panel1.Controls.Add(this.pxColor1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 531);
            this.panel1.TabIndex = 0;
            // 
            // buClearing
            // 
            this.buClearing.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buClearing.BackgroundImage")));
            this.buClearing.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buClearing.Location = new System.Drawing.Point(95, 235);
            this.buClearing.Name = "buClearing";
            this.buClearing.Size = new System.Drawing.Size(61, 47);
            this.buClearing.TabIndex = 10;
            this.buClearing.UseVisualStyleBackColor = true;
            // 
            // buRect
            // 
            this.buRect.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buRect.BackgroundImage")));
            this.buRect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buRect.Location = new System.Drawing.Point(180, 151);
            this.buRect.Name = "buRect";
            this.buRect.Size = new System.Drawing.Size(52, 37);
            this.buRect.TabIndex = 9;
            this.buRect.UseVisualStyleBackColor = true;
            // 
            // buEllipse
            // 
            this.buEllipse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buEllipse.BackgroundImage")));
            this.buEllipse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buEllipse.Location = new System.Drawing.Point(107, 151);
            this.buEllipse.Name = "buEllipse";
            this.buEllipse.Size = new System.Drawing.Size(36, 37);
            this.buEllipse.TabIndex = 9;
            this.buEllipse.UseVisualStyleBackColor = true;
            // 
            // buLine
            // 
            this.buLine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buLine.BackgroundImage")));
            this.buLine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buLine.Location = new System.Drawing.Point(22, 151);
            this.buLine.Name = "buLine";
            this.buLine.Size = new System.Drawing.Size(36, 37);
            this.buLine.TabIndex = 9;
            this.buLine.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(45, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 19);
            this.label3.TabIndex = 8;
            this.label3.Text = "Choose color...";
            // 
            // picChoose
            // 
            this.picChoose.BackColor = System.Drawing.Color.Yellow;
            this.picChoose.Location = new System.Drawing.Point(12, 64);
            this.picChoose.Name = "picChoose";
            this.picChoose.Size = new System.Drawing.Size(27, 26);
            this.picChoose.TabIndex = 6;
            this.picChoose.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(202, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Color2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(154, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Color1";
            // 
            // picRight
            // 
            this.picRight.Location = new System.Drawing.Point(208, 21);
            this.picRight.Name = "picRight";
            this.picRight.Size = new System.Drawing.Size(30, 26);
            this.picRight.TabIndex = 6;
            this.picRight.TabStop = false;
            // 
            // picLeft
            // 
            this.picLeft.Location = new System.Drawing.Point(158, 21);
            this.picLeft.Name = "picLeft";
            this.picLeft.Size = new System.Drawing.Size(30, 26);
            this.picLeft.TabIndex = 6;
            this.picLeft.TabStop = false;
            // 
            // buAddRandomStars
            // 
            this.buAddRandomStars.Location = new System.Drawing.Point(12, 416);
            this.buAddRandomStars.Name = "buAddRandomStars";
            this.buAddRandomStars.Size = new System.Drawing.Size(232, 29);
            this.buAddRandomStars.TabIndex = 5;
            this.buAddRandomStars.Text = "Add Random star";
            this.buAddRandomStars.UseVisualStyleBackColor = true;
            // 
            // buLoadFromFile
            // 
            this.buLoadFromFile.Location = new System.Drawing.Point(12, 381);
            this.buLoadFromFile.Name = "buLoadFromFile";
            this.buLoadFromFile.Size = new System.Drawing.Size(232, 29);
            this.buLoadFromFile.TabIndex = 4;
            this.buLoadFromFile.Text = "Load from file";
            this.buLoadFromFile.UseVisualStyleBackColor = true;
            // 
            // buSaveToFile
            // 
            this.buSaveToFile.Location = new System.Drawing.Point(12, 346);
            this.buSaveToFile.Name = "buSaveToFile";
            this.buSaveToFile.Size = new System.Drawing.Size(232, 29);
            this.buSaveToFile.TabIndex = 3;
            this.buSaveToFile.Text = "Save to file";
            this.buSaveToFile.UseVisualStyleBackColor = true;
            // 
            // buImageClear
            // 
            this.buImageClear.Location = new System.Drawing.Point(12, 311);
            this.buImageClear.Name = "buImageClear";
            this.buImageClear.Size = new System.Drawing.Size(232, 29);
            this.buImageClear.TabIndex = 2;
            this.buImageClear.Text = "Clear";
            this.buImageClear.UseVisualStyleBackColor = true;
            // 
            // trPenWidth
            // 
            this.trPenWidth.Location = new System.Drawing.Point(12, 106);
            this.trPenWidth.Minimum = 1;
            this.trPenWidth.Name = "trPenWidth";
            this.trPenWidth.Size = new System.Drawing.Size(220, 56);
            this.trPenWidth.TabIndex = 1;
            this.trPenWidth.Value = 1;
            // 
            // pxColor4
            // 
            this.pxColor4.BackColor = System.Drawing.Color.Fuchsia;
            this.pxColor4.Location = new System.Drawing.Point(111, 21);
            this.pxColor4.Name = "pxColor4";
            this.pxColor4.Size = new System.Drawing.Size(27, 26);
            this.pxColor4.TabIndex = 0;
            this.pxColor4.TabStop = false;
            // 
            // pxColor3
            // 
            this.pxColor3.BackColor = System.Drawing.Color.Lime;
            this.pxColor3.Location = new System.Drawing.Point(78, 21);
            this.pxColor3.Name = "pxColor3";
            this.pxColor3.Size = new System.Drawing.Size(27, 26);
            this.pxColor3.TabIndex = 0;
            this.pxColor3.TabStop = false;
            // 
            // pxColor2
            // 
            this.pxColor2.BackColor = System.Drawing.Color.Red;
            this.pxColor2.Location = new System.Drawing.Point(45, 21);
            this.pxColor2.Name = "pxColor2";
            this.pxColor2.Size = new System.Drawing.Size(27, 26);
            this.pxColor2.TabIndex = 0;
            this.pxColor2.TabStop = false;
            // 
            // pxColor1
            // 
            this.pxColor1.BackColor = System.Drawing.Color.Blue;
            this.pxColor1.Location = new System.Drawing.Point(12, 21);
            this.pxColor1.Name = "pxColor1";
            this.pxColor1.Size = new System.Drawing.Size(27, 26);
            this.pxColor1.TabIndex = 0;
            this.pxColor1.TabStop = false;
            // 
            // pxImage
            // 
            this.pxImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pxImage.Location = new System.Drawing.Point(250, 0);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(528, 531);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // button1
            // 
            this.buDrawModeLine.Location = new System.Drawing.Point(12, 451);
            this.buDrawModeLine.Name = "button1";
            this.buDrawModeLine.Size = new System.Drawing.Size(232, 29);
            this.buDrawModeLine.TabIndex = 11;
            this.buDrawModeLine.Text = "Линия";
            this.buDrawModeLine.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buDrawModeTriangle.Location = new System.Drawing.Point(12, 486);
            this.buDrawModeTriangle.Name = "button2";
            this.buDrawModeTriangle.Size = new System.Drawing.Size(232, 27);
            this.buDrawModeTriangle.TabIndex = 12;
            this.buDrawModeTriangle.Text = "Треугольник";
            this.buDrawModeTriangle.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 531);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.panel1);
            this.Name = "Fm";
            this.Text = "labPaint";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picChoose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TrackBar trPenWidth;
        private System.Windows.Forms.PictureBox pxColor4;
        private System.Windows.Forms.PictureBox pxColor3;
        private System.Windows.Forms.PictureBox pxColor2;
        private System.Windows.Forms.PictureBox pxColor1;
        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.Button buImageClear;
        private System.Windows.Forms.Button buAddRandomStars;
        private System.Windows.Forms.Button buLoadFromFile;
        private System.Windows.Forms.Button buSaveToFile;
        private System.Windows.Forms.PictureBox picRight;
        private System.Windows.Forms.PictureBox picLeft;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox picChoose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button buLine;
        private System.Windows.Forms.Button buRect;
        private System.Windows.Forms.Button buEllipse;
        private System.Windows.Forms.Button buClearing;
        private System.Windows.Forms.Button buDrawModeTriangle;
        private System.Windows.Forms.Button buDrawModeLine;
    }
}

