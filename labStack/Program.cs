﻿using System;
using System.Collections;

namespace labStack
{
    class Program
    {
        static void Main(string[] args)
        {
            //labStackT
            Stack x = new Stack();
            x.Push("Hello");
            x.Push(555);
            x.Push("qwert");

            Console.WriteLine(x.Peek());
            Console.WriteLine("-----");

            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());

            try
            {

                Console.WriteLine(x.Pop());
            }
            catch (Exception ex) {

                Console.WriteLine($"Что-то пошло не так. Ошибка = {ex.Message}");
            }

        }
    }
}
