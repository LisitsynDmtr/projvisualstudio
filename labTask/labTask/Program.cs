﻿using System;
using System.Threading.Tasks;

namespace labTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("- Begin -");

            //1
            var t1 = new Task(MyTask1);
            t1.Start();
            //t1.Status

            //2
            new Task(() => Console.WriteLine("Task2")).Start();

            //3
            Task.Run(() => Console.WriteLine("Task3"));


            //4

            var t4 = new Task(MyTask4);
            t4.Start();
            //t4.Wait();

            //5
            Task.Run(async () => { await Task.Delay(100); Console.WriteLine("Task5"); });
            
            Console.WriteLine("- End -");
            Console.ReadKey();

        }
       async private static void MyTask4()
        {
            Console.WriteLine("Task4 - BEGIN");
            await Task.Delay(500);
            Console.WriteLine("Task4 - END");
        }

        private static void MyTask1()
        {
            Console.WriteLine("MyTask1");
        }

    }
}
