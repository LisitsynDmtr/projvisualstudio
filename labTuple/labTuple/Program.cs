﻿using System;

namespace labTuple
{
    class Program
    {
        static void Main(string[] args)
        {
            var a1 = 5;
            int a2 = 5;
            var x1 = (2, 3);

            (int, int) x11 = (2, 3);
            Console.WriteLine(x1.Item1);
            Console.WriteLine(x1.Item2);
            Console.WriteLine();

            //2
            (int min, int max) x2 = (2, 3);
            Console.WriteLine(x2.min);
            Console.WriteLine(x2.max);
            Console.WriteLine();
            //3
            var x3 = (min: 2, max: 3);
            Console.WriteLine(x3.min);
            Console.WriteLine(x3.max);
            Console.WriteLine();
            //4
            var (min, max) = (2, 3);
            Console.WriteLine(min);
            Console.WriteLine(max);
            Console.WriteLine();
            //5
            var x5 = Getx5();
            Console.WriteLine(x5.Item1);
            Console.WriteLine(x5.Item2);
            Console.WriteLine();
            //6
            var x6 = Getx6();
            Console.WriteLine(x6.min);
            Console.WriteLine(x6.max);
            Console.WriteLine();
            //7
            var x7 = Getx7((4, 5), 6);
            Console.WriteLine(x7.Item1);
            Console.WriteLine(x7.Item2);
            Console.WriteLine();
        }

        private static (int, int) Getx7((int, int) p, int v) => (p.Item1 + p.Item2, v);


        private static (int min, int max) Getx6() => (3, 4);


        private static (int, int) Getx5() => (3, 4);

    }
}