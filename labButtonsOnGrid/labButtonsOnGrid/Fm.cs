﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labButtonsOnGrid
{
    public partial class Fm : Form
    {
        private Button[,] bu;
        private string N;
        private int xCellWidth;
        private int xCellHeight;

        public int Rows { get; private set; } = 3;
        public int Cols { get; private set; } = 3;

        public Fm()
        {
            InitializeComponent();
            
           
            this.KeyDown += Fm_KeyDown;
        

        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            
            switch (e.KeyCode)
            {
                case Keys.Left:
                    Cols--;
                    CreateButtons();
                    ResizeButtons();
                    break;
                case Keys.Right:
                      Cols++;
                    CreateButtons();
                    ResizeButtons();
                    break;
                case Keys.Up:
                    Rows--;
                    CreateButtons();
                    ResizeButtons();
                    break;
                case Keys.Down:
                    Rows++;
                    CreateButtons();
                    ResizeButtons();
                    break;

            }
        }

        private void Fm_MouseLeave(object sender, EventArgs e)
        {  if (sender is Control x)
            {
                x.Text = N;
            }
        }

        private void Fm_MouseEnter(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                N = x.Text;
                x.Text = "Hover";

            }
        }

        private void ResizeButtons()
        {
            xCellWidth = this.ClientSize.Width / Cols - 1;
             xCellHeight = this.ClientSize.Height / Rows - 1;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j].Width = xCellWidth;
                    bu[i, j].Height = xCellHeight;
                    bu[i, j].Location = new Point((j * xCellWidth), i * xCellHeight);
                }
        }

        private void CreateButtons()
        {
            bu = new Button[Rows, Cols];
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    
                    bu[i, j] = new Button();
                    bu[i, j].Text = (((Rows * Cols)) - ((Rows * Cols) - ((i * Rows) + (j+1)))).ToString();
                    bu[i, j].Font = new Font("Segoe UI", 20);
                    bu[i, j].Click += BuAll_Click;
                    bu[i, j].MouseEnter += Fm_MouseEnter;
                    bu[i, j].MouseLeave += Fm_MouseLeave;
                    bu[i, j].KeyDown += Fm_KeyDown;
                    this.Controls.Add(bu[i, j]);
                }
        }


        private void BuAll_Click(object sender, EventArgs e)
        {
            if (sender is Button x)
                MessageBox.Show(N);
        }
    }
}
