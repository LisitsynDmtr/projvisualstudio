﻿namespace labFileExplorer
{
    partial class labFileExplorer
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(labFileExplorer));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buback = new System.Windows.Forms.ToolStripButton();
            this.buForward = new System.Windows.Forms.ToolStripButton();
            this.buUp = new System.Windows.Forms.ToolStripButton();
            this.edDir = new System.Windows.Forms.ToolStripTextBox();
            this.buDirSelect = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.miViewLargeIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewSmallIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewList = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewTile = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewPanelinf = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewPanelCD = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.showDir = new System.Windows.Forms.ToolStripMenuItem();
            this.showFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.showAll = new System.Windows.Forms.ToolStripMenuItem();
            this.buHidden = new System.Windows.Forms.ToolStripButton();
            this.buCopyNames = new System.Windows.Forms.ToolStripButton();
            this.BuFavorite = new System.Windows.Forms.ToolStripButton();
            this.lv = new System.Windows.Forms.ListView();
            this.LargeIcons = new System.Windows.Forms.ImageList(this.components);
            this.smallIcons = new System.Windows.Forms.ImageList(this.components);
            this.lvDisc = new System.Windows.Forms.ListView();
            this.DiscIcon = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miPopupOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miPopupdel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miPopupProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miPopupFavorite = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lastatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.treeView = new System.Windows.Forms.TreeView();
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.buOpenTree = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.buAddFavoriteTree = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.buPropertiesTree = new System.Windows.Forms.ToolStripMenuItem();
            this.trList = new System.Windows.Forms.ImageList(this.components);
            this.lvFavorite = new System.Windows.Forms.ListView();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.buDelFavorite = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.contextMenuStrip3.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Crimson;
            this.toolStrip1.Font = new System.Drawing.Font("Adobe Pi Std", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buback,
            this.buForward,
            this.buUp,
            this.edDir,
            this.buDirSelect,
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2,
            this.buHidden,
            this.buCopyNames,
            this.BuFavorite});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1293, 25);
            this.toolStrip1.TabIndex = 2;
            // 
            // buback
            // 
            this.buback.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buback.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buback.ForeColor = System.Drawing.Color.Red;
            this.buback.Image = ((System.Drawing.Image)(resources.GetObject("buback.Image")));
            this.buback.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buback.Name = "buback";
            this.buback.Size = new System.Drawing.Size(23, 22);
            this.buback.Text = "<--";
            // 
            // buForward
            // 
            this.buForward.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buForward.ForeColor = System.Drawing.Color.Red;
            this.buForward.Image = ((System.Drawing.Image)(resources.GetObject("buForward.Image")));
            this.buForward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buForward.Name = "buForward";
            this.buForward.Size = new System.Drawing.Size(23, 22);
            this.buForward.Text = "-->";
            // 
            // buUp
            // 
            this.buUp.BackColor = System.Drawing.Color.AntiqueWhite;
            this.buUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buUp.Image = ((System.Drawing.Image)(resources.GetObject("buUp.Image")));
            this.buUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(23, 22);
            this.buUp.Text = "^";
            // 
            // edDir
            // 
            this.edDir.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.edDir.Name = "edDir";
            this.edDir.Size = new System.Drawing.Size(600, 25);
            // 
            // buDirSelect
            // 
            this.buDirSelect.BackColor = System.Drawing.Color.MistyRose;
            this.buDirSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buDirSelect.Image = ((System.Drawing.Image)(resources.GetObject("buDirSelect.Image")));
            this.buDirSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buDirSelect.Name = "buDirSelect";
            this.buDirSelect.Size = new System.Drawing.Size(55, 22);
            this.buDirSelect.Text = "Выбрать";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.BackColor = System.Drawing.Color.MistyRose;
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miViewLargeIcon,
            this.miViewSmallIcon,
            this.miViewList,
            this.miViewDetails,
            this.miViewTile,
            this.miViewPanelinf,
            this.miViewPanelCD});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(39, 22);
            this.toolStripDropDownButton1.Text = "Вид";
            // 
            // miViewLargeIcon
            // 
            this.miViewLargeIcon.Name = "miViewLargeIcon";
            this.miViewLargeIcon.Size = new System.Drawing.Size(171, 22);
            this.miViewLargeIcon.Text = "Большие иконки";
            // 
            // miViewSmallIcon
            // 
            this.miViewSmallIcon.Name = "miViewSmallIcon";
            this.miViewSmallIcon.Size = new System.Drawing.Size(171, 22);
            this.miViewSmallIcon.Text = "Маленькие иконки";
            // 
            // miViewList
            // 
            this.miViewList.Name = "miViewList";
            this.miViewList.Size = new System.Drawing.Size(171, 22);
            this.miViewList.Text = "Список";
            // 
            // miViewDetails
            // 
            this.miViewDetails.Name = "miViewDetails";
            this.miViewDetails.Size = new System.Drawing.Size(171, 22);
            this.miViewDetails.Text = "Детали";
            // 
            // miViewTile
            // 
            this.miViewTile.Name = "miViewTile";
            this.miViewTile.Size = new System.Drawing.Size(171, 22);
            this.miViewTile.Text = "Плитка";
            // 
            // miViewPanelinf
            // 
            this.miViewPanelinf.Name = "miViewPanelinf";
            this.miViewPanelinf.Size = new System.Drawing.Size(171, 22);
            this.miViewPanelinf.Text = "Панель свойств";
            // 
            // miViewPanelCD
            // 
            this.miViewPanelCD.Name = "miViewPanelCD";
            this.miViewPanelCD.Size = new System.Drawing.Size(171, 22);
            this.miViewPanelCD.Text = "Показать Диски";
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.BackColor = System.Drawing.Color.MistyRose;
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showDir,
            this.showFiles,
            this.showAll});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(108, 22);
            this.toolStripDropDownButton2.Text = "Показать только";
            // 
            // showDir
            // 
            this.showDir.Name = "showDir";
            this.showDir.Size = new System.Drawing.Size(111, 22);
            this.showDir.Text = "Папки";
            // 
            // showFiles
            // 
            this.showFiles.Name = "showFiles";
            this.showFiles.Size = new System.Drawing.Size(111, 22);
            this.showFiles.Text = "Файлы";
            // 
            // showAll
            // 
            this.showAll.Name = "showAll";
            this.showAll.Size = new System.Drawing.Size(111, 22);
            this.showAll.Text = "Все";
            // 
            // buHidden
            // 
            this.buHidden.BackColor = System.Drawing.Color.MistyRose;
            this.buHidden.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buHidden.Image = ((System.Drawing.Image)(resources.GetObject("buHidden.Image")));
            this.buHidden.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buHidden.Name = "buHidden";
            this.buHidden.Size = new System.Drawing.Size(109, 22);
            this.buHidden.Text = "Показать скрытые";
            // 
            // buCopyNames
            // 
            this.buCopyNames.BackColor = System.Drawing.Color.MistyRose;
            this.buCopyNames.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buCopyNames.Image = ((System.Drawing.Image)(resources.GetObject("buCopyNames.Image")));
            this.buCopyNames.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buCopyNames.Name = "buCopyNames";
            this.buCopyNames.Size = new System.Drawing.Size(107, 22);
            this.buCopyNames.Text = "Копировать имена";
            this.buCopyNames.ToolTipText = "Копировать имена";
            // 
            // BuFavorite
            // 
            this.BuFavorite.BackColor = System.Drawing.Color.MistyRose;
            this.BuFavorite.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.BuFavorite.Image = ((System.Drawing.Image)(resources.GetObject("BuFavorite.Image")));
            this.BuFavorite.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BuFavorite.Name = "BuFavorite";
            this.BuFavorite.Size = new System.Drawing.Size(118, 22);
            this.BuFavorite.Text = "Показать избранное";
            // 
            // lv
            // 
            this.lv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lv.BackColor = System.Drawing.Color.MistyRose;
            this.lv.Font = new System.Drawing.Font("Adobe Pi Std", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lv.HideSelection = false;
            this.lv.LargeImageList = this.LargeIcons;
            this.lv.Location = new System.Drawing.Point(172, 27);
            this.lv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(880, 629);
            this.lv.SmallImageList = this.smallIcons;
            this.lv.TabIndex = 1;
            this.lv.UseCompatibleStateImageBehavior = false;
            // 
            // LargeIcons
            // 
            this.LargeIcons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.LargeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("LargeIcons.ImageStream")));
            this.LargeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.LargeIcons.Images.SetKeyName(0, "fileIcon.jpg");
            this.LargeIcons.Images.SetKeyName(1, "fileIcon.png");
            this.LargeIcons.Images.SetKeyName(2, "");
            this.LargeIcons.Images.SetKeyName(3, "");
            this.LargeIcons.Images.SetKeyName(4, "pdf.png");
            this.LargeIcons.Images.SetKeyName(5, "rar.png");
            this.LargeIcons.Images.SetKeyName(6, "exe.png");
            // 
            // smallIcons
            // 
            this.smallIcons.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.smallIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("smallIcons.ImageStream")));
            this.smallIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.smallIcons.Images.SetKeyName(0, "fileIcon.jpg");
            this.smallIcons.Images.SetKeyName(1, "fileIcon.png");
            this.smallIcons.Images.SetKeyName(2, "doc.png");
            this.smallIcons.Images.SetKeyName(3, "photo.png");
            this.smallIcons.Images.SetKeyName(4, "pdf.png");
            this.smallIcons.Images.SetKeyName(5, "rar.png");
            this.smallIcons.Images.SetKeyName(6, "exe.png");
            // 
            // lvDisc
            // 
            this.lvDisc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDisc.BackColor = System.Drawing.Color.MistyRose;
            this.lvDisc.HideSelection = false;
            this.lvDisc.LargeImageList = this.DiscIcon;
            this.lvDisc.Location = new System.Drawing.Point(1057, 27);
            this.lvDisc.Name = "lvDisc";
            this.lvDisc.Size = new System.Drawing.Size(237, 606);
            this.lvDisc.TabIndex = 3;
            this.lvDisc.UseCompatibleStateImageBehavior = false;
            // 
            // DiscIcon
            // 
            this.DiscIcon.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.DiscIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("DiscIcon.ImageStream")));
            this.DiscIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.DiscIcon.Images.SetKeyName(0, "CD.png");
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miPopupOpen,
            this.toolStripSeparator1,
            this.miPopupdel,
            this.toolStripSeparator2,
            this.miPopupProperties,
            this.toolStripSeparator3,
            this.miPopupFavorite});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(198, 110);
            // 
            // miPopupOpen
            // 
            this.miPopupOpen.Name = "miPopupOpen";
            this.miPopupOpen.Size = new System.Drawing.Size(197, 22);
            this.miPopupOpen.Text = "Открыть";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(194, 6);
            // 
            // miPopupdel
            // 
            this.miPopupdel.Name = "miPopupdel";
            this.miPopupdel.Size = new System.Drawing.Size(197, 22);
            this.miPopupdel.Text = "Удалить";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(194, 6);
            // 
            // miPopupProperties
            // 
            this.miPopupProperties.Name = "miPopupProperties";
            this.miPopupProperties.Size = new System.Drawing.Size(197, 22);
            this.miPopupProperties.Text = "Свойства";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(194, 6);
            // 
            // miPopupFavorite
            // 
            this.miPopupFavorite.Name = "miPopupFavorite";
            this.miPopupFavorite.Size = new System.Drawing.Size(197, 22);
            this.miPopupFavorite.Text = "Добавить в избранное";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lastatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 631);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1293, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lastatus
            // 
            this.lastatus.Name = "lastatus";
            this.lastatus.Size = new System.Drawing.Size(12, 17);
            this.lastatus.Text = "-";
            // 
            // treeView
            // 
            this.treeView.ContextMenuStrip = this.contextMenuStrip3;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeView.ImageIndex = 1;
            this.treeView.ImageList = this.trList;
            this.treeView.Location = new System.Drawing.Point(0, 25);
            this.treeView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.treeView.Name = "treeView";
            this.treeView.SelectedImageIndex = 0;
            this.treeView.Size = new System.Drawing.Size(155, 606);
            this.treeView.TabIndex = 5;
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buOpenTree,
            this.toolStripSeparator4,
            this.buAddFavoriteTree,
            this.toolStripSeparator5,
            this.buPropertiesTree});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            this.contextMenuStrip3.Size = new System.Drawing.Size(198, 82);
            // 
            // buOpenTree
            // 
            this.buOpenTree.Name = "buOpenTree";
            this.buOpenTree.Size = new System.Drawing.Size(197, 22);
            this.buOpenTree.Text = "Открыть";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(194, 6);
            // 
            // buAddFavoriteTree
            // 
            this.buAddFavoriteTree.Name = "buAddFavoriteTree";
            this.buAddFavoriteTree.Size = new System.Drawing.Size(197, 22);
            this.buAddFavoriteTree.Text = "Добавить в избранное";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(194, 6);
            // 
            // buPropertiesTree
            // 
            this.buPropertiesTree.Name = "buPropertiesTree";
            this.buPropertiesTree.Size = new System.Drawing.Size(197, 22);
            this.buPropertiesTree.Text = "Свойства";
            // 
            // trList
            // 
            this.trList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.trList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("trList.ImageStream")));
            this.trList.TransparentColor = System.Drawing.Color.Transparent;
            this.trList.Images.SetKeyName(0, "strelka.png");
            this.trList.Images.SetKeyName(1, "quest.png");
            this.trList.Images.SetKeyName(2, "dvd.png");
            this.trList.Images.SetKeyName(3, "directory.jpg");
            // 
            // lvFavorite
            // 
            this.lvFavorite.HideSelection = false;
            this.lvFavorite.LargeImageList = this.LargeIcons;
            this.lvFavorite.Location = new System.Drawing.Point(0, 25);
            this.lvFavorite.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lvFavorite.Name = "lvFavorite";
            this.lvFavorite.Size = new System.Drawing.Size(155, 610);
            this.lvFavorite.SmallImageList = this.smallIcons;
            this.lvFavorite.TabIndex = 6;
            this.lvFavorite.UseCompatibleStateImageBehavior = false;
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buDelFavorite});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(202, 26);
            // 
            // buDelFavorite
            // 
            this.buDelFavorite.Name = "buDelFavorite";
            this.buDelFavorite.Size = new System.Drawing.Size(201, 22);
            this.buDelFavorite.Text = "Удалить из избранного";
            // 
            // labFileExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1293, 653);
            this.Controls.Add(this.lvFavorite);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lvDisc);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.lv);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "labFileExplorer";
            this.Text = "LabFileExplorer";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.contextMenuStrip3.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        

        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.ImageList LargeIcons;
        private System.Windows.Forms.ImageList smallIcons;
        private System.Windows.Forms.ToolStripButton buback;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton buUp1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton buDirSelect;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem miViewLargeIcon;
        private System.Windows.Forms.ToolStripMenuItem miViewSmallIcon;

        private System.Windows.Forms.ToolStripButton buForward;
        private System.Windows.Forms.ToolStripButton buUp;
        private System.Windows.Forms.ToolStripTextBox edDir;
        private System.Windows.Forms.ToolStripMenuItem miViewList;
        private System.Windows.Forms.ToolStripMenuItem miViewDetails;
        private System.Windows.Forms.ToolStripMenuItem miViewTile;
        private System.Windows.Forms.ListView lvDisc;
        private System.Windows.Forms.ImageList DiscIcon;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem showDir;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem showFiles;
        private System.Windows.Forms.ToolStripMenuItem showAll;
        private System.Windows.Forms.ToolStripButton buHidden;
        private System.Windows.Forms.ToolStripMenuItem miViewPanelinf;
        private System.Windows.Forms.ToolStripMenuItem miViewPanelCD;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miPopupOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miPopupdel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem miPopupProperties;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lastatus;
        private System.Windows.Forms.ToolStripButton buCopyNames;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ToolStripButton BuFavorite;
        private System.Windows.Forms.ListView lvFavorite;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem miPopupFavorite;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem buDelFavorite;
        private System.Windows.Forms.ImageList trList;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip3;
        private System.Windows.Forms.ToolStripMenuItem buOpenTree;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem buAddFavoriteTree;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem buPropertiesTree;
    }
}

