﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class FmFileProperties : Form
    {
        public FmFileProperties(string path)
        {
            InitializeComponent();

            if (File.Exists(path))
            {
                
                var x = new FileInfo(path);

                this.Text = $"Файл {x.Name}, {x.Extension}, {x.Length}";
                lvProp.BeginUpdate();
                lvProp.Columns.Clear();
                lvProp.Items.Clear();
                lvProp.View = View.Details;
                lvProp.Columns.Add("Свойства", 150);
                lvProp.Columns.Add("", 150);
                lvProp.Items.Add(new ListViewItem(new string[] { "Имя файла: ", x.Name }));
                lvProp.Items.Add(new ListViewItem(new string[] { "Дата Изменения: ", x.LastWriteTime.ToString() }));
                lvProp.Items.Add(new ListViewItem(new string[] { "Тип: ", x.GetType().ToString() }));
                lvProp.Items.Add(new ListViewItem(new string[] { "Размер: ", sizeFile(x.Length).ToString() }));
                lvProp.Items.Add(new ListViewItem(new string[] { "Расширение:  ", x.Extension.ToString() }));
                lvProp.Items.Add(new ListViewItem(new string[] { "Дата Создания: ", x.CreationTime.ToString() }));
                lvProp.EndUpdate();

            }
            else if (Directory.Exists(path))
            {
               
                var x = new DirectoryInfo(path);
                this.Text = $"Папка: {x.Name}";
                lvProp.BeginUpdate();
                lvProp.Columns.Clear();
                lvProp.Items.Clear();
                lvProp.View = View.Details;
                lvProp.Columns.Add("Свойства", 150);
                lvProp.Columns.Add("", 150);
                lvProp.Items.Add(new ListViewItem(new string[] { "Имя файла: ", x.Name }));
                lvProp.Items.Add(new ListViewItem(new string[] { "Дата Изменения: ", x.LastWriteTime.ToString() }));
                lvProp.Items.Add(new ListViewItem(new string[] { "Тип: ", x.GetType().ToString() }));
                lvProp.Items.Add(new ListViewItem(new string[] { "Размер: ", dirSize(x).ToString() }));
                lvProp.Items.Add(new ListViewItem(new string[] { "Расширение:  ", x.Extension.ToString() }));
                lvProp.Items.Add(new ListViewItem(new string[] { "Дата Создания: ", x.CreationTime.ToString() }));
                lvProp.EndUpdate();
            }

            

            lvProp.EndUpdate();
        }



        public static long dirSize(DirectoryInfo d)
        {
            long Size = 0;

            FileInfo[] fis = d.GetFiles();

            foreach (FileInfo fi in fis)
            {
                Size += fi.Length;
            }

            DirectoryInfo[] dis = d.GetDirectories();

            foreach (DirectoryInfo di in dis)
            {
                Size += dirSize(di);
            }

            return (Size);
        }

        static string sizeFile(long byteCount)
        {
            string[] suf = { "Б", "КБ", "МБ", "ГБ" };
            if (byteCount == 0)
                return "0" + suf[0];

            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];

        }
    }
}
