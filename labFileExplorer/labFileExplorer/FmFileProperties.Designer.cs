﻿
namespace labFileExplorer
{
    partial class FmFileProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvProp = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // lvProp
            // 
            this.lvProp.HideSelection = false;
            this.lvProp.Location = new System.Drawing.Point(0, 1);
            this.lvProp.Name = "lvProp";
            this.lvProp.Size = new System.Drawing.Size(532, 371);
            this.lvProp.TabIndex = 0;
            this.lvProp.UseCompatibleStateImageBehavior = false;
            // 
            // FmFileProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 373);
            this.Controls.Add(this.lvProp);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FmFileProperties";
            this.Text = "FmFileProperties";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvProp;
    }
}