﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class labFileExplorer : Form
    {

        Stack x = new Stack();

        Stack y = new Stack();
        private bool spaceSize = false;
        private int flagHidden = 1;
        private string currentPath;
        
        public labFileExplorer()
        {


            
            InitializeComponent();

            CurDir = Directory.GetCurrentDirectory();


            
            
            
            
            
           

            buUp.Click += BuUp_Click;

            edDir.KeyDown += EdDir_KeyDown;
            buDirSelect.Click += BuDirSelect_Click;

            miViewLargeIcon.Click += (s, e) => lv.View = View.LargeIcon;
            miViewSmallIcon.Click += (s, e) => lv.View = View.SmallIcon;
            miViewList.Click += (s, e) => lv.View = View.List;
            miViewDetails.Click += (s, e) => lv.View = View.Details;
            miViewTile.Click += (s, e) => lv.View = View.Tile;
            lvDisc.View = View.LargeIcon;

            foreach (var item in Directory.GetLogicalDrives())
            {
                treeView.Nodes.Add("",item,2);
            }
            treeView.Click += TreeView_Click;
            treeView.DoubleClick += TreeView_DoubleClick;
            treeView.ContextMenuStrip = contextMenuStrip3;

           // treeView.PathSeparator = Path.PathSeparator.ToString();

           lvFavorite.ContextMenuStrip = contextMenuStrip2;
            buDelFavorite.Click += BuDelFavorite_Click;

            BuFavorite.Click += BuFavorite_Click;
            lvFavorite.View = View.SmallIcon;

            showDir.Click += ShowDir_Click;
            showFiles.Click += ShowFiles_Click;
            showAll.Click += ShowAll_Click;

            buback.Click += Buback_Click;
            buForward.Click += BuForward_Click;
            buback.Enabled = false;
            buForward.Enabled = false;
            buCopyNames.Click += BuCopyNames_Click;
            lv.ItemSelectionChanged += Lv_ItemSelectionChanged;
            lvDisc.ItemSelectionChanged += (s, e) => SelItem = Path.Combine(CurDir, e.Item.Text);
            lvFavorite.ItemSelectionChanged += (s, e) => SelItemFav = e.Item.Text;
            lvFavorite.DoubleClick += LvFavorite_DoubleClick;
            lvDisc.DoubleClick += Lv_DoubleClick;
            lv.DoubleClick += Lv_DoubleClick;


            lvFavorite.Hide();
            treeView.Show();
            buHidden.Click += BuHidden_Click1;

            lv.LostFocus += Lv_LostFocus;
            lv.Click += Lv_Click;
            LoadDiscs();
            
            lv.KeyDown += Lv_KeyDown;
            lv.ContextMenuStrip = contextMenuStrip1;

            miPopupOpen.Click += MiPopupOpen_Click;
            miPopupdel.Click += MiPopupdel_Click;
            miPopupProperties.Click += MiPopupProperties_Click;
            miPopupFavorite.Click += MiPopupFavorite_Click;

            buOpenTree.Click += BuOpenTree_Click;
            buAddFavoriteTree.Click += BuAddFavoriteTree_Click;
            buPropertiesTree.Click += BuPropertiesTree_Click;

            lv.Columns.Add("Имя", 400);
            lv.Columns.Add("Дата изменения", 150);
            lv.Columns.Add("Тип", 100);
            lv.Columns.Add("Размер", 100);
            lv.Columns.Add("Расширение", 100);
            lv.Columns.Add("Дата создания", 100);


            lv.ColumnClick += Lv_ColumnClick;

            currentPath = Directory.GetCurrentDirectory().ToString();
            DirectoryInfo dirInfo = new DirectoryInfo(currentPath);

            if (!File.Exists(currentPath + @"\favorite.txt"))
            {
                File.Create(currentPath + @"\favorite.txt");

            }

            using (StreamReader readFile = new StreamReader($@"{currentPath}\favorite.txt"))
            {
                while (!readFile.EndOfStream)
                {
                    string s = readFile.ReadLine();
                    FileInfo g = new FileInfo(s);
                    if (File.Exists(s))
                    {

                        lvFavorite.Items.Add(new ListViewItem(
                           new string[] { System.IO.Path.GetFileName(s), s },
                           fileExtension(g)));
                        
                    }
                    else if (Directory.Exists(s))
                    {
                        lvFavorite.Items.Add(new ListViewItem(
                         new string[] { System.IO.Path.GetFileName(s), s },
                         0));
                        

                    }
                }

                    readFile.Close();
                
            }

            LoadDir(CurDir);
        
        
        }

        private void BuPropertiesTree_Click(object sender, EventArgs e)
        {
            var x = new FmFileProperties(treeView.SelectedNode.FullPath);
            x.ShowDialog();
        }

        private void BuAddFavoriteTree_Click(object sender, EventArgs e)
        {

            if (Directory.Exists(treeView.SelectedNode.FullPath))
            {

                lvFavorite.Items.Add(new ListViewItem(
                       new string[] { System.IO.Path.GetFileName(treeView.SelectedNode.FullPath), treeView.SelectedNode.FullPath },
                       1));

            }

            using (StreamWriter sw = new StreamWriter(currentPath + @"\favorite.txt", true, System.Text.Encoding.Default))
            {
                sw.WriteLine(treeView.SelectedNode.FullPath);
                sw.Close();
            }
        }

        private void BuOpenTree_Click(object sender, EventArgs e)
        {
            y.Clear();
            buForward.Enabled = false;
           
            if (Directory.Exists(treeView.SelectedNode.FullPath))
            {
                buback.Enabled = true;
                LoadDir(treeView.SelectedNode.FullPath);
            }
        }

        private void TreeView_DoubleClick(object sender, EventArgs e)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(treeView.SelectedNode.FullPath);
            treeView.BeginUpdate();
            treeView.SelectedNode.Nodes.Clear();
            foreach (var item in directoryInfo.GetDirectories())
            {
                treeView.SelectedNode.Nodes.Add(item.Name);
            }
            treeView.SelectedNode.Expand();
            treeView.EndUpdate();
           LoadDir(treeView.SelectedNode.FullPath);
        }

        private void TreeView_Click(object sender, EventArgs e)
        {

            //LoadDir(treeView.SelectedNode.FullPath);
            //MessageBox.Show(treeView.SelectedNode.ToString());
        }

        private void LvFavorite_DoubleClick(object sender, EventArgs e)
        {  
            y.Clear();
            buForward.Enabled = false;
            var item = lvFavorite.SelectedItems[0];
            SelItemFav= item.SubItems[1].Text;
            
            if (File.Exists(SelItemFav))
            {
                
                var p = new Process();
                p.StartInfo = new ProcessStartInfo(SelItemFav)
                {
                    UseShellExecute = true
                };
                p.Start();
               
            }
            else if (Directory.Exists(SelItemFav))
            {
               
                buback.Enabled = true;
                LoadDir(SelItemFav);



            }
        }

        private void BuDelFavorite_Click(object sender, EventArgs e)
        {
            var path = currentPath + @"\favorite.txt";
            var item = lvFavorite.SelectedItems[0];
            string str = item.SubItems[1].Text;
            string[] lines = File.ReadAllLines(path);
            using (var sw = new StreamWriter(path))
            {
                foreach (var line in lines.Where(x => x != str))
                    sw.WriteLine(line);

                sw.Close();
                
            }

            foreach (ListViewItem eachItem in lvFavorite.SelectedItems)
            {
                lvFavorite.Items.Remove(eachItem);
            }

            lvFavorite.Refresh();


        }

        private void MiPopupFavorite_Click(object sender, EventArgs e)
        {
           
            FileInfo g = new FileInfo(SelItem);
            if (File.Exists(SelItem))
            {
                
                lvFavorite.Items.Add(new ListViewItem(
                    
                   new string[] { System.IO.Path.GetFileName(SelItem), SelItem,
                   },
                   fileExtension(g)));

            }
            else if (Directory.Exists(SelItem)) {

                lvFavorite.Items.Add(new ListViewItem(
                       new string[] { System.IO.Path.GetFileName(SelItem), SelItem },
                       0));

            }

            using (StreamWriter sw = new StreamWriter(currentPath + @"\favorite.txt", true, System.Text.Encoding.Default))
            {
                sw.WriteLine(SelItem);
                sw.Close();
            }

        }

        private void BuFavorite_Click(object sender, EventArgs e)
        {
           
            if (BuFavorite.Text == "Показать избранное")
            {
                BuFavorite.Text = "Скрыть избранное";
                treeView.Enabled = false;
                treeView.Hide();
                lvFavorite.Show();

                
            } else {

                BuFavorite.Text = "Показать избранное";
                treeView.Enabled = true;
                treeView.Show();
                lvFavorite.Hide();
            }
        }

        private void MiPopupOpen_Click(object sender, EventArgs e)
        {
            y.Clear();
            buForward.Enabled = false;
            if (File.Exists(SelItem))
            {
                var item = lv.SelectedItems[0];
                var p = new Process();
                p.StartInfo = new ProcessStartInfo(SelItem)
                {
                    UseShellExecute = true
                };
                p.Start();
               
            }
            else if (Directory.Exists(SelItem))
            {
                buback.Enabled = true;
                LoadDir(SelItem);



            }

        }

        private void BuCopyNames_Click(object sender, EventArgs e)
        {
            string st = "";
            //(1) Lv
            for (int i = 0; i < lv.Items.Count; i++)
            {
                st += lv.Items[i].Text + Environment.NewLine;
            }
            //(2) D
            //DirectoryInfo directoryInfo = new DirectoryInfo(CurDir);
            //foreach (var item in directoryInfo.GetDirectories())
            //{
            //    st += item.FullName + Environment.NewLine;
            //}
            //foreach (var item in directoryInfo.GetFiles())
            //{
            //    st += item.Name + Environment.NewLine;
            //}
            Clipboard.SetText(st);
        }

        private void Lv_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            SelItem = Path.Combine(CurDir, e.Item.Text);
            lastatus.Text = $"Элементов:{lv.Items.Count} , Выбрано: {lv.SelectedItems.Count}";
        }

        private void MiPopupProperties_Click(object sender, EventArgs e)
        {
            var x = new FmFileProperties(SelItem);
            x.ShowDialog();
            
           
        }

        private void MiPopupdel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Удалить?",
                Application.ProductName,
                MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
            
            {
                MessageBox.Show($"Файл Удален {SelItem}");
                File.Delete(SelItem);
            }
        }

        private void Lv_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count < 0)
                return;        

            lvDisc.BeginUpdate();
            lvDisc.Columns.Clear();
            lvDisc.Items.Clear();
            lvDisc.View = View.Details;
            lvDisc.Columns.Add("Свойства", 150);
            lvDisc.Columns.Add("", 150);
           
            var item = lv.SelectedItems[0];
            lvDisc.Items.Add(new ListViewItem(new string[] { "Имя файла: ", item.SubItems[0].Text}));
            lvDisc.Items.Add(new ListViewItem(new string[] { "Дата Изменения: ", item.SubItems[1].Text }));
            lvDisc.Items.Add(new ListViewItem(new string[] { "Тип: ", item.SubItems[2].Text }));
            lvDisc.Items.Add(new ListViewItem(new string[] { "Размер: ", item.SubItems[3].Text }));
            lvDisc.Items.Add(new ListViewItem(new string[] { "Расширение:  ", item.SubItems[4].Text }));
            lvDisc.Items.Add(new ListViewItem(new string[] { "Дата Создания: ", item.SubItems[5].Text }));

            lvDisc.EndUpdate();

        }
            private void Lv_LostFocus(object sender, EventArgs e)
        {
            lvDisc.Columns.Clear();
            lvDisc.Items.Clear();
            lvDisc.View = View.LargeIcon;
            lvDisc.BeginUpdate();
            LoadDiscs();
            lvDisc.EndUpdate();
        }

      

    
      
        private void LoadDiscs()
        {

            lvDisc.BeginUpdate();
            foreach (var item in Directory.GetLogicalDrives())
            {

                lvDisc.Items.Add(new ListViewItem(
                new string[] { item },
                0));

            }
            lvDisc.EndUpdate();

        }

   

      

        private void BuHidden_Click1(object sender, EventArgs e)
        {
            DirectoryInfo showHidden = new DirectoryInfo(CurDir);
            lv.BeginUpdate();
            lv.Items.Clear();
            
            toolStrip1.Update();

            if (flagHidden == 1)
            {
               

                foreach (var dir in showHidden.GetDirectories())
                {
                    if (dir.Attributes == FileAttributes.Hidden)
                    {
                        lv.Items.Add(new ListViewItem(
                        new string[] { dir.Name, dir.LastWriteTime.ToString(), "Папка", spaceSize == true ? sizeFile(dirSize(dir)).ToString() : " ", " ", dir.CreationTime.ToString() },
                        0));
                    }
                }

                foreach (var item in showHidden.GetFiles())
                {
                    if (item.Attributes == FileAttributes.Hidden)
                    {
                        lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(),
                    "Файл", sizeFile(item.Length), item.Extension},
                    fileExtension(item)));
                    }

                }


                buHidden.Text = "Скрыть";
            }

            if (flagHidden == 0)
            {

                foreach (var dir in showHidden.GetDirectories())
                {
                   
                        lv.Items.Add(new ListViewItem(
                    new string[] { dir.Name, dir.LastWriteTime.ToString(), spaceSize == true ? sizeFile(dirSize(dir)).ToString() : " ", " ", dir.CreationTime.ToString() },
                    0));
                   
                }

                foreach (var item in showHidden.GetFiles())
                {
                   
                        lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(),
                    "Файл", sizeFile(item.Length), item.Extension, item.CreationTime.ToString()},
                    fileExtension(item)));
                   

                }
                
              
                buHidden.Text = "Показать скрытые";
            }

            if (flagHidden == 1)
                flagHidden = 0;
            else
                flagHidden = 1;

            lv.EndUpdate();
        }

       

        private void Lv_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            this.lv.ListViewItemSorter = new ListViewColumnComparer(e.Column);
        }

        private void ShowAll_Click(object sender, EventArgs e)
        {
            LoadDir(CurDir);
        }

        private void ShowFiles_Click(object sender, EventArgs e)
        {
            DirectoryInfo fileShow = new DirectoryInfo(CurDir);
            lv.BeginUpdate();
            lv.Items.Clear();

            foreach (var item in fileShow.GetFiles())
            {

                lv.Items.Add(new ListViewItem(
                new string[] { item.Name, item.LastWriteTime.ToString(),
                    "Файл", sizeFile(item.Length), item.Extension, item.CreationTime.ToString()},
                fileExtension(item)));


            }

            lv.EndUpdate();
        }

        private void ShowDir_Click(object sender, EventArgs e)
        {
            
            DirectoryInfo dirShow = new DirectoryInfo(CurDir);
            lv.BeginUpdate();
            lv.Items.Clear();

            foreach (var item in dirShow.GetDirectories())
            {

                lv.Items.Add(new ListViewItem(
                new string[] { item.Name, item.LastWriteTime.ToString(), "Папка", spaceSize == true ? sizeFile(dirSize(item)).ToString() : " ", " ",  item.CreationTime.ToString() },
                0));

            }
            lv.EndUpdate();

        }

        private void BuForward_Click(object sender, EventArgs e)
        {
            try
            {

                if (y.Count < 1)
                {
                    buForward.Enabled = false;
                    buback.Enabled = true;
                }else
                    LoadDir(y.Pop().ToString()); 



               
            }
            catch (Exception)
            { 
            }
        }

        private void Buback_Click(object sender, EventArgs e)
        {


            try
            {
                buForward.Enabled = true;


                if (x.Count == 0)
                {
                    buback.Enabled = false;
                    buForward.Enabled = true;
                }
                    y.Push(x.Pop());
                    LoadDir(x.Pop().ToString());

            }
            catch (Exception)
            {
            }
        }

        private void BuUp_Click(object sender, EventArgs e)
        {
            
            buForward.Enabled = false;

            if (Directory.GetParent(CurDir) != null)
            {
                buback.Enabled = true;
                spaceSize = false;
                LoadDir(Directory.GetParent(CurDir).ToString());
                
            }
        }
        private void Lv_DoubleClick(object sender, EventArgs e)
        {
            y.Clear();
            buForward.Enabled = false;
            if (File.Exists(SelItem)) {
                var item = lv.SelectedItems[0];
                var p = new Process();
                p.StartInfo = new ProcessStartInfo(SelItem)
                {
                    UseShellExecute = true
                };
                p.Start();
               //s
            }
            else if (Directory.Exists(SelItem))
            {
                buback.Enabled = true;
                LoadDir(SelItem);
              


            }
        }

        public static long dirSize(DirectoryInfo d)
        {
            long Size = 0;
            
            FileInfo[] fis = d.GetFiles();

            foreach (FileInfo fi in fis)
            {
                Size += fi.Length;
            }
           
            DirectoryInfo[] dis = d.GetDirectories();
           
            foreach (DirectoryInfo di in dis)
            {
                Size += dirSize(di);
            }
            
            return(Size);
        }




        


        private void Lv_KeyDown(object sender, KeyEventArgs e)
        {
            

            if (e.KeyCode == Keys.Back)
            {
                BuUp_Click(sender, e);
            }

            if (e.KeyCode == Keys.Space)
            {

                spaceSize = true;
                LoadDir(CurDir);
            }
            if (e.KeyCode == Keys.Delete )
            {
                if (SelItem != null)
                {
                    if (MessageBox.Show("Удалить?",
                    Application.ProductName,
                    MessageBoxButtons.YesNoCancel) == DialogResult.Yes)

                    {
                        MessageBox.Show($"файл удален{SelItem}");
                        File.Delete(SelItem);
                    }
                }
            }

        }

        
        private void BuDirSelect_Click(object sender, EventArgs e)
        {
            
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
                LoadDir(dialog.SelectedPath);
        }

        

        private void EdDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                LoadDir(edDir.Text);
        }

        private void LoadDir(string newDir)
        {
            if (y.Count == 0) { buForward.Enabled = false; buback.Enabled = true;}
            if (x.Count == 0) { buForward.Enabled = true; buback.Enabled = false; }
            if (x.Count == 0 && y.Count ==0) { buForward.Enabled = false; buback.Enabled = false; }

            if (newDir != null)
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(newDir);
              
                lv.BeginUpdate();
                lv.Items.Clear();

                foreach (var item in directoryInfo.GetDirectories())
                {

                    lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(), "Папка", spaceSize==true ? sizeFile(dirSize(item)).ToString() : " ", " ",  item.CreationTime.ToString() },
                    0)) ;
                    
                }

                foreach (var item in directoryInfo.GetFiles())
                {

                    lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(),
                    "Файл", sizeFile(item.Length), item.Extension , item.CreationTime.ToString()},
                    fileExtension(item)));

                    
                }

                x.Push(newDir);
                lv.EndUpdate();
                edDir.Text = newDir;
                CurDir = newDir;

            }
            
              
            
        }

       

        static string sizeFile(long byteCount)
        {
            string[] suf = {"Б", "КБ", "МБ", "ГБ"}; 
            if (byteCount == 0)
                return "0" + suf[0];
            
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];

        }


        private int fileExtension(FileInfo g)
        {
            switch (g.Extension)
            {
                case ".doc":
                case ".docx":
                    return 2;
                case ".jpg":
                case ".png":
                case ".jpeg":
                case ".gif":
                    return 3;
                case ".pdf":
                    return 4;
                case ".rar":
                case ".zip":
                    return 5;
                case ".exe":
                    return 6;
              
                default:
                    return 1;
                    
            }

            
        }




        public string CurDir { get; private set; }
        public string SelItem { get; private set; }
        public string SelItemFav { get; private set; }
    }
}
