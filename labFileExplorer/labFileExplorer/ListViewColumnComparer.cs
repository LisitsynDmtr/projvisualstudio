﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace labFileExplorer
{
    internal class ListViewColumnComparer : IComparer
    {
        public int Column { get; set; }

        public ListViewColumnComparer(int column)
        {
            Column = column;
        }


        public int Compare(object x, object y)
        {
            try
            {
                return String.Compare(
                ((ListViewItem)x).SubItems[Column].Text,
                ((ListViewItem)y).SubItems[Column].Text);
            }
            catch (Exception) // если вдруг столбец пустой (или что-то пошло не так)
            {
                return 0;
            }
        }

    }
}