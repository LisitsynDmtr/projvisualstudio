﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollHorz
{
    public partial class Form1 : Form
    {
        private Bitmap imBG;
        private Bitmap b;
        private Graphics g;
        private Point startPoint;
        private int deltaX;

        public Form1()
        {
            InitializeComponent();

            imBG = Properties.Resources.FonHorz;
            this.Height = imBG.Height;
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(b, 0, 0); };
            this.MouseDown += (s, e) => startPoint = e.Location;
            this.MouseMove += Form1_MouseMove;
            this.KeyDown += Form1_KeyDown;

            

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                
                case Keys.Left:
                    UpdateDeltaX(15);
                    break;
              
                case Keys.Right:
                    UpdateDeltaX(-15);
                    break;
        
            }
            this.Invalidate();
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaX(e.X - startPoint.X);
                startPoint = e.Location;
                this.Invalidate();
            }
        }

        private void UpdateDeltaX(int v)
        {
            this.Text = $"{Application.ProductName} : {deltaX}, {v}";
            deltaX += v;
            if (deltaX > 0)
            {
                deltaX -= imBG.Width;
            } else {
                if (deltaX < -imBG.Width)
                    deltaX += imBG.Width;
            }
        }

        private void UpdateBG() {
            g.Clear(SystemColors.Control);
            for (int i = 0; i < 3; i++)
            {
                g.DrawImage(imBG, deltaX + i*imBG.Width, 0);
            }
            
                
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
