﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSoundPlayer
{
    public partial class Form1 : Form
    {
        private SoundPlayer soundPlayer = new SoundPlayer();

        public Form1()
        {
            InitializeComponent();

            soundPlayer.Stream = Properties.Resources._90;
            buPlay.Click += (s, e) => soundPlayer.Play();
            buStop.Click += (s, e) => soundPlayer.Stop();
            buPlayLooping.Click += (s, e) => soundPlayer.PlayLooping();

            button1.Click += (s, e) => SystemSounds.Beep.Play();

        }

    }
}
