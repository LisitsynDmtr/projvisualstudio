﻿namespace labSoundPlayer
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.buPlay = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.buPlayLooping = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(78, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buPlay.Location = new System.Drawing.Point(185, 50);
            this.buPlay.Name = "button2";
            this.buPlay.Size = new System.Drawing.Size(75, 23);
            this.buPlay.TabIndex = 1;
            this.buPlay.Text = "button2";
            this.buPlay.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(453, 119);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(8, 8);
            this.button3.TabIndex = 2;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.buPlayLooping.Location = new System.Drawing.Point(286, 50);
            this.buPlayLooping.Name = "button4";
            this.buPlayLooping.Size = new System.Drawing.Size(75, 23);
            this.buPlayLooping.TabIndex = 3;
            this.buPlayLooping.Text = "button4";
            this.buPlayLooping.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.buStop.Location = new System.Drawing.Point(185, 104);
            this.buStop.Name = "button5";
            this.buStop.Size = new System.Drawing.Size(75, 23);
            this.buStop.TabIndex = 4;
            this.buStop.Text = "button5";
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 160);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buPlayLooping);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.buPlay);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "labSoundPlayer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buPlay;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button buPlayLooping;
        private System.Windows.Forms.Button buStop;
    }
}

