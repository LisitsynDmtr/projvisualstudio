﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen_Lisitsyn
{
    public partial class Form1 : Form
    {
        private Button[,] buttons;
        public int Rows { get; private set; } = 6;
        public int Cols { get; private set; } = 6;
        Timer timer = new Timer();
        private DateTime startDown;
        private int SEC_MAX = 60;
        private int sum = 0;
        private int min_sum;
        private int score = 0;
        private int wrongscore = 0;
        private int level = 1;
        Game game = new Game();

        public Form1()
        {
            InitializeComponent();


            CreateButtons();
            ResizeButtons();
            timer.Tick += Timer_Tick;

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            var x = startDown.AddSeconds(SEC_MAX) - DateTime.Now;
            if (x.Ticks < 0)
            {
                timer.Stop();
                MessageBox.Show("Time's up!");
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Cols; j++)
                    {
                        buttons[i, j].Enabled = false;
                    }
                }
            }

            label3.Text = $"{x.Seconds}";
            int proc = (int)((x.Seconds * 100) / SEC_MAX);

        }

        private void ResizeButtons()
        {
            int xCellWidth = this.groupBox1.Width / Cols;
            int xCellHeight = this.groupBox1.Height / Rows;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    buttons[i, j].Width = xCellWidth - 20;
                    buttons[i, j].Height = xCellHeight - 20;
                    if (i == 1 || i == 2)
                        buttons[i, j].Location = new Point(xCellWidth * j + 10, xCellHeight * i + 10);
                    else
                        buttons[i, j].Location = new Point(xCellWidth * j + 10, xCellHeight * i + 20);

                }
        }

        private void CreateButtons()
        {
            int count = 0;
            buttons = new Button[Rows, Cols];
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    buttons[i, j] = new Button();
                    buttons[i, j].Name = $"button{count}";
                    buttons[i, j].BackColor = Color.White;
                    buttons[i, j].Tag = count;
                    buttons[i, j].MouseDown += Form1_MouseDown;
                    this.groupBox1.Controls.Add(buttons[i, j]);
                    count++;
                }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (sender is Button x)
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Cols; j++)
                    {
                        int number = Convert.ToInt32(x.Tag);

                        if (number == Convert.ToInt32(buttons[i, j].Tag))
                          choose_row(j);

                    }
                }
            }
        }

        private void choose_row(int j)
        {
            sum = 0;
            for (int k = 0; k < Rows; k++)
            {
                for (int l = 0; l < Cols; l++)
                {
                    buttons[k, l].BackColor = Color.Gray;
                }
            }


            for (int i = 0; i < Cols; i++)
            {
                buttons[i, j].BackColor = Color.White;

                if (buttons[i, j].ForeColor == Color.Blue)
                {
                    sum += Convert.ToInt32(buttons[i, j].Text);

                }
            }
          

            if (sum == min_sum)
            {
                score++;
                label2.Text = $"Correct answers: {score}";
                

            }
            else
            {
                wrongscore++;
                label1.Text = $"Wrong answers: {wrongscore}";
            }

                sum = 0;
                startGame();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            timer.Start();
            startDown = DateTime.Now;
            startGame();

        }

        private void startGame()
        {
            if (score > 3)
                level = 2;
            if (score > 6)
                level = 3;
            min_sum = 100;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    buttons[i, j].Text = game.Gen_Num(level).ToString();
                    buttons[i, j].Font = new Font("Microsoft Sans Serif", 14);
                    if (game.gen_Color() < 5)
                    {
                        buttons[i, j].ForeColor = Color.Blue;
                    } else
                        buttons[i, j].ForeColor = Color.Orange;
                }
            }

            for (int k = 0; k < Rows; k++)
            {
                for (int i = 0; i < Cols; i++)
                {
                    if (buttons[i, k].ForeColor == Color.Blue)
                    {
                        sum += Convert.ToInt32(buttons[i, k].Text);

                    }
                }
                if (sum < min_sum)
                    min_sum = sum;
                sum = 0;


            }
               //MessageBox.Show(min_sum.ToString());

        }

        private void MoreCells_Click(object sender, EventArgs e)
        {
            this.groupBox1.Controls.Clear();
            Rows++;
            Cols++;
            CreateButtons();
            ResizeButtons();
        }
    }
}
