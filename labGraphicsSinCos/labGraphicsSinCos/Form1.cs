﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labGraphicsSinCos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.BackgroundImageLayout = ImageLayout.None;


            DrawAll();
            this.Resize += (s, e) => DrawAll();
            this.Text += ":(Sin-красный, Cos - зеленый, Tan - синий)";

            //HW
            // режим рисования(точки, линии)
            //при перемещении курсора выводить информацию о значении синуса косинуса, тангенса
            //


        }

        public int DOT_RADIUS = 2;

        private void DrawAll()
        {
            var b = new Bitmap(this.ClientSize.Width, this.ClientSize.Height);
            var g = Graphics.FromImage(b);

            var grCountWave = 5;
            var grShifty = b.Height / 2;
            var grHeight = (int)(grShifty*0.8);
            var grWidthPirad = Math.PI / b.Width;
            
           
            //Рисуем X
            g.DrawLine(new Pen(Color.Black), 0, grShifty, b.Width, grShifty);
            for (int i = 1; i < grCountWave; i++)
            {
                var pointX = b.Width / grCountWave * i;
                g.DrawLine(new Pen(Color.Black, 5), pointX, grShifty - 10, pointX, grShifty+10);
                g.DrawString(i>1 ? $"{i}π" : "π", new Font("", 10), Brushes.Black, pointX - 10, grShifty + 10 + 5);
            }


            //Рисуем Y
            g.DrawLine(Pens.Black, 0, 0, 0, b.Height);
            g.DrawLine(new Pen(Color.Black, 10), 0 - 10, grShifty - grHeight, 0 + 10, grShifty - grHeight);
            g.DrawLine(new Pen(Color.Black, 10), 0 - 10, grShifty + grHeight, 0 + 10, grShifty + grHeight);
            g.DrawString("+1", new Font("", 10),Brushes.Black, 10 + 5, grShifty - grHeight - 10);
            g.DrawString("-1", new Font("", 10),Brushes.Black, 10 + 5, grShifty + grHeight - 10);


            //Graphic
            float x;
            float y;
            for (int i = 0; i < b.Width; i++)
            {
                x = i;
                y = (float)(grHeight * -Math.Sin(i * grCountWave * grWidthPirad)+grShifty);
                g.FillEllipse(Brushes.Red, PointToRectangleF(x, y));

                y = (float)(grHeight * -Math.Cos(i * grCountWave * grWidthPirad) + grShifty);
                g.FillEllipse(Brushes.Green, PointToRectangleF(x, y));

                y = (float)(grHeight * -Math.Tan(i * grCountWave * grWidthPirad) + grShifty);
                if(y>0 && y< b.Height)
                    g.FillEllipse(Brushes.Blue, PointToRectangleF(x, y));

                y = (float)(grHeight * -Math.Sin(i * grCountWave * grWidthPirad)*i/grHeight/3 + grShifty);
                g.FillEllipse(Brushes.Orange, PointToRectangleF(x, y));

            }


            this.BackgroundImage = (Bitmap)b.Clone();
        }
        private RectangleF PointToRectangleF(float x, float y) => PointToRectangleF(x, y, DOT_RADIUS);
        private RectangleF PointToRectangleF(float x, float y, int r) => new RectangleF(x-r, y-r, r*2, r*2);
    }
}
