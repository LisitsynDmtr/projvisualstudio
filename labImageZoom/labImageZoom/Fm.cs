﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageZoom
{
    public partial class Fm : Form
    {
        private Point startPoint;
        private Point curPoint;
        private  Bitmap b;
        private  Graphics g;
        private int xPos;
        private int yPos;
        private bool dragging;
        private int wNew;
        private int hNew;

        public int zoomDelta { get; private set; } = 50;

        public Fm()
        {
            InitializeComponent();
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

           /* System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
            gp.AddEllipse(0, 0, pxZoom.Width, pxZoom.Height);
            Region rg = new Region(gp);
            pxZoom.Region = rg;
           */
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

            pxImage.MouseMove += pxImage_MouseMove;
            pxImage.MouseWheel += PxImage_MouseWheel;

            pxZoom.Paint += pxZoom_Paint;

            buLoad.Click += BuLoad_Click;

            pxZoom.MouseUp += PxZoom_MouseUp;
            pxZoom.MouseDown += PxZoom_MouseDown;
            pxZoom.MouseMove += PxZoom_MouseMove;
            
            //Hw
            //исправить стартпоинт в зуме
            //добавить ограничения на zoomdelta
            
         


        }

        private void PxZoom_MouseMove(object sender, MouseEventArgs e)
        {
            var c = sender as PictureBox;
            if (dragging == false || null == c) 
                return;
            c.Top = e.Y + c.Top - yPos;
            c.Left = e.X + c.Left - xPos;
        }

        private void PxZoom_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) 
                return;
            dragging = true;
            xPos = e.X;
            yPos = e.Y;
        }

        private void PxZoom_MouseUp(object sender, MouseEventArgs e)
        {
            var c = sender as PictureBox;
            if (null == c) 
                return;
            dragging = false;
        }

        private void BuLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                pxImage.Image = new Bitmap(dialog.FileName);
                pxImage.Invalidate();
            }
        }

        private void PxImage_MouseWheel(object sender, MouseEventArgs e)
        {
            zoomDelta += e.Delta > 0 ? -2 : 2;
            pxZoom.Invalidate();
        }

        private void pxZoom_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawImage(pxImage.Image,

            new Rectangle(0, 0, pxZoom.Width, pxZoom.Height),
            new Rectangle((startPoint.X - zoomDelta), (startPoint.Y - zoomDelta), zoomDelta*2, zoomDelta*2),
            GraphicsUnit.Pixel
            );
            
        }

        private void pxImage_MouseMove(object sender, MouseEventArgs e)
        {
            Text = $"{Application.ProductName} : ({pxImage.Image.Width },{e.Y})";

            if (pxImage.SizeMode == PictureBoxSizeMode.Zoom)
            {
                int zoomWidth = pxZoom.Width /zoomDelta;
                int zoomHeight = pxZoom.Height / zoomDelta;
                int halfWidth = zoomWidth / 2;
                int halfHeight = zoomHeight / 2;

                //startPoint.X = (e.X * pxImage.Width / pxImage.Image.Width);
                startPoint.X = e.X - halfWidth;
                startPoint.Y = e.Y - halfHeight;
                //startPoint.Y = (e.Y * pxImage.Height / pxImage.Image.Height);


             pxZoom.Invalidate();
            }
            //startPoint = e.Location;
            
        }
    }

}

