﻿namespace labImageZoom
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fm));
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.pxZoom = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buLoad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxZoom)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pxImage
            // 
            this.pxImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pxImage.Image = ((System.Drawing.Image)(resources.GetObject("pxImage.Image")));
            this.pxImage.Location = new System.Drawing.Point(0, 32);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(763, 531);
            this.pxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pxImage.TabIndex = 0;
            this.pxImage.TabStop = false;
            // 
            // pxZoom
            // 
            this.pxZoom.Location = new System.Drawing.Point(485, 34);
            this.pxZoom.Name = "pxZoom";
            this.pxZoom.Size = new System.Drawing.Size(134, 90);
            this.pxZoom.TabIndex = 1;
            this.pxZoom.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buLoad);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(763, 35);
            this.panel1.TabIndex = 2;
            // 
            // buLoad
            // 
            this.buLoad.Location = new System.Drawing.Point(0, 6);
            this.buLoad.Name = "buLoad";
            this.buLoad.Size = new System.Drawing.Size(94, 29);
            this.buLoad.TabIndex = 0;
            this.buLoad.Text = "Загрузить";
            this.buLoad.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 555);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pxZoom);
            this.Controls.Add(this.pxImage);
            this.Name = "Fm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxZoom)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.PictureBox pxZoom;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buLoad;
    }
}

