﻿using System;
using System.Linq;

namespace labLINQ
{
    class Program
    {
        static void Main(string[] args)
        {

            var arr = Enumerable.Range(0, 10).ToArray();

            var myQuery =
                from v in arr
                where v > 3 && v < 8
                orderby v descending
                select v * 2;

        
            Console.WriteLine(string.Join(", ", arr));
            Console.WriteLine(string.Join(", ", myQuery));
            Console.WriteLine($"Count = {myQuery.Count()}, Sum{myQuery.Sum()}");

            var arr2 = new string[] { "Миша", "Юра", "Максим", "Сергей", "Олег", "майкл"};

            var myQuery2 =  
                from v in arr2
                where v.ToUpper().StartsWith("М")
                orderby v
                select v;

            Console.WriteLine(string.Join(", ", myQuery2));
        }



    }
}
