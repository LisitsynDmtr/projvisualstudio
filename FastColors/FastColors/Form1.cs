﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastColors
{
    public partial class Form1 : Form
    {
        private TimeSpan x_Down;
        private DateTime startDown;
        PictureBox[] pics = new PictureBox[4];
        private readonly Timer tmDown = new Timer();
        public int SEС_MAX = 1500;
        private int pxNeed;
        private int score;
        private int best = 0;
        Color myChooseColor;

        public Form1()
        {
            InitializeComponent();
            progressBar1.Hide();
            pxPlay.MouseEnter += (s,e)=> pxPlay.BorderStyle = BorderStyle.FixedSingle;
            pxPlay.MouseLeave += (s,e) => pxPlay.BorderStyle = BorderStyle.None;
            pxPlay.Click += PxPlay_Click;
           
            tmDown.Interval = 100;
            tmDown.Tick += Timer_Tick;
            pics[0] = pictureBox1;
            pics[1] = pictureBox2;
            pics[2] = pictureBox3;
            pics[3] = pictureBox4;

            pxRepeat.Click += PxRepeat_Click;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            x_Down = startDown.AddMilliseconds(SEС_MAX) - DateTime.Now;

            if (x_Down.TotalSeconds < 0)
            {
                tmDown.Stop();
                x_Down = TimeSpan.Zero;
                End_Click(sender,e);
            }
            if ((int)x_Down.TotalMilliseconds> progressBar1.Maximum)
                progressBar1.Value = 3000;
            else
                progressBar1.Value = (int)x_Down.TotalMilliseconds;
        }

        private void PxPlay_Click(object sender, EventArgs e)
        {
            SoundPlayer sound = new SoundPlayer(Properties.Resources.click);
            sound.Play();
            pxPlay.Hide();
            labelTitle.Hide();
            labelScore.Show();
            labelRecord.Show();
            labelCurScore.Show();
            labelCurRecord.Show();
            progressBar1.Show();
            pxRepeat.Hide();
            labelTap.Hide();
            labelRed.Hide();
            startPlay();
            tmDown.Enabled = true;
            startDown = DateTime.Now;
         
        }

        private void startPlay() {
            Random curCol = new Random();
            var colInt = curCol.Next(0, 255);
            var redSquare = curCol.Next(0, 20);
            pxNeed = curCol.Next(1, 4);
            Color myRgbColor = new Color();
            if (redSquare > 10)
                myChooseColor = Color.Red;
            else
            {
                myChooseColor = new Color();
                myChooseColor = Color.FromArgb(curCol.Next(200, 255), curCol.Next(200, 255), colInt);
            }

            myRgbColor = Color.FromArgb(curCol.Next(150, 255), curCol.Next(150, 255), curCol.Next(150, 255)); 
            labelScore.ForeColor = Color.White; labelCurScore.ForeColor = Color.White; labelRecord.ForeColor = Color.White; labelCurRecord.ForeColor = Color.White;
            pictureBox1.BackColor = myRgbColor;
            pictureBox2.BackColor = myRgbColor;
            pictureBox3.BackColor = myRgbColor;
            pictureBox4.BackColor = myRgbColor;
            if (pxNeed == 1)
                pictureBox1.BackColor = myChooseColor;
            else if (pxNeed == 2)
                pictureBox2.BackColor = myChooseColor;
            else if (pxNeed == 3)
                pictureBox3.BackColor = myChooseColor;
            else if (pxNeed == 4)
                pictureBox4.BackColor = myChooseColor;
            labelScore.BackColor = pictureBox1.BackColor; labelCurScore.BackColor = pictureBox2.BackColor; labelRecord.BackColor = pictureBox2.BackColor; labelCurRecord.BackColor = pictureBox1.BackColor;

            for (int i = 0; i < 4; i++)
            {
                if (i == pxNeed - 1 && myChooseColor != Color.Red)
                    pics[pxNeed - 1].Click += Chosen_Click;
                else if (i != pxNeed - 1 && myChooseColor != Color.Red)
                    pics[i].Click += End_Click;
                else if (i == pxNeed - 1 && myChooseColor == Color.Red)
                    pics[pxNeed - 1].Click += End_Click;
                else if (i != pxNeed - 1 && myChooseColor == Color.Red)
                    pics[i].Click += Chosen_Click;
            }
                
        }

      
        

        private void End_Click(object sender, EventArgs e)
        {
            SoundPlayer sound = new SoundPlayer(Properties.Resources.lost);
            sound.Play();
            tmDown.Stop();
            pxRepeat.Show();
            pxRepeat.BringToFront();
            for (int i = 0; i < 4; i++)
            {
                if (i == pxNeed - 1 && myChooseColor != Color.Red)
                    pics[pxNeed - 1].Click -= Chosen_Click;
                else if (i != pxNeed - 1 && myChooseColor != Color.Red)
                    pics[i].Click -= End_Click;
                else if (i == pxNeed - 1 && myChooseColor == Color.Red)
                    pics[pxNeed - 1].Click -= End_Click;
                else if (i != pxNeed - 1 && myChooseColor == Color.Red)
                    pics[i].Click -= Chosen_Click;
            }
            if (score > best)
                best = score;
            
            labelCurScore.Text = best.ToString();
            pxRepeat.MouseEnter += delegate { pxRepeat.BorderStyle = BorderStyle.FixedSingle; };
            pxRepeat.MouseLeave += delegate { pxRepeat.BorderStyle = BorderStyle.None; };
        }



        private void PxRepeat_Click(object sender, EventArgs e)
        {
            SoundPlayer sound = new SoundPlayer(Properties.Resources.click);
            sound.Play();
            pxRepeat.Hide();
            score = 0;
            SEС_MAX = 1500;
            labelCurRecord.Text = $"{0}";
            tmDown.Start();
            progressBar1.Value = SEС_MAX;
            startDown = DateTime.Now;
            startPlay();
        }

        private void Chosen_Click(object sender, EventArgs e)
        {
            SoundPlayer sound = new SoundPlayer(Properties.Resources.clickPic);
            sound.Play();

            SEС_MAX+= 650;
           
            if (progressBar1.Value > 3000)
                progressBar1.Value = 3000;
            score++;
            labelCurRecord.Text = $"{score}";
            for (int i = 0; i < 4; i++)
            {
                if (i == pxNeed - 1 && myChooseColor != Color.Red)
                    pics[pxNeed - 1].Click -= Chosen_Click;
                else if (i != pxNeed - 1 && myChooseColor != Color.Red)
                    pics[i].Click -= End_Click;
                else if (i == pxNeed - 1 && myChooseColor == Color.Red)
                    pics[pxNeed - 1].Click -= End_Click;
                else if (i != pxNeed - 1 && myChooseColor == Color.Red)
                    pics[i].Click -= Chosen_Click;
            }

            startPlay();
        }

     
    }
}
