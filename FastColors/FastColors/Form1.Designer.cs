﻿
namespace FastColors
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pxPlay = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelScore = new System.Windows.Forms.Label();
            this.labelRecord = new System.Windows.Forms.Label();
            this.labelCurScore = new System.Windows.Forms.Label();
            this.labelCurRecord = new System.Windows.Forms.Label();
            this.pxRepeat = new System.Windows.Forms.PictureBox();
            this.labelTap = new System.Windows.Forms.Label();
            this.labelRed = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxPlay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxRepeat)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, -2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 286);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "0";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Location = new System.Drawing.Point(222, -2);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(216, 286);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "1";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Location = new System.Drawing.Point(222, 282);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(216, 297);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Tag = "3";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox4.Location = new System.Drawing.Point(0, 282);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(223, 297);
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Tag = "2";
            // 
            // pxPlay
            // 
            this.pxPlay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pxPlay.Image = global::FastColors.Properties.Resources.play_button;
            this.pxPlay.Location = new System.Drawing.Point(182, 245);
            this.pxPlay.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pxPlay.Name = "pxPlay";
            this.pxPlay.Size = new System.Drawing.Size(92, 85);
            this.pxPlay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pxPlay.TabIndex = 4;
            this.pxPlay.TabStop = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(149, 34);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.progressBar1.Maximum = 3000;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(150, 19);
            this.progressBar1.TabIndex = 5;
            this.progressBar1.Value = 1500;
            // 
            // labelTitle
            // 
            this.labelTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Comic Sans MS", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(2, 78);
            this.labelTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(448, 90);
            this.labelTitle.TabIndex = 6;
            this.labelTitle.Text = "FAST COLOR";
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.Font = new System.Drawing.Font("Comic Sans MS", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelScore.Location = new System.Drawing.Point(10, 7);
            this.labelScore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(109, 38);
            this.labelScore.TabIndex = 7;
            this.labelScore.Text = "SCORE";
            this.labelScore.Visible = false;
            // 
            // labelRecord
            // 
            this.labelRecord.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRecord.AutoSize = true;
            this.labelRecord.Font = new System.Drawing.Font("Comic Sans MS", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRecord.Location = new System.Drawing.Point(339, 7);
            this.labelRecord.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRecord.Name = "labelRecord";
            this.labelRecord.Size = new System.Drawing.Size(89, 38);
            this.labelRecord.TabIndex = 8;
            this.labelRecord.Text = "BEST";
            this.labelRecord.Visible = false;
            // 
            // labelCurScore
            // 
            this.labelCurScore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCurScore.AutoSize = true;
            this.labelCurScore.Font = new System.Drawing.Font("Comic Sans MS", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCurScore.Location = new System.Drawing.Point(364, 46);
            this.labelCurScore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelCurScore.Name = "labelCurScore";
            this.labelCurScore.Size = new System.Drawing.Size(33, 38);
            this.labelCurScore.TabIndex = 9;
            this.labelCurScore.Text = "0";
            this.labelCurScore.Visible = false;
            // 
            // labelCurRecord
            // 
            this.labelCurRecord.AutoSize = true;
            this.labelCurRecord.Font = new System.Drawing.Font("Comic Sans MS", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCurRecord.Location = new System.Drawing.Point(45, 46);
            this.labelCurRecord.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelCurRecord.Name = "labelCurRecord";
            this.labelCurRecord.Size = new System.Drawing.Size(33, 38);
            this.labelCurRecord.TabIndex = 10;
            this.labelCurRecord.Text = "0";
            this.labelCurRecord.Visible = false;
            // 
            // pxRepeat
            // 
            this.pxRepeat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pxRepeat.Image = ((System.Drawing.Image)(resources.GetObject("pxRepeat.Image")));
            this.pxRepeat.Location = new System.Drawing.Point(176, 245);
            this.pxRepeat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pxRepeat.Name = "pxRepeat";
            this.pxRepeat.Size = new System.Drawing.Size(92, 85);
            this.pxRepeat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pxRepeat.TabIndex = 11;
            this.pxRepeat.TabStop = false;
            this.pxRepeat.Visible = false;
            // 
            // labelTap
            // 
            this.labelTap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTap.AutoSize = true;
            this.labelTap.Font = new System.Drawing.Font("Comic Sans MS", 20F);
            this.labelTap.Location = new System.Drawing.Point(68, 376);
            this.labelTap.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelTap.Name = "labelTap";
            this.labelTap.Size = new System.Drawing.Size(328, 38);
            this.labelTap.TabIndex = 12;
            this.labelTap.Text = "Tap on the another color";
            // 
            // labelRed
            // 
            this.labelRed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRed.AutoSize = true;
            this.labelRed.Font = new System.Drawing.Font("Comic Sans MS", 20F);
            this.labelRed.ForeColor = System.Drawing.Color.Red;
            this.labelRed.Location = new System.Drawing.Point(111, 439);
            this.labelRed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRed.Name = "labelRed";
            this.labelRed.Size = new System.Drawing.Size(224, 38);
            this.labelRed.TabIndex = 13;
            this.labelRed.Text = "Dont touch RED!";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(436, 573);
            this.Controls.Add(this.labelRed);
            this.Controls.Add(this.labelTap);
            this.Controls.Add(this.pxRepeat);
            this.Controls.Add(this.labelCurRecord);
            this.Controls.Add(this.labelCurScore);
            this.Controls.Add(this.labelRecord);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.pxPlay);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "FastColor";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxPlay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxRepeat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pxPlay;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Label labelRecord;
        private System.Windows.Forms.Label labelCurScore;
        private System.Windows.Forms.Label labelCurRecord;
        private System.Windows.Forms.PictureBox pxRepeat;
        private System.Windows.Forms.Label labelTap;
        private System.Windows.Forms.Label labelRed;
    }
}

