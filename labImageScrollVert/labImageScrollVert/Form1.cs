﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollVert
{
    public partial class Form1 : Form
    {
        private Bitmap imBG;
        private Bitmap imCar;
        private Bitmap b;
        private Graphics g;
        private int deltaX;
        private int deltaY;

        public Form1()
        {
            InitializeComponent();
    
            imBG = Properties.Resources.doroga;
            imCar = Properties.Resources.car;
            
            this.Height = imBG.Height;
            this.Width = imBG.Width;
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            this.DoubleBuffered = true;

            this.KeyDown += Form1_KeyDown;
           // this.Paint += (s, e) => { UpdateCar(); e.Graphics.DrawImage(b, 0, 0); };
            this.Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(b, 0, 0); };
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;


        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {

                case Keys.Up:
                    UpdateDeltaX(15);
                    break;

                case Keys.Down:
                    UpdateDeltaX(-15);
                    break;
                case Keys.Left:
                    UpdateDeltaY(-15);
                    break;

                case Keys.Right:
                    UpdateDeltaY(15);
                    break;

            }
            this.Invalidate();
        }


        private void UpdateBG()
        {
            g.Clear(SystemColors.Control);
           
            for (int i = 0; i < 3; i++)
            {
                g.DrawImage(imBG, 0, deltaX + i * imBG.Height);
                g.DrawImage(imCar, deltaY + this.Width - imCar.Width / 2, this.Height / 2 - imCar.Height / 2);
            }
        }
        

        



        private void UpdateDeltaX(int v)
        {
            this.Text = $"{Application.ProductName} : {deltaX}, {v}";
            deltaX += v;
            if (deltaX > 0)            {
                deltaX -= imBG.Height;
            }
            else
            {
                if (deltaX < -imBG.Height)
                    deltaX += imBG.Height;
            }
        }

        private void UpdateDeltaY(int v)
        {
            this.Text = $"{Application.ProductName} : {deltaY}, {v}";
            deltaY += v;
            if (deltaY > 0)
            {
                deltaY -= imCar.Width/2;
            }
            else
            {
                if (deltaY < -imBG.Width)
                    deltaY += imCar.Width/2;
            }
        }
    }
}
