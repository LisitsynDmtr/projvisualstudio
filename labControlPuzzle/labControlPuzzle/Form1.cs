﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labControlPuzzle
{
    public partial class Form1 : Form
    {
        private const int HOLD_PIXEL = 15;
        private Bitmap x_Image;
        private Bitmap imBg;
        private int sizex;
        private PictureBox[,] pics;
      
        private int cellWidth;
        private int cellHeight;
        private Point startMouseDown;
        public static int Rows { get; private set; } = 4;
        public static int Cols { get; private set; } = 3;

        Point[,] positions = new Point[Rows,Cols];
        Bitmap d;
        private int sizey;

        public Form1()
        {
            InitializeComponent();
            x_Image = Properties.Resources.zamok;
            imBg = new Bitmap(x_Image, this.Width, this.Height);
        
            sizex = this.Width; sizey = this.Height;
            CreateCells();
            ResizeCells();
            StartLocationCells();
            RandomLocationCells();
            this.KeyDown += Form1_KeyDown;
            toolStrip1.BringToFront();
            toolStripButton1.Click += ToolStripMenuItem1_Click;
            //hw

            //совм перемещ неск компонентов с помощью мыши
            this.Resize += Form1_Resize;
           

        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            imBg = new Bitmap(x_Image, this.Width, this.Height);
            ResizeCells();
            StartLocationCells();
        }

        private void ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                x_Image = (Bitmap)Bitmap.FromFile(dialog.FileName);

                imBg = new Bitmap(x_Image, this.Width, this.Height);
            }

            ResizeCells();
            StartLocationCells();
            RandomLocationCells();

        }


        

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {

                case Keys.F1:
                    StartLocationCells();
                    break;
                case Keys.F2:
                    RandomLocationCells();
                    break;
                case Keys.F3:
                    ResizeCells();
                    break;
                default:
                    break;
            }
        }

        private void RandomLocationCells()
        {
            Random rnd = new Random();
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Location = new Point(
                    rnd.Next(this.ClientSize.Width - pics[i,j].Width),
                    rnd.Next(this.ClientSize.Height - pics[i,j].Height));
                }
            }
        }

        private void StartLocationCells()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    
                    pics[i, j].Location = new Point(j*cellWidth, i*cellHeight);
                    positions[i, j] = pics[i, j].Location;
                }
            }
        }

        private void ResizeCells()
        {
            cellWidth = this.ClientSize.Width / Cols;
            cellHeight = this.ClientSize.Height / Rows;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Width = cellWidth;
                    pics[i, j].Height = cellHeight;
                    pics[i, j].BackgroundImage = new Bitmap(cellWidth, cellHeight);
                    pics[i, j].Tag = (i, j);
                    var g = Graphics.FromImage(pics[i, j].BackgroundImage);
                    g.Clear(DefaultBackColor);
                    g.DrawImage(imBg, new Rectangle(0, 0, pics[i, j].Width, pics[i, j].Height),
                        new Rectangle(j*cellWidth, i* cellHeight, cellWidth, cellHeight), 
                        GraphicsUnit.Pixel
                        );
                    g.Dispose();
                }
            }
        }

        private void CreateCells()
        {
            pics = new PictureBox[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j] = new PictureBox();
                    pics[i, j].BorderStyle = BorderStyle.FixedSingle;
                    pics[i, j].MouseDown += PictureBox_All_MouseDown;
                    pics[i, j].MouseMove += PictureBox_All_MouseMove;
                    pics[i, j].MouseUp += PictureBox_All_MouseUp;
                    
                    this.Controls.Add(pics[i, j]);
                }
            }
        }

        private void PictureBox_All_MouseUp(object sender, MouseEventArgs e)
        {
            if (sender is Control x)
            {
                if (e.Button == MouseButtons.Left)
                {
                    var xLocation = x.Location;
                    for (int i = 0; i < Rows; i++)
                        for (int j = 0; j < Cols; j++)
                        {
                            if (x.Location.X > j * cellWidth - HOLD_PIXEL &&
                                x.Location.X < j * cellWidth + HOLD_PIXEL)
                                xLocation.X = j * cellWidth;
                            if (x.Location.Y > i * cellHeight - HOLD_PIXEL &&
                                x.Location.Y < i * cellHeight + HOLD_PIXEL)
                                xLocation.Y = i * cellHeight;
                        }
                    x.Location = xLocation;
                    toolStrip1.BringToFront();
                }
                (int r, int c) = ((int, int))x.Tag;
              
                if (x.Location == positions[r,c])
                {
                
                    pics[r,c].MouseMove -= PictureBox_All_MouseMove;
                }
                x.Cursor = Cursors.Default;
                pics[r, c].BorderStyle = BorderStyle.FixedSingle;

            }
        }

       

        private void PictureBox_All_MouseMove(object sender, MouseEventArgs e)
        {
            if (sender is Control x)
            {
                if (e.Button == MouseButtons.Left) {
                  
                    x.Location = new Point(
                        x.Location.X + e.X -startMouseDown.X,
                        x.Location.Y + e.Y - startMouseDown.Y

                        );
                        
                }
                (int r, int c) = ((int,int))x.Tag;
                this.Text = $"{r}:{c}";
                

            }
        }

        private void PictureBox_All_MouseDown(object sender, MouseEventArgs e)
        {
            startMouseDown = e.Location;
            if (sender is Control x)
            {
                var y = sender as PictureBox;
                y.BorderStyle = BorderStyle.None;
               
                x.BringToFront();
                x.Cursor = Cursors.NoMove2D;
            }
        }
    }
}
