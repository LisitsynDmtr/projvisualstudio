﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labRoadEditor
{

    public partial class Form1 : Form
    {
        enum SelMode
        {
            Line,
            Square,
            Normal
        }
       
        private Bitmap fon_image;
        private Bitmap fon_road;
        private Bitmap b;
        private Bitmap b2;
        private int cellWidth;
        private int cellHeight;
        private int cellWidthOrig;
        private int cellHeightOrig;
        private PictureBox[,] pics;
        private PictureBox[,] picsOrig;
        List<PictureBox> pictures = new List<PictureBox>();
        public int Cols { get; private set; } = 10;
        public int Rows { get; private set; } = 10;
        public int ColsOrig { get; private set; } = 10;
        public int RowsOrig { get; private set; } = 10;

        public Form1()
        {
            InitializeComponent();

            fon_image = Properties.Resources.sprite;
            fon_road = Properties.Resources.fon;
            b = new Bitmap(fon_image, pxOriginal.Width, pxOriginal.Height);
            b2 = new Bitmap(fon_road,pxRoad.Width , pxRoad.Height);


            pxRoad.Hide();
            pxOriginal.Hide();

            CreateCells();
            ResizeCells();
            StartLocationCells();
            buClear.Click += BuClear_Click;
            buSave.Click += BuSave_Click;

            buLoad.Click += BuLoad_Click;
            x10ToolStripMenuItem.Click += (s, e) => {
                for (int i = 0; i < RowsOrig; i++)
                {
                    for (int j = 0; j < ColsOrig; j++)
                        this.Controls.Remove(picsOrig[i, j]);
                }
                RowsOrig = 10; ColsOrig = 10; CreateCells(); ResizeCells(); StartLocationCells(); this.Invalidate();
            };
            x5ToolStripMenuItem.Click += (s, e) =>
            {
                for (int i = 0; i < RowsOrig; i++)
                {
                    for (int j = 0; j < ColsOrig; j++)
                        this.Controls.Remove(picsOrig[i, j]);
                }
                RowsOrig = 5; ColsOrig = 5; CreateCells(); ResizeCells(); StartLocationCells(); this.Invalidate();
            };
            x8ToolStripMenuItem.Click += (s, e) => {
                for (int i = 0; i < RowsOrig; i++)
                {
                    for (int j = 0; j < ColsOrig; j++)
                        this.Controls.Remove(picsOrig[i, j]);
                }
                RowsOrig = 8; ColsOrig = 8; CreateCells(); ResizeCells(); StartLocationCells(); this.Invalidate();
            };
            x9ToolStripMenuItem.Click += (s, e) => {
                for (int i = 0; i < RowsOrig; i++)
                {
                    for (int j = 0; j < ColsOrig; j++)
                        this.Controls.Remove(picsOrig[i, j]);
                }
                RowsOrig = 9; ColsOrig = 9; CreateCells(); ResizeCells(); StartLocationCells(); this.Invalidate();
            };
            x3ToolStripMenuItem.Click += (s, e) => {
                for (int i = 0; i < RowsOrig; i++)
                {
                    for (int j = 0; j < ColsOrig; j++)
                        this.Controls.Remove(picsOrig[i, j]);
                }
                RowsOrig = 3; ColsOrig = 3; CreateCells(); ResizeCells(); StartLocationCells(); this.Invalidate();
            };

        }

        private void BuClear_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Width = cellWidth;
                    pics[i, j].Height = cellHeight;
                    pics[i, j].BackgroundImage = new Bitmap(cellWidth, cellHeight);
                    var g = Graphics.FromImage(pics[i, j].BackgroundImage);
                    g.Clear(DefaultBackColor);
                    g.DrawImage(b2, new Rectangle(0, 0, pics[i, j].Width, pics[i, j].Height),
                        new Rectangle(i * cellWidth, j * cellHeight, cellWidth, cellHeight),
                        GraphicsUnit.Pixel
                        );
                    g.Dispose();
                }
            }
        }

        private void BuSave_Click(object sender, EventArgs e)
        {
           
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "(*.JPG)|*.JPG";
            if (dialog.ShowDialog() == DialogResult.OK)
                {
                var bitmap = new Bitmap(pxRoad.Width, pxRoad.Height);
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Cols; j++)
                    {

                        pics[i, j].BorderStyle = BorderStyle.None;
                        pics[i, j].DrawToBitmap(bitmap, new Rectangle(j * cellWidth, i * cellHeight, cellWidth, cellHeight));
                        pics[i, j].BorderStyle = BorderStyle.FixedSingle;

                    }
                }
                
                
                bitmap.Save(dialog.FileName);
                }

        }

        private void BuLoad_Click(object sender, EventArgs e)
        {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    pxOriginal.BackgroundImage = new Bitmap(dialog.FileName);
                    var bitmap = new Bitmap(pxOriginal.BackgroundImage, pxOriginal.Width, pxOriginal.Height);
                    b = bitmap;
                    pxOriginal.BackgroundImage = new Bitmap(b);
                    ResizeCells();
                    pxOriginal.Invalidate();
                }
        }

        private void ResizeCells()
        {
            cellWidth = pxRoad.Width / Cols;
            cellHeight = pxRoad.Height / Rows;

            cellWidthOrig = pxOriginal.Width / ColsOrig;
            cellHeightOrig = pxOriginal.Height / RowsOrig;

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Width = cellWidth;
                    pics[i, j].Height = cellHeight;
                    pics[i, j].BackgroundImage = new Bitmap(cellWidth, cellHeight);
                    var g = Graphics.FromImage(pics[i, j].BackgroundImage);
                    g.Clear(DefaultBackColor);
                    g.DrawImage(b2, new Rectangle(0, 0, pics[i, j].Width, pics[i, j].Height),
                        new Rectangle(i * cellWidth, j * cellHeight, cellWidth, cellHeight),
                        GraphicsUnit.Pixel
                        );
                    g.Dispose();
                }
            }

            for (int k = 0; k < RowsOrig; k++)
            {
                for (int l = 0; l < ColsOrig; l++)
                {
                    picsOrig[k, l].Width = cellWidthOrig;
                    picsOrig[k, l].Height = cellHeightOrig;
                    picsOrig[k, l].BackgroundImage = new Bitmap(cellWidthOrig, cellHeightOrig);
                    var g = Graphics.FromImage(picsOrig[k, l].BackgroundImage);
                    g.Clear(DefaultBackColor);
                    g.DrawImage(b, new Rectangle(0, 0, picsOrig[k, l].Width, picsOrig[k, l].Height),
                        new Rectangle(l * cellWidthOrig, k * cellHeightOrig, cellWidthOrig, cellHeightOrig),
                        GraphicsUnit.Pixel
                        );
                    g.Dispose();
                }
            }




        }

        private void StartLocationCells()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                    pics[i, j].Location = new Point(j * cellWidth+pxRoad.Location.X, i * cellHeight + pxRoad.Location.Y);

            }
            for (int i = 0; i < RowsOrig; i++)
            {
                for (int j = 0; j < ColsOrig; j++)
                    picsOrig[i, j].Location = new Point(j * cellWidthOrig + pxOriginal.Location.X, i * cellHeightOrig + pxOriginal.Location.Y);
            }
        }

        private void CreateCells()
        {
            pics = new PictureBox[Rows, Cols];
            picsOrig = new PictureBox[RowsOrig, ColsOrig];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j] = new PictureBox();
                    pics[i, j].BorderStyle = BorderStyle.FixedSingle;
                    pics[i, j].MouseMove += Form1_MouseMove1;
                    pics[i, j].MouseLeave += Form1_MouseLeave;
                    pics[i, j].MouseDown += Form1_MouseDown;
                    this.Controls.Add(pics[i, j]);
                }
            }

            for (int k = 0; k < RowsOrig; k++)
            {
                for (int l = 0; l < ColsOrig; l++)
                {
                    picsOrig[k, l] = new PictureBox();
                    picsOrig[k, l].BorderStyle = BorderStyle.FixedSingle;
                    picsOrig[k, l].MouseMove += Form1_MouseMove;
                    picsOrig[k, l].MouseLeave += Form1_MouseLeave;
                    picsOrig[k, l].MouseClick += Form1_MouseClick;

                    this.Controls.Add(picsOrig[k, l]);
                }
            }




        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
           
            if (e.Button == MouseButtons.Right)
            {
                if (sender is PictureBox x)
                {
                    x.BackgroundImage = null;
                    x.BackColor = Color.White;
                }
            }

            if (e.Button == MouseButtons.Left)
            {

                if (sender is PictureBox x && pictures.Count != 0)
                {
                    var bitmap = new Bitmap(pictures[0].BackgroundImage, cellWidth, cellHeight);
                    x.BackgroundImage = new Bitmap(bitmap);
                }

            }



        }

      

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
           
            pictures.Clear();
            if (sender is PictureBox x) {
                pictures.Add(x);
                var bitmap = new Bitmap(pictures[0].BackgroundImage, pxChosen.Width, pxChosen.Height);
                pxChosen.Image = new Bitmap(bitmap);
                
             }
            
        }

        private void Form1_MouseLeave(object sender, EventArgs e)
        {
            if (sender is Control x)
            {

                var y = sender as PictureBox;
                y.BorderStyle = BorderStyle.FixedSingle;

            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (sender is Control x)
            {

                var y = sender as PictureBox;
                y.BorderStyle = BorderStyle.None;
              
            }

        }
        private void Form1_MouseMove1(object sender, MouseEventArgs e)
        {
            if (sender is Control g)
            {
                var y = sender as PictureBox;
                y.BorderStyle = BorderStyle.None;
            }

            if (e.Button == MouseButtons.Left) {

                if (sender is PictureBox x && pictures.Count != 0)
                {
                    var bitmap = new Bitmap(pictures[0].BackgroundImage, cellWidth, cellHeight);
                    x.BackgroundImage = new Bitmap(bitmap);
                }

            }

        }


    }
}
