﻿
namespace labRoadEditor
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pxOriginal = new System.Windows.Forms.PictureBox();
            this.pxRoad = new System.Windows.Forms.PictureBox();
            this.pxChosen = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buLoad = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.x10ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x8ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x9ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buSave = new System.Windows.Forms.ToolStripButton();
            this.buClear = new System.Windows.Forms.ToolStripButton();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pxOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxRoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxChosen)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pxOriginal
            // 
            this.pxOriginal.Location = new System.Drawing.Point(11, 265);
            this.pxOriginal.Margin = new System.Windows.Forms.Padding(2);
            this.pxOriginal.Name = "pxOriginal";
            this.pxOriginal.Size = new System.Drawing.Size(338, 320);
            this.pxOriginal.TabIndex = 0;
            this.pxOriginal.TabStop = false;
            // 
            // pxRoad
            // 
            this.pxRoad.Image = global::labRoadEditor.Properties.Resources.fon;
            this.pxRoad.Location = new System.Drawing.Point(408, 42);
            this.pxRoad.Margin = new System.Windows.Forms.Padding(2);
            this.pxRoad.Name = "pxRoad";
            this.pxRoad.Size = new System.Drawing.Size(617, 543);
            this.pxRoad.TabIndex = 2;
            this.pxRoad.TabStop = false;
            // 
            // pxChosen
            // 
            this.pxChosen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pxChosen.Location = new System.Drawing.Point(88, 93);
            this.pxChosen.Name = "pxChosen";
            this.pxChosen.Size = new System.Drawing.Size(184, 126);
            this.pxChosen.TabIndex = 3;
            this.pxChosen.TabStop = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buLoad,
            this.toolStripDropDownButton1,
            this.buSave,
            this.buClear});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1050, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buLoad
            // 
            this.buLoad.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buLoad.Image = ((System.Drawing.Image)(resources.GetObject("buLoad.Image")));
            this.buLoad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buLoad.Name = "buLoad";
            this.buLoad.Size = new System.Drawing.Size(127, 22);
            this.buLoad.Text = "Загрузить картинку...";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.x10ToolStripMenuItem,
            this.x5ToolStripMenuItem,
            this.x8ToolStripMenuItem,
            this.x9ToolStripMenuItem,
            this.x3ToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(106, 22);
            this.toolStripDropDownButton1.Text = "Изменить сетку";
            // 
            // x10ToolStripMenuItem
            // 
            this.x10ToolStripMenuItem.Name = "x10ToolStripMenuItem";
            this.x10ToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.x10ToolStripMenuItem.Text = "10 x10";
            // 
            // x5ToolStripMenuItem
            // 
            this.x5ToolStripMenuItem.Name = "x5ToolStripMenuItem";
            this.x5ToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.x5ToolStripMenuItem.Text = "5 x 5";
            // 
            // x8ToolStripMenuItem
            // 
            this.x8ToolStripMenuItem.Name = "x8ToolStripMenuItem";
            this.x8ToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.x8ToolStripMenuItem.Text = "8 x 8";
            // 
            // x9ToolStripMenuItem
            // 
            this.x9ToolStripMenuItem.Name = "x9ToolStripMenuItem";
            this.x9ToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.x9ToolStripMenuItem.Text = "9 x 9";
            // 
            // x3ToolStripMenuItem
            // 
            this.x3ToolStripMenuItem.Name = "x3ToolStripMenuItem";
            this.x3ToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.x3ToolStripMenuItem.Text = "3 x 3";
            // 
            // buSave
            // 
            this.buSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buSave.Image = ((System.Drawing.Image)(resources.GetObject("buSave.Image")));
            this.buSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buSave.Name = "buSave";
            this.buSave.Size = new System.Drawing.Size(132, 22);
            this.buSave.Text = "Сохранить картинку...";
            // 
            // buClear
            // 
            this.buClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buClear.Image = ((System.Drawing.Image)(resources.GetObject("buClear.Image")));
            this.buClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buClear.Name = "buClear";
            this.buClear.Size = new System.Drawing.Size(63, 22);
            this.buClear.Text = "Очистить";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.Location = new System.Drawing.Point(54, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(262, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "Выбранный элемент:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 596);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.pxChosen);
            this.Controls.Add(this.pxRoad);
            this.Controls.Add(this.pxOriginal);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "RoadEditor";
            ((System.ComponentModel.ISupportInitialize)(this.pxOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxRoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxChosen)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pxOriginal;
        private System.Windows.Forms.PictureBox pxRoad;
        private System.Windows.Forms.PictureBox pxChosen;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buLoad;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem x10ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x8ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x9ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton buSave;
        private System.Windows.Forms.ToolStripButton buClear;
        private System.Windows.Forms.Label label1;
    }
}

