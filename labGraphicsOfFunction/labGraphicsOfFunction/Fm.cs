﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labGraphicsOfFunction
{
    public partial class labGraphicsOfFunctions : Form
    {
        private Bitmap b;
        private Graphics g;
        private int cellCount;
        private int cellSize = 30;
        private int centrX;
        private int centrY;
        Pen BlackPen;
        Pen RedPen;
        Pen GreenPen;
     

        public labGraphicsOfFunctions()
        {

            

            InitializeComponent();
            //Hw 
            //сетка + график + диапазон значений
           
            GreenPen = new Pen(Color.Green,4);
            BlackPen = new Pen(Color.Black);
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            labelA.Text = $"a:{trackBarA.Value}";
            labelB.Text = $"b:{trackBarB.Value}";
            labelC.Text = $"c:{trackBarC.Value}";

            cellCount = this.ClientSize.Width / cellSize;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);
            
            drawMap();
            drawOs();

            trackBarA.ValueChanged += TrackBarAll_ValueChanged;
            trackBarB.ValueChanged += TrackBarAll_ValueChanged;
            trackBarC.ValueChanged += TrackBarAll_ValueChanged;


            this.Resize += LabGraphicsOfFunctions_Resize;
            comboBox.SelectedIndexChanged += ComboBox_SelectedIndexChanged;
        }

        private void TrackBarAll_ValueChanged(object sender, EventArgs e)
        {
            ComboBox_SelectedIndexChanged(sender, e);
            labelA.Text = $"a:{trackBarA.Value}";
            labelB.Text = $"a:{trackBarB.Value}";
            labelC.Text = $"a:{trackBarC.Value}";
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int a = trackBarA.Value;
            int b = trackBarB.Value;
            int c = trackBarC.Value;
            double y, x;


            g.Clear(SystemColors.Control);
            drawMap();
            drawOs();

            PointF[] curvePointsPlus = new PointF[this.Width];
            PointF[] curvePointsMinus = new PointF[this.Width];

            int XWID = this.Width / 2;
            int YHEI = this.ClientSize.Height / 2;

            g.TranslateTransform(XWID + splitter.Width / 2, YHEI);

            switch (comboBox.Text) {

                case "y = x":
                   
                    
                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = x;

                        curvePointsPlus[i] = new Point((int)x, -(int)y);
                        

                    }
                    g.DrawCurve(GreenPen, curvePointsPlus);
                    
                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = x;

                        curvePointsMinus[i] = new Point(-(int)x,  (int)y);


                    }
                   
                    g.DrawCurve(GreenPen, curvePointsMinus);
                    g.ResetTransform();

                    this.Invalidate();

                    break;
                case "y = a*(x^2)+b*x + c":
                    

                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = a*(x*x)+ b*x + c;

                        curvePointsPlus[i] = new Point((int)x, -(int)y);


                    }
                    g.DrawCurve(GreenPen, curvePointsPlus);

                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = a * (x * x) + b * x + c;

                        curvePointsMinus[i] = new Point(-(int)x, -(int)y);


                    }

                    g.DrawCurve(GreenPen, curvePointsMinus);
                    g.ResetTransform();

                    this.Invalidate();

                    break;

                case "y = |x|":
                   

                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = x;

                        curvePointsPlus[i] = new Point((int)x, -(int)y);


                    }
                    g.DrawCurve(GreenPen, curvePointsPlus);

                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = x;

                        curvePointsMinus[i] = new Point(-(int)x, -(int)y);


                    }

                    g.DrawCurve(GreenPen, curvePointsMinus);
                    g.ResetTransform();

                    this.Invalidate();

                    break;
                case "y = sin(x)":


                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = Math.Sin(x) * 100;

                        curvePointsPlus[i] = new Point((int)x* 10, -(int)y);


                    }
                    g.DrawCurve(GreenPen, curvePointsPlus);

                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = Math.Sin(x) * 100;

                        curvePointsMinus[i] = new Point(-(int)x* 10, (int)y);


                    }

                    g.DrawCurve(GreenPen, curvePointsMinus);
                    g.ResetTransform();

                    this.Invalidate();

                    break;
                case "y = cos(x)":


                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = Math.Cos(x) * 100;

                        curvePointsPlus[i] = new Point((int)x * 10, -(int)y);


                    }
                    g.DrawCurve(GreenPen, curvePointsPlus);

                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = Math.Cos(x) * 100;

                        curvePointsMinus[i] = new Point(-(int)x * 10, -(int)y);


                    }

                    g.DrawCurve(GreenPen, curvePointsMinus);
                    g.ResetTransform();

                    this.Invalidate();

                    break;
                case "y = 13cos(x) - 5cos(2x) - 2cos(3x) - cos(4x)":


                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = Math.Sin(x) *( Math.Sqrt(Math.Abs(Math.Cos(x)))/Math.Sin(x) + 7/5)-2 * Math.Sin(x) + 2;

                        curvePointsPlus[i] = new Point((int)x*50 , -(int)y*50);


                    }
                    g.DrawCurve(GreenPen, curvePointsPlus);

                    for (int i = 0; i < this.Width; i++)
                    {
                        x = i;
                        y = Math.Sin(x) * (Math.Sqrt(Math.Abs(Math.Cos(x))) / Math.Sin(x) + 7 / 5) - 2 * Math.Sin(x) + 2;

                         curvePointsMinus[i] = new Point(-(int)x * 50, (int)y*50);


                    }

                    g.DrawCurve(GreenPen, curvePointsMinus);
                    g.ResetTransform();

                    this.Invalidate();

                    break;
            }

        }

     



        private void LabGraphicsOfFunctions_Resize(object sender, EventArgs e)
        {
            g.Clear(SystemColors.Control);
            cellCount = this.Width / cellSize;
            drawMap();
            drawOs();
            ComboBox_SelectedIndexChanged(sender, e);
           
        }

        private void drawMap() {

            for (int i = 0; i < cellCount; i++)
            {
               
                g.DrawLine(BlackPen, i * cellSize + splitter.Width, 0, i * cellSize + splitter.Width, this.ClientSize.Height);
                
                g.DrawLine(BlackPen, 0, i * cellSize,this.ClientSize.Width, i * cellSize);
            }
            this.Invalidate();
        }

        private void drawOs() {

           
            RedPen = new Pen(Color.Red, 3);
            
  
            centrX = this.Width; 
            centrY = this.ClientSize.Height; 

            g.DrawLine(RedPen, 0, centrY/ 2 , centrX, centrY/2);

            g.DrawLine(RedPen, centrX/2 +splitter.Width/2 , 0, centrX / 2 + splitter.Width/2 , centrY);

            

        }
       
    }
}
