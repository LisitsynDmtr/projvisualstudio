﻿namespace labGraphicsOfFunction
{
    partial class labGraphicsOfFunctions
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitter = new System.Windows.Forms.Splitter();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.trackBarA = new System.Windows.Forms.TrackBar();
            this.trackBarB = new System.Windows.Forms.TrackBar();
            this.trackBarC = new System.Windows.Forms.TrackBar();
            this.labelA = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.labelC = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarC)).BeginInit();
            this.SuspendLayout();
            // 
            // splitter
            // 
            this.splitter.BackColor = System.Drawing.SystemColors.Control;
            this.splitter.Location = new System.Drawing.Point(0, 0);
            this.splitter.Name = "splitter";
            this.splitter.Size = new System.Drawing.Size(200, 600);
            this.splitter.TabIndex = 0;
            this.splitter.TabStop = false;
            // 
            // comboBox
            // 
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Items.AddRange(new object[] {
            "y = x",
            "y = a*(x^2)+b*x + c",
            "y = |x|",
            "y = sin(x)",
            "y = cos(x)",
            "y = 13cos(x) - 5cos(2x) - 2cos(3x) - cos(4x)"});
            this.comboBox.Location = new System.Drawing.Point(12, 28);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(162, 28);
            this.comboBox.TabIndex = 1;
            // 
            // trackBarA
            // 
            this.trackBarA.Location = new System.Drawing.Point(26, 140);
            this.trackBarA.Maximum = 20;
            this.trackBarA.Minimum = -20;
            this.trackBarA.Name = "trackBarA";
            this.trackBarA.Size = new System.Drawing.Size(130, 56);
            this.trackBarA.TabIndex = 2;
            this.trackBarA.Value = 1;
            // 
            // trackBarB
            // 
            this.trackBarB.Location = new System.Drawing.Point(26, 242);
            this.trackBarB.Maximum = 20;
            this.trackBarB.Minimum = -20;
            this.trackBarB.Name = "trackBarB";
            this.trackBarB.Size = new System.Drawing.Size(130, 56);
            this.trackBarB.TabIndex = 3;
            this.trackBarB.Value = 1;
            // 
            // trackBarC
            // 
            this.trackBarC.Location = new System.Drawing.Point(26, 347);
            this.trackBarC.Maximum = 20;
            this.trackBarC.Minimum = -20;
            this.trackBarC.Name = "trackBarC";
            this.trackBarC.Size = new System.Drawing.Size(130, 56);
            this.trackBarC.TabIndex = 4;
            this.trackBarC.Value = 1;
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Location = new System.Drawing.Point(26, 107);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(20, 20);
            this.labelA.TabIndex = 5;
            this.labelA.Text = "a:";
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Location = new System.Drawing.Point(26, 210);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(21, 20);
            this.labelB.TabIndex = 6;
            this.labelB.Text = "b:";
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.Location = new System.Drawing.Point(26, 314);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(19, 20);
            this.labelC.TabIndex = 7;
            this.labelC.Text = "c:";
            // 
            // labGraphicsOfFunctions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(1082, 600);
            this.Controls.Add(this.labelC);
            this.Controls.Add(this.labelB);
            this.Controls.Add(this.labelA);
            this.Controls.Add(this.trackBarC);
            this.Controls.Add(this.trackBarB);
            this.Controls.Add(this.trackBarA);
            this.Controls.Add(this.comboBox);
            this.Controls.Add(this.splitter);
            this.Name = "labGraphicsOfFunctions";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Splitter splitter;
        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.TrackBar trackBarA;
        private System.Windows.Forms.TrackBar trackBarB;
        private System.Windows.Forms.TrackBar trackBarC;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.Label labelC;
    }
}

