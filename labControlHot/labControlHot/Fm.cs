﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labControlHot
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();
            button1.MouseEnter += ButtonAll_MouseEnter;
            button2.MouseEnter += ButtonAll_MouseEnter;
            button3.MouseEnter += ButtonAll_MouseEnter;
            button4.MouseEnter += ButtonAll_MouseEnter;

            button1.MouseLeave += ButtonAll_MouseLeave;
            button1.MouseLeave += ButtonAll_MouseLeave;
            button1.MouseLeave += ButtonAll_MouseLeave;
            button1.MouseLeave += ButtonAll_MouseLeave;

            textBox1.MouseEnter += TextBoxAll_MouseEnter;
            textBox2.MouseEnter += TextBoxAll_MouseEnter;
            textBox3.MouseEnter += TextBoxAll_MouseEnter;

            textBox1.MouseLeave += TextBoxAll_MouseLeave;
            textBox2.MouseLeave += TextBoxAll_MouseLeave;
            textBox3.MouseLeave += TextBoxAll_MouseLeave;

            label1.MouseEnter += LabelAll_MouseEnter;
            label2.MouseEnter += LabelAll_MouseEnter;
            label3.MouseEnter += LabelAll_MouseEnter;

            label1.MouseLeave += LabelAll_MouseLeave;
            label2.MouseLeave += LabelAll_MouseLeave;
            label3.MouseLeave += LabelAll_MouseLeave;
        }

        private void LabelAll_MouseLeave(object sender, EventArgs e)
        {
            if (sender is Control x)
                x.Font = new Font(x.Font, FontStyle.Regular);
        }

        private void LabelAll_MouseEnter(object sender, EventArgs e)
        {
            if (sender is Control x)
                x.Font = new Font(x.Font, FontStyle.Bold);
        }

        private void TextBoxAll_MouseLeave(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.BackColor = SystemColors.Window;
            }

        }

        private void TextBoxAll_MouseEnter(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.BackColor = Color.LightGreen;
            }
        }

        private void ButtonAll_MouseEnter(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.Margin = new Padding(0);
            }

        }


        private void ButtonAll_MouseLeave(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.Margin = new Padding(3);
            };
        }
    }
}
