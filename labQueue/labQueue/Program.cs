﻿using System;
using System.Collections;


namespace labQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue x = new Queue();
            x.Enqueue(6);
            x.Enqueue("Привет!");
            x.Enqueue(54);
            x.Enqueue(new Object());

            Console.WriteLine(x.Peek());
            Console.WriteLine("---------");
            while (x.Count> 0)
            {
                Console.WriteLine(x.Dequeue());
            }
        }
    }
}
