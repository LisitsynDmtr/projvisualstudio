﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labKey
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.KeyDown += Fm_KeyDown;

        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    labText.Text = "Left";
                    break;
                case Keys.Right:
                    labText.Text = "Right";
                    break;
                case Keys.Up:
                    labText.Text = "Up";
                    break;
                case Keys.Down:
                    labText.Text = "Down";
                    break;
                case Keys.Space:
                    if (e.Shift)
                    {
                        labText.Text = "SHIFT PLUS SPACE";
                    }
                    else
                    {
                        labText.Text = "SPACE";
                    }
                    break;

                case Keys.X:
                    labText.Text = e.Shift ? "Shift + X" : "X";
                    break;

                default:
                    labText.Text = $"Other Key: {e.KeyCode}";
                    break;
            }
        }
    }
}
