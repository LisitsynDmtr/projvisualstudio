﻿using System;
using System.Collections.Generic;
using System.IO;

namespace labFile
{
    class Program
    {
        static void Main(string[] args)
        {
            //string path = "C:\\temp\temp.txt";
            //string path = @"C:\temp\temp/txt";

            //string path = Path.Combine(Directory.GetTempPath(), "tempAAA.txt");
            string path = Path.Combine(Directory.GetCurrentDirectory(), "tempAAA.txt");

            //var s = new string[] { "Самара", "Воронеж", "Тула", "Орел", "Полтава" };
            var s = new List<string> { "Самара", "Воронеж", "Тула", "Орел", "Полтава" };

            File.WriteAllText(path,string.Join(Environment.NewLine,s));

            var r = File.ReadAllText(path);
            Console.WriteLine(r);

            FileInfo fileinfo = new FileInfo(path);
            Console.WriteLine(fileinfo.FullName);
            Console.WriteLine(fileinfo.Length);
            Console.WriteLine(fileinfo.Attributes);
         



            Console.WriteLine(File.Exists(path));
            File.Delete(path);
            Console.WriteLine(File.Exists(path));

        }
    }
}
